EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 17 17
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 4650 2000 0    60   Input ~ 0
IN0
$Comp
L Isolator:LTV-847S U201
U 3 1 60A42A77
P 5250 3500
AR Path="/5FA6069E/60A42A77" Ref="U201"  Part="3" 
AR Path="/60AEF199/60A42A77" Ref="U1501"  Part="3" 
AR Path="/60B05AE5/60A42A77" Ref="U1601"  Part="3" 
AR Path="/60B05AFF/60A42A77" Ref="U1701"  Part="3" 
F 0 "U1701" H 5250 3825 50  0000 C CNN
F 1 "LTV-847S" H 5250 3734 50  0000 C CNN
F 2 "Package_DIP:SMDIP-16_W9.53mm" H 5250 3200 50  0001 C CNN
F 3 "http://www.us.liteon.com/downloads/LTV-817-827-847.PDF" H 4650 3950 50  0001 C CNN
	3    5250 3500
	1    0    0    -1  
$EndComp
$Comp
L Isolator:LTV-847S U201
U 4 1 60A439ED
P 5250 4200
AR Path="/5FA6069E/60A439ED" Ref="U201"  Part="4" 
AR Path="/60AEF199/60A439ED" Ref="U1501"  Part="4" 
AR Path="/60B05AE5/60A439ED" Ref="U1601"  Part="4" 
AR Path="/60B05AFF/60A439ED" Ref="U1701"  Part="4" 
F 0 "U1701" H 5250 4525 50  0000 C CNN
F 1 "LTV-847S" H 5250 4434 50  0000 C CNN
F 2 "Package_DIP:SMDIP-16_W9.53mm" H 5250 3900 50  0001 C CNN
F 3 "http://www.us.liteon.com/downloads/LTV-817-827-847.PDF" H 4650 4650 50  0001 C CNN
	4    5250 4200
	1    0    0    -1  
$EndComp
Text Notes 1650 2800 0    60   ~ 0
AVCC can be 1.8V to 5.5V \n  Ic = (1.8V-0.2V)/2.2kΩ = 0.7 mA\n  Ic = (5.5V-0.2V)/2.2kΩ = 2.4 mA
Text Notes 1650 2450 0    60   ~ 0
Input High is 3.3V to 5V\n  If = (3.3V-1.2V)/330Ω = 6.4 mA\n  If = (5.0V-1.2V)/330Ω = 11.5 mA
Text HLabel 4650 2700 0    60   Input ~ 0
IN1
$Comp
L Device:R R?
U 1 1 60A9CC2F
P 4800 3400
AR Path="/5AC88AD6/60A9CC2F" Ref="R?"  Part="1" 
AR Path="/5ABD2F13/60A9CC2F" Ref="R?"  Part="1" 
AR Path="/5ABD507D/60A9CC2F" Ref="R?"  Part="1" 
AR Path="/5FA6069E/60A9CC2F" Ref="R203"  Part="1" 
AR Path="/60AEF199/60A9CC2F" Ref="R1503"  Part="1" 
AR Path="/60B05AE5/60A9CC2F" Ref="R1603"  Part="1" 
AR Path="/60B05AFF/60A9CC2F" Ref="R1703"  Part="1" 
F 0 "R1703" V 4900 3400 50  0000 C CNN
F 1 "330Ω" V 4700 3400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4730 3400 50  0001 C CNN
F 3 "" H 4800 3400 50  0000 C CNN
	1    4800 3400
	0    -1   -1   0   
$EndComp
Text HLabel 4650 3400 0    60   Input ~ 0
IN2
Text GLabel 4950 3600 0    60   Input ~ 0
DGND
$Comp
L Device:R R?
U 1 1 60A9CC3B
P 4800 4100
AR Path="/5AC88AD6/60A9CC3B" Ref="R?"  Part="1" 
AR Path="/5ABD2F13/60A9CC3B" Ref="R?"  Part="1" 
AR Path="/5ABD507D/60A9CC3B" Ref="R?"  Part="1" 
AR Path="/5FA6069E/60A9CC3B" Ref="R204"  Part="1" 
AR Path="/60AEF199/60A9CC3B" Ref="R1504"  Part="1" 
AR Path="/60B05AE5/60A9CC3B" Ref="R1604"  Part="1" 
AR Path="/60B05AFF/60A9CC3B" Ref="R1704"  Part="1" 
F 0 "R1704" V 4900 4100 50  0000 C CNN
F 1 "330Ω" V 4700 4100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4730 4100 50  0001 C CNN
F 3 "" H 4800 4100 50  0000 C CNN
	1    4800 4100
	0    -1   -1   0   
$EndComp
Text GLabel 4950 4300 0    60   Input ~ 0
DGND
$Comp
L Device:R R?
U 1 1 60AA8260
P 5800 3900
AR Path="/5AC88AD6/60AA8260" Ref="R?"  Part="1" 
AR Path="/5ABD2F13/60AA8260" Ref="R?"  Part="1" 
AR Path="/5ABD507D/60AA8260" Ref="R?"  Part="1" 
AR Path="/5FA6069E/60AA8260" Ref="R208"  Part="1" 
AR Path="/60AEF199/60AA8260" Ref="R1508"  Part="1" 
AR Path="/60B05AE5/60AA8260" Ref="R1608"  Part="1" 
AR Path="/60B05AFF/60AA8260" Ref="R1708"  Part="1" 
F 0 "R1708" V 5880 3900 50  0000 C CNN
F 1 "2.2kΩ" V 5700 3900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5730 3900 50  0001 C CNN
F 3 "" H 5800 3900 50  0000 C CNN
	1    5800 3900
	0    -1   -1   0   
$EndComp
Text GLabel 5950 3900 2    60   Input ~ 0
AVCC
Text HLabel 5950 4100 2    60   Output ~ 0
OUT3
Text GLabel 5550 4300 2    60   Input ~ 0
AGND
Wire Wire Line
	5550 4100 5600 4100
Wire Wire Line
	5650 3900 5600 3900
Wire Wire Line
	5600 3900 5600 4100
Connection ~ 5600 4100
$Comp
L Device:R R?
U 1 1 60AAE7AB
P 5800 3200
AR Path="/5AC88AD6/60AAE7AB" Ref="R?"  Part="1" 
AR Path="/5ABD2F13/60AAE7AB" Ref="R?"  Part="1" 
AR Path="/5ABD507D/60AAE7AB" Ref="R?"  Part="1" 
AR Path="/5FA6069E/60AAE7AB" Ref="R207"  Part="1" 
AR Path="/60AEF199/60AAE7AB" Ref="R1507"  Part="1" 
AR Path="/60B05AE5/60AAE7AB" Ref="R1607"  Part="1" 
AR Path="/60B05AFF/60AAE7AB" Ref="R1707"  Part="1" 
F 0 "R1707" V 5880 3200 50  0000 C CNN
F 1 "2.2kΩ" V 5700 3200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5730 3200 50  0001 C CNN
F 3 "" H 5800 3200 50  0000 C CNN
	1    5800 3200
	0    -1   -1   0   
$EndComp
Text GLabel 5950 3200 2    60   Input ~ 0
AVCC
Text HLabel 5950 3400 2    60   Output ~ 0
OUT2
Text GLabel 5550 3600 2    60   Input ~ 0
AGND
Wire Wire Line
	5550 3400 5600 3400
Wire Wire Line
	5650 3200 5600 3200
Wire Wire Line
	5600 3200 5600 3400
Connection ~ 5600 3400
Connection ~ 5600 2700
Wire Wire Line
	5600 2500 5600 2700
Wire Wire Line
	5650 2500 5600 2500
Wire Wire Line
	5550 2700 5600 2700
Text GLabel 5550 2900 2    60   Input ~ 0
AGND
Text HLabel 5950 2700 2    60   Output ~ 0
OUT1
Text GLabel 5950 2500 2    60   Input ~ 0
AVCC
$Comp
L Device:R R?
U 1 1 60AABCB7
P 5800 2500
AR Path="/5AC88AD6/60AABCB7" Ref="R?"  Part="1" 
AR Path="/5ABD2F13/60AABCB7" Ref="R?"  Part="1" 
AR Path="/5ABD507D/60AABCB7" Ref="R?"  Part="1" 
AR Path="/5FA6069E/60AABCB7" Ref="R206"  Part="1" 
AR Path="/60AEF199/60AABCB7" Ref="R1506"  Part="1" 
AR Path="/60B05AE5/60AABCB7" Ref="R1606"  Part="1" 
AR Path="/60B05AFF/60AABCB7" Ref="R1706"  Part="1" 
F 0 "R1706" V 5880 2500 50  0000 C CNN
F 1 "2.2kΩ" V 5700 2500 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5730 2500 50  0001 C CNN
F 3 "" H 5800 2500 50  0000 C CNN
	1    5800 2500
	0    -1   -1   0   
$EndComp
Text GLabel 4950 2900 0    60   Input ~ 0
DGND
$Comp
L Device:R R?
U 1 1 60A99628
P 4800 2700
AR Path="/5AC88AD6/60A99628" Ref="R?"  Part="1" 
AR Path="/5ABD2F13/60A99628" Ref="R?"  Part="1" 
AR Path="/5ABD507D/60A99628" Ref="R?"  Part="1" 
AR Path="/5FA6069E/60A99628" Ref="R202"  Part="1" 
AR Path="/60AEF199/60A99628" Ref="R1502"  Part="1" 
AR Path="/60B05AE5/60A99628" Ref="R1602"  Part="1" 
AR Path="/60B05AFF/60A99628" Ref="R1702"  Part="1" 
F 0 "R1702" V 4900 2700 50  0000 C CNN
F 1 "330Ω" V 4700 2700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4730 2700 50  0001 C CNN
F 3 "" H 4800 2700 50  0000 C CNN
	1    4800 2700
	0    -1   -1   0   
$EndComp
Connection ~ 5600 2000
Wire Wire Line
	5600 1800 5600 2000
Wire Wire Line
	5650 1800 5600 1800
Wire Wire Line
	5550 2000 5600 2000
Text GLabel 5550 2200 2    60   Input ~ 0
AGND
Text HLabel 5950 2000 2    60   Output ~ 0
OUT0
Text GLabel 4950 2200 0    60   Input ~ 0
DGND
Text GLabel 5950 1800 2    60   Input ~ 0
AVCC
$Comp
L Device:R R?
U 1 1 60A476BC
P 5800 1800
AR Path="/5AC88AD6/60A476BC" Ref="R?"  Part="1" 
AR Path="/5ABD2F13/60A476BC" Ref="R?"  Part="1" 
AR Path="/5ABD507D/60A476BC" Ref="R?"  Part="1" 
AR Path="/5FA6069E/60A476BC" Ref="R205"  Part="1" 
AR Path="/60AEF199/60A476BC" Ref="R1505"  Part="1" 
AR Path="/60B05AE5/60A476BC" Ref="R1605"  Part="1" 
AR Path="/60B05AFF/60A476BC" Ref="R1705"  Part="1" 
F 0 "R1705" V 5880 1800 50  0000 C CNN
F 1 "2.2kΩ" V 5700 1800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5730 1800 50  0001 C CNN
F 3 "" H 5800 1800 50  0000 C CNN
	1    5800 1800
	0    -1   -1   0   
$EndComp
$Comp
L Isolator:LTV-847S U201
U 2 1 60A40C69
P 5250 2800
AR Path="/5FA6069E/60A40C69" Ref="U201"  Part="2" 
AR Path="/60AEF199/60A40C69" Ref="U1501"  Part="2" 
AR Path="/60B05AE5/60A40C69" Ref="U1601"  Part="2" 
AR Path="/60B05AFF/60A40C69" Ref="U1701"  Part="2" 
F 0 "U1701" H 5250 3125 50  0000 C CNN
F 1 "LTV-847S" H 5250 3034 50  0000 C CNN
F 2 "Package_DIP:SMDIP-16_W9.53mm" H 5250 2500 50  0001 C CNN
F 3 "http://www.us.liteon.com/downloads/LTV-817-827-847.PDF" H 4650 3250 50  0001 C CNN
	2    5250 2800
	1    0    0    -1  
$EndComp
$Comp
L Isolator:LTV-847S U201
U 1 1 60A3F951
P 5250 2100
AR Path="/5FA6069E/60A3F951" Ref="U201"  Part="1" 
AR Path="/60AEF199/60A3F951" Ref="U1501"  Part="1" 
AR Path="/60B05AE5/60A3F951" Ref="U1601"  Part="1" 
AR Path="/60B05AFF/60A3F951" Ref="U1701"  Part="1" 
F 0 "U1701" H 5250 2425 50  0000 C CNN
F 1 "LTV-847S" H 5250 2334 50  0000 C CNN
F 2 "Package_DIP:SMDIP-16_W9.53mm" H 5250 1800 50  0001 C CNN
F 3 "http://www.us.liteon.com/downloads/LTV-817-827-847.PDF" H 4650 2550 50  0001 C CNN
	1    5250 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5FA65CEA
P 4800 2000
AR Path="/5AC88AD6/5FA65CEA" Ref="R?"  Part="1" 
AR Path="/5ABD2F13/5FA65CEA" Ref="R?"  Part="1" 
AR Path="/5ABD507D/5FA65CEA" Ref="R?"  Part="1" 
AR Path="/5FA6069E/5FA65CEA" Ref="R201"  Part="1" 
AR Path="/60AEF199/5FA65CEA" Ref="R1501"  Part="1" 
AR Path="/60B05AE5/5FA65CEA" Ref="R1601"  Part="1" 
AR Path="/60B05AFF/5FA65CEA" Ref="R1701"  Part="1" 
F 0 "R1701" V 4900 2000 50  0000 C CNN
F 1 "330Ω" V 4700 2000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4730 2000 50  0001 C CNN
F 3 "" H 4800 2000 50  0000 C CNN
	1    4800 2000
	0    -1   -1   0   
$EndComp
Text HLabel 4650 4100 0    60   Input ~ 0
IN3
Wire Wire Line
	5950 2700 5600 2700
Wire Wire Line
	5950 3400 5600 3400
Wire Wire Line
	5950 4100 5600 4100
Wire Wire Line
	5600 2000 5950 2000
$EndSCHEMATC
