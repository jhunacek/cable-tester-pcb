EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 17
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
NoConn ~ 10050 1100
NoConn ~ 10050 1200
NoConn ~ 10050 1300
NoConn ~ 8150 1600
NoConn ~ 8150 1700
NoConn ~ 8150 1900
NoConn ~ 8150 2200
NoConn ~ 9400 3000
NoConn ~ 8900 3000
Wire Wire Line
	10050 1500 10800 1500
Text Label 10150 1500 0    60   ~ 0
LED_STATUS
Wire Wire Line
	10050 1600 10800 1600
Text Label 10150 1600 0    60   ~ 0
MUX_A_A4
Wire Wire Line
	10050 1700 10800 1700
Text Label 10150 1700 0    60   ~ 0
MUX_A_A3
Wire Wire Line
	10050 1800 10800 1800
Text Label 10150 1800 0    60   ~ 0
MUX_A_A2
Wire Wire Line
	10050 1900 10800 1900
Text Label 10150 1900 0    60   ~ 0
MUX_A_A1
Wire Wire Line
	10050 2200 10800 2200
Text Label 10150 2200 0    60   ~ 0
MUX_B_A7
Wire Wire Line
	10050 2300 10800 2300
Text Label 10150 2300 0    60   ~ 0
MUX_B_A6
Wire Wire Line
	10050 2400 10800 2400
Text Label 10150 2400 0    60   ~ 0
MUX_B_A5
Wire Wire Line
	10050 2500 10800 2500
Text Label 10150 2500 0    60   ~ 0
MUX_B_A4
Wire Wire Line
	10050 2600 10800 2600
Text Label 10150 2600 0    60   ~ 0
MUX_B_A3
Wire Wire Line
	10050 2700 10800 2700
Text Label 10150 2700 0    60   ~ 0
MUX_B_A2
Wire Wire Line
	10050 2000 10800 2000
Text Label 10150 2000 0    60   ~ 0
MUX_A_A0
Wire Wire Line
	10050 2800 10800 2800
Text Label 10150 2800 0    60   ~ 0
MUX_B_A1
Wire Wire Line
	10050 2900 10800 2900
Text Label 10150 2900 0    60   ~ 0
MUX_B_A0
Wire Wire Line
	8150 2500 7400 2500
Text Label 8050 2500 2    60   ~ 0
MUX_A_A6
Wire Wire Line
	8150 2400 7400 2400
Text Label 8050 2400 2    60   ~ 0
MUX_A_A5
Text Label 8050 2600 2    60   ~ 0
MUX_A_A7
Text GLabel 1250 3050 0    60   Input ~ 0
CH1:10
Text GLabel 2050 3050 2    60   Input ~ 0
CH1:11
Text GLabel 1250 3150 0    60   Input ~ 0
CH1:12
Text GLabel 2050 3150 2    60   Input ~ 0
CH1:13
Text GLabel 1250 3250 0    60   Input ~ 0
CH1:14
Text GLabel 2050 3250 2    60   Input ~ 0
CH1:15
Text GLabel 1250 3350 0    60   Input ~ 0
CH1:16
Text GLabel 2050 3350 2    60   Input ~ 0
CH1:17
Text GLabel 1250 3450 0    60   Input ~ 0
CH1:18
Text GLabel 2050 3450 2    60   Input ~ 0
CH1:19
Text GLabel 1250 3550 0    60   Input ~ 0
CH1:20
Text GLabel 2050 3550 2    60   Input ~ 0
CH1:21
Text GLabel 1250 3650 0    60   Input ~ 0
CH1:22
Text GLabel 2050 3650 2    60   Input ~ 0
CH1:23
Text GLabel 1250 3750 0    60   Input ~ 0
CH1:24
Text GLabel 2050 3750 2    60   Input ~ 0
CH1:25
Text GLabel 1250 3850 0    60   Input ~ 0
CH1:26
Text GLabel 2050 3850 2    60   Input ~ 0
CH1:27
Text GLabel 1250 3950 0    60   Input ~ 0
CH1:28
Text GLabel 2050 3950 2    60   Input ~ 0
CH1:29
Text GLabel 1250 2550 0    60   Input ~ 0
CH1:00
Text GLabel 2050 2550 2    60   Input ~ 0
CH1:01
Text GLabel 1250 2650 0    60   Input ~ 0
CH1:02
Text GLabel 2050 2650 2    60   Input ~ 0
CH1:03
Text GLabel 1250 2750 0    60   Input ~ 0
CH1:04
Text GLabel 2050 2750 2    60   Input ~ 0
CH1:05
Text GLabel 1250 2850 0    60   Input ~ 0
CH1:06
Text GLabel 2050 2850 2    60   Input ~ 0
CH1:07
Text GLabel 1250 2950 0    60   Input ~ 0
CH1:08
Text GLabel 2050 2950 2    60   Input ~ 0
CH1:09
Text GLabel 1250 4050 0    60   Input ~ 0
CH1:30
Text GLabel 2050 4050 2    60   Input ~ 0
CH1:31
Text GLabel 4550 3550 2    60   Input ~ 0
CH2:10
Text GLabel 3750 3550 0    60   Input ~ 0
CH2:11
Text GLabel 4550 3450 2    60   Input ~ 0
CH2:12
Text GLabel 3750 3450 0    60   Input ~ 0
CH2:13
Text GLabel 4550 3350 2    60   Input ~ 0
CH2:14
Text GLabel 3750 3350 0    60   Input ~ 0
CH2:15
Text GLabel 4550 3250 2    60   Input ~ 0
CH2:16
Text GLabel 3750 3250 0    60   Input ~ 0
CH2:17
Text GLabel 4550 3150 2    60   Input ~ 0
CH2:18
Text GLabel 3750 3150 0    60   Input ~ 0
CH2:19
Text GLabel 4550 3050 2    60   Input ~ 0
CH2:20
Text GLabel 3750 3050 0    60   Input ~ 0
CH2:21
Text GLabel 4550 2950 2    60   Input ~ 0
CH2:22
Text GLabel 3750 2950 0    60   Input ~ 0
CH2:23
Text GLabel 4550 2850 2    60   Input ~ 0
CH2:24
Text GLabel 3750 2850 0    60   Input ~ 0
CH2:25
Text GLabel 4550 2750 2    60   Input ~ 0
CH2:26
Text GLabel 3750 2750 0    60   Input ~ 0
CH2:27
Text GLabel 4550 2650 2    60   Input ~ 0
CH2:28
Text GLabel 3750 2650 0    60   Input ~ 0
CH2:29
Text GLabel 4550 4050 2    60   Input ~ 0
CH2:00
Text GLabel 3750 4050 0    60   Input ~ 0
CH2:01
Text GLabel 4550 3950 2    60   Input ~ 0
CH2:02
Text GLabel 3750 3950 0    60   Input ~ 0
CH2:03
Text GLabel 4550 3850 2    60   Input ~ 0
CH2:04
Text GLabel 3750 3850 0    60   Input ~ 0
CH2:05
Text GLabel 4550 3750 2    60   Input ~ 0
CH2:06
Text GLabel 3750 3750 0    60   Input ~ 0
CH2:07
Text GLabel 4550 3650 2    60   Input ~ 0
CH2:08
Text GLabel 3750 3650 0    60   Input ~ 0
CH2:09
Text GLabel 4550 2550 2    60   Input ~ 0
CH2:30
Text GLabel 3750 2550 0    60   Input ~ 0
CH2:31
Text GLabel 4550 1950 2    60   Input ~ 0
CH3:10
Text GLabel 3750 1950 0    60   Input ~ 0
CH3:11
Text GLabel 4550 1850 2    60   Input ~ 0
CH3:12
Text GLabel 3750 1850 0    60   Input ~ 0
CH3:13
Text GLabel 4550 1750 2    60   Input ~ 0
CH3:14
Text GLabel 3750 1750 0    60   Input ~ 0
CH3:15
Text GLabel 4550 1650 2    60   Input ~ 0
CH3:16
Text GLabel 3750 1650 0    60   Input ~ 0
CH3:17
Text GLabel 4550 1550 2    60   Input ~ 0
CH3:18
Text GLabel 3750 1550 0    60   Input ~ 0
CH3:19
Text GLabel 4550 1450 2    60   Input ~ 0
CH3:20
Text GLabel 3750 1450 0    60   Input ~ 0
CH3:21
Text GLabel 4550 1350 2    60   Input ~ 0
CH3:22
Text GLabel 3750 1350 0    60   Input ~ 0
CH3:23
Text GLabel 4550 1250 2    60   Input ~ 0
CH3:24
Text GLabel 3750 1250 0    60   Input ~ 0
CH3:25
Text GLabel 4550 1150 2    60   Input ~ 0
CH3:26
Text GLabel 3750 1150 0    60   Input ~ 0
CH3:27
Text GLabel 4550 1050 2    60   Input ~ 0
CH3:28
Text GLabel 3750 1050 0    60   Input ~ 0
CH3:29
Text GLabel 4550 2450 2    60   Input ~ 0
CH3:00
Text GLabel 3750 2450 0    60   Input ~ 0
CH3:01
Text GLabel 4550 2350 2    60   Input ~ 0
CH3:02
Text GLabel 3750 2350 0    60   Input ~ 0
CH3:03
Text GLabel 4550 2250 2    60   Input ~ 0
CH3:04
Text GLabel 3750 2250 0    60   Input ~ 0
CH3:05
Text GLabel 4550 2150 2    60   Input ~ 0
CH3:06
Text GLabel 3750 2150 0    60   Input ~ 0
CH3:07
Text GLabel 4550 2050 2    60   Input ~ 0
CH3:08
Text GLabel 3750 2050 0    60   Input ~ 0
CH3:09
Text GLabel 4550 950  2    60   Input ~ 0
CH3:30
Text GLabel 3750 950  0    60   Input ~ 0
CH3:31
Text GLabel 1250 1450 0    60   Input ~ 0
CH0:10
Text GLabel 2050 1450 2    60   Input ~ 0
CH0:11
Text GLabel 1250 1550 0    60   Input ~ 0
CH0:12
Text GLabel 2050 1550 2    60   Input ~ 0
CH0:13
Text GLabel 1250 1650 0    60   Input ~ 0
CH0:14
Text GLabel 2050 1650 2    60   Input ~ 0
CH0:15
Text GLabel 1250 1750 0    60   Input ~ 0
CH0:16
Text GLabel 2050 1750 2    60   Input ~ 0
CH0:17
Text GLabel 1250 1850 0    60   Input ~ 0
CH0:18
Text GLabel 2050 1850 2    60   Input ~ 0
CH0:19
Text GLabel 1250 1950 0    60   Input ~ 0
CH0:20
Text GLabel 2050 1950 2    60   Input ~ 0
CH0:21
Text GLabel 1250 2050 0    60   Input ~ 0
CH0:22
Text GLabel 2050 2050 2    60   Input ~ 0
CH0:23
Text GLabel 1250 2150 0    60   Input ~ 0
CH0:24
Text GLabel 2050 2150 2    60   Input ~ 0
CH0:25
Text GLabel 1250 2250 0    60   Input ~ 0
CH0:26
Text GLabel 2050 2250 2    60   Input ~ 0
CH0:27
Text GLabel 1250 2350 0    60   Input ~ 0
CH0:28
Text GLabel 2050 2350 2    60   Input ~ 0
CH0:29
Text GLabel 1250 950  0    60   Input ~ 0
CH0:00
Text GLabel 2050 950  2    60   Input ~ 0
CH0:01
Text GLabel 1250 1050 0    60   Input ~ 0
CH0:02
Text GLabel 2050 1050 2    60   Input ~ 0
CH0:03
Text GLabel 1250 1150 0    60   Input ~ 0
CH0:04
Text GLabel 2050 1150 2    60   Input ~ 0
CH0:05
Text GLabel 1250 1250 0    60   Input ~ 0
CH0:06
Text GLabel 2050 1250 2    60   Input ~ 0
CH0:07
Text GLabel 1250 1350 0    60   Input ~ 0
CH0:08
Text GLabel 2050 1350 2    60   Input ~ 0
CH0:09
Text GLabel 1250 2450 0    60   Input ~ 0
CH0:30
Text GLabel 2050 2450 2    60   Input ~ 0
CH0:31
$Comp
L cable-tester-rescue:LED D101
U 1 1 5763200C
P 6150 3300
F 0 "D101" H 6150 3400 50  0000 C CNN
F 1 "LED" H 6150 3200 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 6150 3300 50  0001 C CNN
F 3 "" H 6150 3300 50  0000 C CNN
	1    6150 3300
	-1   0    0    1   
$EndComp
Text Label 5900 3300 2    60   ~ 0
LED_STATUS
$Comp
L cable-tester-rescue:R R101
U 1 1 576338E2
P 6500 3300
F 0 "R101" V 6580 3300 50  0000 C CNN
F 1 "330Ω" V 6500 3300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6430 3300 50  0001 C CNN
F 3 "" H 6500 3300 50  0000 C CNN
	1    6500 3300
	0    1    1    0   
$EndComp
$Comp
L cable-tester-rescue:TEST_1P J104
U 1 1 576AD011
P 1250 5250
F 0 "J104" H 1250 5520 50  0000 C CNN
F 1 "AVSS" H 1250 5450 50  0000 C CNN
F 2 "banana-jack-pcb:deltron-571-0X00" H 1450 5250 50  0001 C CNN
F 3 "" H 1450 5250 50  0000 C CNN
	1    1250 5250
	0    -1   -1   0   
$EndComp
$Comp
L cable-tester-rescue:TEST_1P J108
U 1 1 576AE1D7
P 5800 1450
F 0 "J108" H 5800 1720 50  0000 C CNN
F 1 "CH127" H 5800 1650 50  0000 C CNN
F 2 "banana-jack-pcb:deltron-571-0X00" H 6000 1450 50  0001 C CNN
F 3 "" H 6000 1450 50  0000 C CNN
	1    5800 1450
	0    -1   -1   0   
$EndComp
$Comp
L cable-tester-rescue:TEST_1P J107
U 1 1 576AE14B
P 5800 1150
F 0 "J107" H 5800 1420 50  0000 C CNN
F 1 "CH126" H 5800 1350 50  0000 C CNN
F 2 "banana-jack-pcb:deltron-571-0X00" H 6000 1150 50  0001 C CNN
F 3 "" H 6000 1150 50  0000 C CNN
	1    5800 1150
	0    -1   -1   0   
$EndComp
Text GLabel 5800 1450 2    60   Input ~ 0
CH3:31
Text GLabel 5800 1150 2    60   Input ~ 0
CH3:30
Wire Wire Line
	8150 2600 7400 2600
NoConn ~ 8150 1500
NoConn ~ 9100 3000
NoConn ~ 9200 3000
NoConn ~ 9300 3000
Text GLabel 1250 5250 2    60   Input ~ 0
AVSS
Text GLabel 6650 3300 2    60   Input ~ 0
DGND
$Sheet
S 3000 6200 650  500 
U 5FA6069E
F0 "Sheet5FA6069D" 60
F1 "optoisol.sch" 60
F2 "OUT0" O L 3000 6300 60 
F3 "OUT1" O L 3000 6400 60 
F4 "IN0" I R 3650 6300 60 
F5 "IN1" I R 3650 6400 60 
F6 "IN2" I R 3650 6500 60 
F7 "OUT3" O L 3000 6600 60 
F8 "OUT2" O L 3000 6500 60 
F9 "IN3" I R 3650 6600 60 
$EndSheet
Text GLabel 10800 1400 2    60   Input ~ 0
DGND
Wire Wire Line
	10800 1400 10050 1400
Text GLabel 8150 2100 0    60   Input ~ 0
DGND
Text GLabel 8150 2000 0    60   Input ~ 0
DGND
$Comp
L cable-tester-rescue:TEST_1P J103
U 1 1 60729C75
P 1250 5000
F 0 "J103" H 1250 5270 50  0000 C CNN
F 1 "AGND" H 1250 5200 50  0000 C CNN
F 2 "banana-jack-pcb:deltron-571-0X00" H 1450 5000 50  0001 C CNN
F 3 "" H 1450 5000 50  0000 C CNN
	1    1250 5000
	0    -1   -1   0   
$EndComp
Text GLabel 1250 5000 2    60   Input ~ 0
AGND
$Comp
L cable-tester-rescue:TEST_1P J102
U 1 1 6072CF7A
P 1250 4750
F 0 "J102" H 1250 5020 50  0000 C CNN
F 1 "AVCC" H 1250 4950 50  0000 C CNN
F 2 "banana-jack-pcb:deltron-571-0X00" H 1450 4750 50  0001 C CNN
F 3 "" H 1450 4750 50  0000 C CNN
	1    1250 4750
	0    -1   -1   0   
$EndComp
Text GLabel 1250 4750 2    60   Input ~ 0
AVCC
Text Notes 1800 5300 0    60   ~ 0
Single Supply:\nAVCC = 1.8V to 5.5V\nAVSS = 0V\n\nDual Supply:\nAVCC = 2.25 to 2.75V\nAVSS = -2.25 to -2.75V
Text GLabel 8150 1800 0    60   Input ~ 0
DVCC
Text Notes 5250 1950 0    60   ~ 0
Power Input Bypass Jumpers\n(To Use Arduino Power)
Text GLabel 6250 2350 2    60   Input ~ 0
AVCC
Text GLabel 6250 2250 2    60   Input ~ 0
DVCC
$Comp
L Connector:Conn_01x02_Male J109
U 1 1 607AD720
P 6050 2250
F 0 "J109" H 6200 2400 50  0000 C CNN
F 1 "Jumper" H 6200 2050 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 6050 2250 50  0001 C CNN
F 3 "~" H 6050 2250 50  0001 C CNN
	1    6050 2250
	1    0    0    -1  
$EndComp
Text GLabel 5550 2350 2    60   Input ~ 0
AGND
Text GLabel 5550 2250 2    60   Input ~ 0
DGND
$Comp
L Connector:Conn_01x02_Male J106
U 1 1 607A3422
P 5350 2250
F 0 "J106" H 5500 2400 50  0000 C CNN
F 1 "Jumper" H 5500 2050 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 5350 2250 50  0001 C CNN
F 3 "~" H 5350 2250 50  0001 C CNN
	1    5350 2250
	1    0    0    -1  
$EndComp
Text Notes 800  4500 0    60   ~ 0
Mux Power Inputs
Text Notes 5400 900  0    60   ~ 0
For System Monitor
Text Label 3550 5700 0    60   ~ 0
ISOL_A_A7
Text Label 3550 5600 0    60   ~ 0
ISOL_A_A6
Text Label 3550 5500 0    60   ~ 0
ISOL_A_A5
Text Label 3550 5400 0    60   ~ 0
ISOL_A_A4
Text Label 3550 5300 0    60   ~ 0
ISOL_A_A3
Text Label 3550 5200 0    60   ~ 0
ISOL_A_A2
Text Label 3550 5100 0    60   ~ 0
ISOL_A_A1
Text Label 3550 5000 0    60   ~ 0
ISOL_A_A0
$Sheet
S 4050 4900 550  900 
U 6024D6F3
F0 "mux-tree" 60
F1 "mux-tree.sch" 60
F2 "MUX_A4" I L 4050 5400 60 
F3 "MUX_A3" I L 4050 5300 60 
F4 "MUX_A6" I L 4050 5600 60 
F5 "MUX_A5" I L 4050 5500 60 
F6 "MUX_A1" I L 4050 5100 60 
F7 "MUX_A2" I L 4050 5200 60 
F8 "MUX_A0" I L 4050 5000 60 
F9 "MUX_A7" I L 4050 5700 60 
$EndSheet
Text Label 5050 5000 0    60   ~ 0
ISOL_B_A0
Wire Wire Line
	5550 5000 5000 5000
Text Label 5050 5100 0    60   ~ 0
ISOL_B_A1
Wire Wire Line
	5550 5100 5000 5100
Text Label 5050 5200 0    60   ~ 0
ISOL_B_A2
Wire Wire Line
	5550 5200 5000 5200
Text Label 5050 5300 0    60   ~ 0
ISOL_B_A3
Wire Wire Line
	5550 5300 5000 5300
Text Label 5050 5400 0    60   ~ 0
ISOL_B_A4
Wire Wire Line
	5550 5400 5000 5400
Text Label 5050 5500 0    60   ~ 0
ISOL_B_A5
Wire Wire Line
	5550 5500 5000 5500
Text Label 5050 5600 0    60   ~ 0
ISOL_B_A6
Wire Wire Line
	5550 5600 5000 5600
Text Label 5050 5700 0    60   ~ 0
ISOL_B_A7
Wire Wire Line
	5550 5700 5000 5700
$Sheet
S 5550 4900 550  900 
U 6056E1C8
F0 "sheet6056E1C8" 60
F1 "mux-tree.sch" 60
F2 "MUX_A4" I L 5550 5400 60 
F3 "MUX_A3" I L 5550 5300 60 
F4 "MUX_A6" I L 5550 5600 60 
F5 "MUX_A5" I L 5550 5500 60 
F6 "MUX_A1" I L 5550 5100 60 
F7 "MUX_A2" I L 5550 5200 60 
F8 "MUX_A0" I L 5550 5000 60 
F9 "MUX_A7" I L 5550 5700 60 
$EndSheet
Wire Wire Line
	4050 5000 3500 5000
Wire Wire Line
	4050 5100 3500 5100
Wire Wire Line
	4050 5200 3500 5200
Wire Wire Line
	4050 5300 3500 5300
Wire Wire Line
	4050 5400 3500 5400
Wire Wire Line
	4050 5500 3500 5500
Wire Wire Line
	4050 5600 3500 5600
Wire Wire Line
	4050 5700 3500 5700
$Comp
L cable-tester-rescue:connector-header-128 J105
U 1 1 575B555A
P 1450 950
F 0 "J105" H 2900 1100 60  0000 C CNN
F 1 "connector-header-128" H 2900 1200 60  0000 C CNN
F 2 "cable-tester:connector-header-128" H 1450 950 60  0001 C CNN
F 3 "" H 1450 950 60  0000 C CNN
	1    1450 950 
	1    0    0    -1  
$EndComp
Text Notes 4550 4600 0    60   ~ 0
Main Mux Trees
Text GLabel 10700 5250 2    60   Input ~ 0
AGND
Text GLabel 10700 5450 2    60   Input ~ 0
AGND
$Comp
L Mechanical:MountingHole_Pad H101
U 1 1 6083ED38
P 10600 5250
F 0 "H101" V 10600 5500 50  0000 C CNN
F 1 "MountingHole_Pad" V 10746 5253 50  0001 C CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 10600 5250 50  0001 C CNN
F 3 "~" H 10600 5250 50  0001 C CNN
	1    10600 5250
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H102
U 1 1 60840C47
P 10600 5450
F 0 "H102" V 10600 5700 50  0000 C CNN
F 1 "MountingHole_Pad" V 10746 5453 50  0001 C CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 10600 5450 50  0001 C CNN
F 3 "~" H 10600 5450 50  0001 C CNN
	1    10600 5450
	0    -1   -1   0   
$EndComp
Text GLabel 10700 5650 2    60   Input ~ 0
AGND
Text GLabel 10700 5850 2    60   Input ~ 0
AGND
$Comp
L Mechanical:MountingHole_Pad H103
U 1 1 60842E15
P 10600 5650
F 0 "H103" V 10600 5900 50  0000 C CNN
F 1 "MountingHole_Pad" V 10746 5653 50  0001 C CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 10600 5650 50  0001 C CNN
F 3 "~" H 10600 5650 50  0001 C CNN
	1    10600 5650
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H104
U 1 1 60842E1F
P 10600 5850
F 0 "H104" V 10600 6100 50  0000 C CNN
F 1 "MountingHole_Pad" V 10746 5853 50  0001 C CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 10600 5850 50  0001 C CNN
F 3 "~" H 10600 5850 50  0001 C CNN
	1    10600 5850
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H105
U 1 1 608460F3
P 10600 6050
F 0 "H105" V 10600 6300 50  0000 C CNN
F 1 "MountingHole_Pad" V 10746 6053 50  0001 C CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 10600 6050 50  0001 C CNN
F 3 "~" H 10600 6050 50  0001 C CNN
	1    10600 6050
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H106
U 1 1 608460FD
P 10600 6250
F 0 "H106" V 10600 6500 50  0000 C CNN
F 1 "MountingHole_Pad" V 10746 6253 50  0001 C CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380" H 10600 6250 50  0001 C CNN
F 3 "~" H 10600 6250 50  0001 C CNN
	1    10600 6250
	0    -1   -1   0   
$EndComp
NoConn ~ 10700 6250
Text Notes 10800 6350 0    60   ~ 0
Digital\nSide
Wire Wire Line
	8200 4200 7450 4200
Text Label 8100 4200 2    60   ~ 0
MUX_B_A7
Wire Wire Line
	8200 4100 7450 4100
Text Label 8100 4100 2    60   ~ 0
MUX_B_A6
Wire Wire Line
	8200 4000 7450 4000
Text Label 8100 4000 2    60   ~ 0
MUX_B_A5
Wire Wire Line
	8200 3900 7450 3900
Text Label 8100 3900 2    60   ~ 0
MUX_B_A4
Wire Wire Line
	8200 3800 7450 3800
Text Label 8100 3800 2    60   ~ 0
MUX_B_A3
Wire Wire Line
	8200 3700 7450 3700
Text Label 8100 3700 2    60   ~ 0
MUX_B_A2
Wire Wire Line
	8200 3600 7450 3600
Text Label 8100 3600 2    60   ~ 0
MUX_B_A1
Wire Wire Line
	8200 3500 7450 3500
Text Label 8100 3500 2    60   ~ 0
MUX_B_A0
Wire Wire Line
	8200 4800 7450 4800
Text Label 8100 4800 2    60   ~ 0
LED_STATUS
Wire Wire Line
	8200 4700 7450 4700
Text Label 8100 4700 2    60   ~ 0
MUX_A_A4
Wire Wire Line
	8200 4600 7450 4600
Text Label 8100 4600 2    60   ~ 0
MUX_A_A3
Wire Wire Line
	8200 4500 7450 4500
Text Label 8100 4500 2    60   ~ 0
MUX_A_A2
Wire Wire Line
	8200 4400 7450 4400
Text Label 8100 4400 2    60   ~ 0
MUX_A_A1
Wire Wire Line
	8200 4300 7450 4300
Text Label 8100 4300 2    60   ~ 0
MUX_A_A0
Text GLabel 10000 3800 2    60   Input ~ 0
DGND
Text GLabel 10000 4300 2    60   Input ~ 0
DGND
Text GLabel 10000 3600 2    60   Input ~ 0
DVCC
NoConn ~ 10000 4000
NoConn ~ 10000 4500
NoConn ~ 10000 3500
NoConn ~ 10000 3700
Wire Wire Line
	8200 5000 7450 5000
Text Label 8100 5000 2    60   ~ 0
MUX_A_A6
Wire Wire Line
	8200 4900 7450 4900
Text Label 8100 4900 2    60   ~ 0
MUX_A_A5
Text Label 8100 5100 2    60   ~ 0
MUX_A_A7
Wire Wire Line
	8200 5100 7450 5100
$Comp
L Teensy_3_Series_Board_v1.0:TEENSY_3.1_BASIC M102
U 1 1 6085349C
P 9100 4600
F 0 "M102" H 9100 6054 45  0000 C CNN
F 1 "Teensy 3.x" H 9100 5970 45  0000 C CNN
F 2 "Teensy_3_Series_Board_v1.0:TEENSY_3.1_BASIC" H 9130 4750 20  0001 C CNN
F 3 "" H 9100 4600 60  0001 C CNN
	1    9100 4600
	1    0    0    -1  
$EndComp
NoConn ~ 8200 5200
NoConn ~ 8200 5300
NoConn ~ 8200 5400
NoConn ~ 8200 5500
NoConn ~ 8200 5600
NoConn ~ 8200 5700
NoConn ~ 8200 5800
Text Notes 8150 700  0    60   ~ 0
Allow Either Arduino or Teensy (Not Both)
Text Notes 10850 2000 0    60   ~ 0
LSB
Text Notes 10850 2200 0    60   ~ 0
MSB
Text Notes 10850 2900 0    60   ~ 0
LSB
Text Notes 7150 2600 0    60   ~ 0
MSB
Text Notes 7250 3500 0    60   ~ 0
LSB
Text Notes 7250 4200 0    60   ~ 0
MSB
Text Notes 7250 4300 0    60   ~ 0
LSB
Text Notes 7250 5100 0    60   ~ 0
MSB
Wire Wire Line
	5300 3300 5950 3300
$Comp
L cable-tester-rescue:LED D102
U 1 1 609E46C6
P 6150 3600
F 0 "D102" H 6150 3700 50  0000 C CNN
F 1 "LED" H 6150 3500 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 6150 3600 50  0001 C CNN
F 3 "" H 6150 3600 50  0000 C CNN
	1    6150 3600
	-1   0    0    1   
$EndComp
$Comp
L cable-tester-rescue:R R102
U 1 1 609E46D1
P 6500 3600
F 0 "R102" V 6580 3600 50  0000 C CNN
F 1 "330Ω" V 6500 3600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6430 3600 50  0001 C CNN
F 3 "" H 6500 3600 50  0000 C CNN
	1    6500 3600
	0    1    1    0   
$EndComp
Text GLabel 6650 3600 2    60   Input ~ 0
DGND
Text GLabel 5950 3600 0    60   Input ~ 0
DVCC
$Comp
L cable-tester-rescue:LED D103
U 1 1 609EB4F5
P 6150 3900
F 0 "D103" H 6150 4000 50  0000 C CNN
F 1 "LED" H 6150 3800 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 6150 3900 50  0001 C CNN
F 3 "" H 6150 3900 50  0000 C CNN
	1    6150 3900
	-1   0    0    1   
$EndComp
$Comp
L cable-tester-rescue:R R103
U 1 1 609EB4FF
P 6500 3900
F 0 "R103" V 6580 3900 50  0000 C CNN
F 1 "330Ω" V 6500 3900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6430 3900 50  0001 C CNN
F 3 "" H 6500 3900 50  0000 C CNN
	1    6500 3900
	0    1    1    0   
$EndComp
Text GLabel 6650 3900 2    60   Input ~ 0
AGND
Text GLabel 5950 3900 0    60   Input ~ 0
AVCC
$Comp
L cable-tester-rescue:LED D104
U 1 1 609F8870
P 6150 4200
F 0 "D104" H 6150 4300 50  0000 C CNN
F 1 "LED" H 6150 4100 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 6150 4200 50  0001 C CNN
F 3 "" H 6150 4200 50  0000 C CNN
	1    6150 4200
	-1   0    0    1   
$EndComp
$Comp
L cable-tester-rescue:R R104
U 1 1 609F887A
P 6500 4200
F 0 "R104" V 6580 4200 50  0000 C CNN
F 1 "330Ω" V 6500 4200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6430 4200 50  0001 C CNN
F 3 "" H 6500 4200 50  0000 C CNN
	1    6500 4200
	0    1    1    0   
$EndComp
Text GLabel 6650 4200 2    60   Input ~ 0
AVSS
Text GLabel 5950 4200 0    60   Input ~ 0
AGND
Text Notes 1600 4300 0    60   ~ 0
(Same breakout connector as original/mini cable-tester)
Text Label 2500 6600 0    60   ~ 0
ISOL_A_A3
Text Label 2500 6500 0    60   ~ 0
ISOL_A_A2
Text Label 2500 6400 0    60   ~ 0
ISOL_A_A1
Text Label 2500 6300 0    60   ~ 0
ISOL_A_A0
Wire Wire Line
	3000 6300 2450 6300
Wire Wire Line
	3000 6400 2450 6400
Wire Wire Line
	3000 6500 2450 6500
Wire Wire Line
	3000 6600 2450 6600
Wire Wire Line
	4250 6600 3650 6600
Text Label 4150 6600 2    60   ~ 0
MUX_A_A3
Wire Wire Line
	4250 6500 3650 6500
Text Label 4150 6500 2    60   ~ 0
MUX_A_A2
Wire Wire Line
	4250 6400 3650 6400
Text Label 4150 6400 2    60   ~ 0
MUX_A_A1
Wire Wire Line
	4250 6300 3650 6300
Text Label 4150 6300 2    60   ~ 0
MUX_A_A0
$Sheet
S 3000 6950 650  500 
U 60AEF199
F0 "sheet60AEF199" 60
F1 "optoisol.sch" 60
F2 "OUT0" O L 3000 7050 60 
F3 "OUT1" O L 3000 7150 60 
F4 "IN0" I R 3650 7050 60 
F5 "IN1" I R 3650 7150 60 
F6 "IN2" I R 3650 7250 60 
F7 "OUT3" O L 3000 7350 60 
F8 "OUT2" O L 3000 7250 60 
F9 "IN3" I R 3650 7350 60 
$EndSheet
Text Label 2500 7350 0    60   ~ 0
ISOL_A_A7
Text Label 2500 7250 0    60   ~ 0
ISOL_A_A6
Text Label 2500 7150 0    60   ~ 0
ISOL_A_A5
Text Label 2500 7050 0    60   ~ 0
ISOL_A_A4
Wire Wire Line
	3000 7050 2450 7050
Wire Wire Line
	3000 7150 2450 7150
Wire Wire Line
	3000 7250 2450 7250
Wire Wire Line
	3000 7350 2450 7350
Wire Wire Line
	4250 7350 3650 7350
Text Label 4150 7350 2    60   ~ 0
MUX_A_A7
Wire Wire Line
	4250 7250 3650 7250
Text Label 4150 7250 2    60   ~ 0
MUX_A_A6
Wire Wire Line
	4250 7150 3650 7150
Text Label 4150 7150 2    60   ~ 0
MUX_A_A5
Wire Wire Line
	4250 7050 3650 7050
Text Label 4150 7050 2    60   ~ 0
MUX_A_A4
$Sheet
S 5050 6200 650  500 
U 60B05AE5
F0 "sheet60B05AE5" 60
F1 "optoisol.sch" 60
F2 "OUT0" O L 5050 6300 60 
F3 "OUT1" O L 5050 6400 60 
F4 "IN0" I R 5700 6300 60 
F5 "IN1" I R 5700 6400 60 
F6 "IN2" I R 5700 6500 60 
F7 "OUT3" O L 5050 6600 60 
F8 "OUT2" O L 5050 6500 60 
F9 "IN3" I R 5700 6600 60 
$EndSheet
Text Label 4550 6600 0    60   ~ 0
ISOL_B_A3
Text Label 4550 6500 0    60   ~ 0
ISOL_B_A2
Text Label 4550 6400 0    60   ~ 0
ISOL_B_A1
Text Label 4550 6300 0    60   ~ 0
ISOL_B_A0
Wire Wire Line
	5050 6300 4500 6300
Wire Wire Line
	5050 6400 4500 6400
Wire Wire Line
	5050 6500 4500 6500
Wire Wire Line
	5050 6600 4500 6600
Wire Wire Line
	6300 6600 5700 6600
Text Label 6200 6600 2    60   ~ 0
MUX_B_A3
Wire Wire Line
	6300 6500 5700 6500
Text Label 6200 6500 2    60   ~ 0
MUX_B_A2
Wire Wire Line
	6300 6400 5700 6400
Text Label 6200 6400 2    60   ~ 0
MUX_B_A1
Wire Wire Line
	6300 6300 5700 6300
Text Label 6200 6300 2    60   ~ 0
MUX_B_A0
$Sheet
S 5050 6950 650  500 
U 60B05AFF
F0 "sheet60B05AFF" 60
F1 "optoisol.sch" 60
F2 "OUT0" O L 5050 7050 60 
F3 "OUT1" O L 5050 7150 60 
F4 "IN0" I R 5700 7050 60 
F5 "IN1" I R 5700 7150 60 
F6 "IN2" I R 5700 7250 60 
F7 "OUT3" O L 5050 7350 60 
F8 "OUT2" O L 5050 7250 60 
F9 "IN3" I R 5700 7350 60 
$EndSheet
Text Label 4550 7350 0    60   ~ 0
ISOL_B_A7
Text Label 4550 7250 0    60   ~ 0
ISOL_B_A6
Text Label 4550 7150 0    60   ~ 0
ISOL_B_A5
Text Label 4550 7050 0    60   ~ 0
ISOL_B_A4
Wire Wire Line
	5050 7050 4500 7050
Wire Wire Line
	5050 7150 4500 7150
Wire Wire Line
	5050 7250 4500 7250
Wire Wire Line
	5050 7350 4500 7350
Wire Wire Line
	6300 7350 5700 7350
Text Label 6200 7350 2    60   ~ 0
MUX_B_A7
Wire Wire Line
	6300 7250 5700 7250
Text Label 6200 7250 2    60   ~ 0
MUX_B_A6
Wire Wire Line
	6300 7150 5700 7150
Text Label 6200 7150 2    60   ~ 0
MUX_B_A5
Wire Wire Line
	6300 7050 5700 7050
Text Label 6200 7050 2    60   ~ 0
MUX_B_A4
Text Notes 5950 3050 0    60   ~ 0
Status LEDs
Wire Wire Line
	1350 6500 1050 6500
Wire Wire Line
	1050 6400 1350 6400
Text GLabel 1350 6400 2    60   Input ~ 0
AGND
Text GLabel 1350 6500 2    60   Input ~ 0
AVSS
$Comp
L Connector:Conn_01x02_Male J101
U 1 1 6075D0DF
P 850 6400
F 0 "J101" H 1000 6550 50  0000 C CNN
F 1 "Jumper" H 1000 6200 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 850 6400 50  0001 C CNN
F 3 "~" H 850 6400 50  0001 C CNN
	1    850  6400
	1    0    0    -1  
$EndComp
Text GLabel 1400 7400 2    60   Input ~ 0
AGND
Text GLabel 1100 7400 0    60   Input ~ 0
AVSS
$Comp
L cable-tester-rescue:C C102
U 1 1 607591B0
P 1250 7400
F 0 "C102" V 1100 7400 50  0000 C CNN
F 1 "47uF" V 1400 7400 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1365 7309 50  0001 L CNN
F 3 "" H 1250 7400 50  0000 C CNN
	1    1250 7400
	0    1    1    0   
$EndComp
Text GLabel 1400 7000 2    60   Input ~ 0
AGND
Text GLabel 1100 7000 0    60   Input ~ 0
AVCC
$Comp
L cable-tester-rescue:C C101
U 1 1 60754469
P 1250 7000
F 0 "C101" V 1100 7000 50  0000 C CNN
F 1 "47uF" V 1400 7000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1365 6909 50  0001 L CNN
F 3 "" H 1250 7000 50  0000 C CNN
	1    1250 7000
	0    1    1    0   
$EndComp
NoConn ~ 8150 2700
NoConn ~ 8150 2800
NoConn ~ 8150 2900
Wire Wire Line
	9000 3000 9000 3050
Wire Wire Line
	9000 3050 8450 3050
Text GLabel 8450 3050 0    60   Input ~ 0
DGND
Text GLabel 1200 5650 2    60   Input ~ 0
AVCC
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 60BA53DC
P 1200 5650
F 0 "#FLG0101" H 1200 5725 50  0001 C CNN
F 1 "PWR_FLAG" V 1200 5777 50  0000 L CNN
F 2 "" H 1200 5650 50  0001 C CNN
F 3 "~" H 1200 5650 50  0001 C CNN
	1    1200 5650
	0    -1   -1   0   
$EndComp
Text GLabel 1200 5800 2    60   Input ~ 0
AGND
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5F8B5034
P 1200 5800
F 0 "#FLG0102" H 1200 5875 50  0001 C CNN
F 1 "PWR_FLAG" V 1200 5927 50  0000 L CNN
F 2 "" H 1200 5800 50  0001 C CNN
F 3 "~" H 1200 5800 50  0001 C CNN
	1    1200 5800
	0    -1   -1   0   
$EndComp
Text GLabel 1200 5950 2    60   Input ~ 0
AVSS
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 5F8BBE15
P 1200 5950
F 0 "#FLG0103" H 1200 6025 50  0001 C CNN
F 1 "PWR_FLAG" V 1200 6077 50  0000 L CNN
F 2 "" H 1200 5950 50  0001 C CNN
F 3 "~" H 1200 5950 50  0001 C CNN
	1    1200 5950
	0    -1   -1   0   
$EndComp
Text GLabel 10700 4800 2    60   Input ~ 0
DGND
$Comp
L power:PWR_FLAG #FLG0104
U 1 1 5F8DC6DE
P 10700 4800
F 0 "#FLG0104" H 10700 4875 50  0001 C CNN
F 1 "PWR_FLAG" V 10700 4927 50  0000 L CNN
F 2 "" H 10700 4800 50  0001 C CNN
F 3 "~" H 10700 4800 50  0001 C CNN
	1    10700 4800
	0    -1   -1   0   
$EndComp
$Comp
L arduino:arduino-zero M101
U 1 1 5F8F0216
P 9100 2100
F 0 "M101" H 9100 3437 60  0000 C CNN
F 1 "Arduino" H 9100 3331 60  0000 C CNN
F 2 "arduino:arduino-zero-upsidedown" H 9100 2100 60  0001 C CNN
F 3 "" H 9100 2100 60  0000 C CNN
	1    9100 2100
	1    0    0    -1  
$EndComp
Text GLabel 10700 6050 2    60   Input ~ 0
AGND
$EndSCHEMATC
