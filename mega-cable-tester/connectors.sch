EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 62 62
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L mdm:MDM-37 JB0
U 1 1 5A04C9A2
P 8800 3050
F 0 "JB0" H 8800 4265 50  0000 C CNN
F 1 "gmr7580-37s" H 8800 4174 50  0000 C CNN
F 2 "mdm-cabletester:gmr7580-37s" H 800 1300 50  0001 C CNN
F 3 "" H 800 1300 50  0000 C CNN
	1    8800 3050
	1    0    0    -1  
$EndComp
NoConn ~ 8550 2050
NoConn ~ 9050 2050
$Comp
L mdm:MDM-37 JB1
U 1 1 5A04D55D
P 8800 5350
F 0 "JB1" H 8800 6565 50  0000 C CNN
F 1 "gmr7580-37s" H 8800 6474 50  0000 C CNN
F 2 "mdm-cabletester:gmr7580-37s" H 800 3600 50  0001 C CNN
F 3 "" H 800 3600 50  0000 C CNN
	1    8800 5350
	1    0    0    -1  
$EndComp
$Comp
L mdm:MDM-37 JB2
U 1 1 5A04DB01
P 8800 7650
F 0 "JB2" H 8800 8865 50  0000 C CNN
F 1 "gmr7580-37s" H 8800 8774 50  0000 C CNN
F 2 "mdm-cabletester:gmr7580-37s" H 800 5900 50  0001 C CNN
F 3 "" H 800 5900 50  0000 C CNN
	1    8800 7650
	1    0    0    -1  
$EndComp
$Comp
L mdm:MDM-37 JB3
U 1 1 5A04E31B
P 10400 3050
F 0 "JB3" H 10400 4265 50  0000 C CNN
F 1 "gmr7580-37s" H 10400 4174 50  0000 C CNN
F 2 "mdm-cabletester:gmr7580-37s" H 2400 1300 50  0001 C CNN
F 3 "" H 2400 1300 50  0000 C CNN
	1    10400 3050
	1    0    0    -1  
$EndComp
$Comp
L mdm:MDM-37 JB4
U 1 1 5A04E396
P 10400 5350
F 0 "JB4" H 10400 6565 50  0000 C CNN
F 1 "gmr7580-37s" H 10400 6474 50  0000 C CNN
F 2 "mdm-cabletester:gmr7580-37s" H 2400 3600 50  0001 C CNN
F 3 "" H 2400 3600 50  0000 C CNN
	1    10400 5350
	1    0    0    -1  
$EndComp
$Comp
L mdm:MDM-37 JB5
U 1 1 5A04E40E
P 10400 7650
F 0 "JB5" H 10400 8865 50  0000 C CNN
F 1 "gmr7580-37s" H 10400 8774 50  0000 C CNN
F 2 "mdm-cabletester:gmr7580-37s" H 2400 5900 50  0001 C CNN
F 3 "" H 2400 5900 50  0000 C CNN
	1    10400 7650
	1    0    0    -1  
$EndComp
NoConn ~ 10150 2050
NoConn ~ 10650 2050
NoConn ~ 10650 4350
NoConn ~ 10150 4350
NoConn ~ 9050 4350
NoConn ~ 8550 4350
NoConn ~ 8550 6650
NoConn ~ 9050 6650
NoConn ~ 10150 6650
NoConn ~ 10650 6650
Text GLabel 2600 1150 0    60   Input ~ 0
P128
Text GLabel 3100 1150 2    60   Input ~ 0
P129
Text GLabel 2600 1250 0    60   Input ~ 0
P130
Text GLabel 2600 1350 0    60   Input ~ 0
P132
Text GLabel 2600 1450 0    60   Input ~ 0
P134
Text GLabel 2600 1550 0    60   Input ~ 0
P136
Text GLabel 2600 1650 0    60   Input ~ 0
P138
Text GLabel 3100 1250 2    60   Input ~ 0
P131
Text GLabel 3100 1350 2    60   Input ~ 0
P133
Text GLabel 3100 1450 2    60   Input ~ 0
P135
Text GLabel 3100 1550 2    60   Input ~ 0
P137
Text GLabel 3100 1650 2    60   Input ~ 0
P139
Text GLabel 2600 1750 0    60   Input ~ 0
P140
Text GLabel 2600 1850 0    60   Input ~ 0
P142
Text GLabel 2600 1950 0    60   Input ~ 0
P144
Text GLabel 2600 2050 0    60   Input ~ 0
P146
Text GLabel 2600 2150 0    60   Input ~ 0
P148
Text GLabel 3100 1750 2    60   Input ~ 0
P141
Text GLabel 3100 1850 2    60   Input ~ 0
P143
Text GLabel 3100 1950 2    60   Input ~ 0
P145
Text GLabel 3100 2050 2    60   Input ~ 0
P147
Text GLabel 3100 2150 2    60   Input ~ 0
P149
Text GLabel 2600 2250 0    60   Input ~ 0
P150
Text GLabel 2600 2350 0    60   Input ~ 0
P152
Text GLabel 2600 2450 0    60   Input ~ 0
P154
Text GLabel 2600 2550 0    60   Input ~ 0
P156
Text GLabel 2600 2650 0    60   Input ~ 0
P158
Text GLabel 3100 2250 2    60   Input ~ 0
P151
Text GLabel 3100 2350 2    60   Input ~ 0
P153
Text GLabel 3100 2450 2    60   Input ~ 0
P155
Text GLabel 3100 2550 2    60   Input ~ 0
P157
Text GLabel 3100 2650 2    60   Input ~ 0
P159
Text GLabel 2600 2750 0    60   Input ~ 0
P160
Text GLabel 2600 2850 0    60   Input ~ 0
P162
Text GLabel 2600 2950 0    60   Input ~ 0
P164
Text GLabel 2600 3050 0    60   Input ~ 0
P166
Text GLabel 2600 3150 0    60   Input ~ 0
P168
Text GLabel 3100 2750 2    60   Input ~ 0
P161
Text GLabel 3100 2850 2    60   Input ~ 0
P163
Text GLabel 3100 2950 2    60   Input ~ 0
P165
Text GLabel 3100 3050 2    60   Input ~ 0
P167
Text GLabel 3100 3150 2    60   Input ~ 0
P169
Text GLabel 2600 3250 0    60   Input ~ 0
P170
Text GLabel 2600 3350 0    60   Input ~ 0
P172
Text GLabel 2600 3450 0    60   Input ~ 0
P174
Text GLabel 2600 3550 0    60   Input ~ 0
P176
Text GLabel 2600 3650 0    60   Input ~ 0
P178
Text GLabel 3100 3250 2    60   Input ~ 0
P171
Text GLabel 3100 3350 2    60   Input ~ 0
P173
Text GLabel 3100 3450 2    60   Input ~ 0
P175
Text GLabel 3100 3550 2    60   Input ~ 0
P177
Text GLabel 3100 3650 2    60   Input ~ 0
P179
Text GLabel 2600 3750 0    60   Input ~ 0
P180
Text GLabel 2600 3850 0    60   Input ~ 0
P182
Text GLabel 2600 3950 0    60   Input ~ 0
P184
Text GLabel 2600 4050 0    60   Input ~ 0
P186
Text GLabel 2600 4150 0    60   Input ~ 0
P188
Text GLabel 3100 3750 2    60   Input ~ 0
P181
Text GLabel 3100 3850 2    60   Input ~ 0
P183
Text GLabel 3100 3950 2    60   Input ~ 0
P185
Text GLabel 3100 4050 2    60   Input ~ 0
P187
Text GLabel 3100 4150 2    60   Input ~ 0
P189
Text GLabel 2600 4250 0    60   Input ~ 0
P190
Text GLabel 2600 4350 0    60   Input ~ 0
P192
Text GLabel 2600 4450 0    60   Input ~ 0
P194
Text GLabel 2600 4550 0    60   Input ~ 0
P196
Text GLabel 2600 4650 0    60   Input ~ 0
P198
Text GLabel 3100 4250 2    60   Input ~ 0
P191
Text GLabel 3100 4350 2    60   Input ~ 0
P193
Text GLabel 3100 4450 2    60   Input ~ 0
P195
Text GLabel 3100 4550 2    60   Input ~ 0
P197
Text GLabel 3100 4650 2    60   Input ~ 0
P199
Text GLabel 2600 4750 0    60   Input ~ 0
P200
Text GLabel 2600 4850 0    60   Input ~ 0
P202
Text GLabel 2600 4950 0    60   Input ~ 0
P204
Text GLabel 2600 5050 0    60   Input ~ 0
P206
Text GLabel 2600 5150 0    60   Input ~ 0
P208
Text GLabel 3100 4750 2    60   Input ~ 0
P201
Text GLabel 3100 4850 2    60   Input ~ 0
P203
Text GLabel 3100 4950 2    60   Input ~ 0
P205
Text GLabel 3100 5050 2    60   Input ~ 0
P207
Text GLabel 3100 5150 2    60   Input ~ 0
P209
Text GLabel 2600 5250 0    60   Input ~ 0
P210
Text GLabel 2600 5350 0    60   Input ~ 0
P212
Text GLabel 2600 5450 0    60   Input ~ 0
P214
Text GLabel 2600 5550 0    60   Input ~ 0
P216
Text GLabel 2600 5650 0    60   Input ~ 0
P218
Text GLabel 3100 5250 2    60   Input ~ 0
P211
Text GLabel 3100 5350 2    60   Input ~ 0
P213
Text GLabel 3100 5450 2    60   Input ~ 0
P215
Text GLabel 3100 5550 2    60   Input ~ 0
P217
Text GLabel 3100 5650 2    60   Input ~ 0
P219
Text GLabel 2600 5750 0    60   Input ~ 0
P220
Text GLabel 2600 5850 0    60   Input ~ 0
P222
Text GLabel 2600 5950 0    60   Input ~ 0
P224
Text GLabel 2600 6050 0    60   Input ~ 0
P226
Text GLabel 3100 5750 2    60   Input ~ 0
P221
Text GLabel 3100 5850 2    60   Input ~ 0
P223
Text GLabel 3100 5950 2    60   Input ~ 0
P225
Text GLabel 3100 6050 2    60   Input ~ 0
P227
Text GLabel 3800 1150 0    60   Input ~ 0
P256
Text GLabel 3800 1250 0    60   Input ~ 0
P258
Text GLabel 4300 1150 2    60   Input ~ 0
P257
Text GLabel 4300 1250 2    60   Input ~ 0
P259
Text GLabel 3800 1350 0    60   Input ~ 0
P260
Text GLabel 3800 1450 0    60   Input ~ 0
P262
Text GLabel 3800 1550 0    60   Input ~ 0
P264
Text GLabel 3800 1650 0    60   Input ~ 0
P266
Text GLabel 3800 1750 0    60   Input ~ 0
P268
Text GLabel 4300 1350 2    60   Input ~ 0
P261
Text GLabel 4300 1450 2    60   Input ~ 0
P263
Text GLabel 4300 1550 2    60   Input ~ 0
P265
Text GLabel 4300 1650 2    60   Input ~ 0
P267
Text GLabel 4300 1750 2    60   Input ~ 0
P269
Text GLabel 3800 1850 0    60   Input ~ 0
P270
Text GLabel 3800 1950 0    60   Input ~ 0
P272
Text GLabel 3800 2050 0    60   Input ~ 0
P274
Text GLabel 3800 2150 0    60   Input ~ 0
P276
Text GLabel 3800 2250 0    60   Input ~ 0
P278
Text GLabel 4300 1850 2    60   Input ~ 0
P271
Text GLabel 4300 1950 2    60   Input ~ 0
P273
Text GLabel 4300 2050 2    60   Input ~ 0
P275
Text GLabel 4300 2150 2    60   Input ~ 0
P277
Text GLabel 4300 2250 2    60   Input ~ 0
P279
Text GLabel 3800 2350 0    60   Input ~ 0
P280
Text GLabel 3800 2450 0    60   Input ~ 0
P282
Text GLabel 3800 2550 0    60   Input ~ 0
P284
Text GLabel 3800 2650 0    60   Input ~ 0
P286
Text GLabel 3800 2750 0    60   Input ~ 0
P288
Text GLabel 4300 2350 2    60   Input ~ 0
P281
Text GLabel 4300 2450 2    60   Input ~ 0
P283
Text GLabel 4300 2550 2    60   Input ~ 0
P285
Text GLabel 4300 2650 2    60   Input ~ 0
P287
Text GLabel 4300 2750 2    60   Input ~ 0
P289
Text GLabel 3800 2850 0    60   Input ~ 0
P290
Text GLabel 3800 2950 0    60   Input ~ 0
P292
Text GLabel 3800 3050 0    60   Input ~ 0
P294
Text GLabel 3800 3150 0    60   Input ~ 0
P296
Text GLabel 3800 3250 0    60   Input ~ 0
P298
Text GLabel 4300 2850 2    60   Input ~ 0
P291
Text GLabel 4300 2950 2    60   Input ~ 0
P293
Text GLabel 4300 3050 2    60   Input ~ 0
P295
Text GLabel 4300 3150 2    60   Input ~ 0
P297
Text GLabel 4300 3250 2    60   Input ~ 0
P299
Text GLabel 5000 1950 0    60   Input ~ 0
P400
Text GLabel 5000 2050 0    60   Input ~ 0
P402
Text GLabel 5000 2150 0    60   Input ~ 0
P404
Text GLabel 5000 2250 0    60   Input ~ 0
P406
Text GLabel 5000 2350 0    60   Input ~ 0
P408
Text GLabel 5500 1950 2    60   Input ~ 0
P401
Text GLabel 5500 2050 2    60   Input ~ 0
P403
Text GLabel 5500 2150 2    60   Input ~ 0
P405
Text GLabel 5500 2250 2    60   Input ~ 0
P407
Text GLabel 5500 2350 2    60   Input ~ 0
P409
Text GLabel 5000 2450 0    60   Input ~ 0
P410
Text GLabel 5000 2550 0    60   Input ~ 0
P412
Text GLabel 5000 2650 0    60   Input ~ 0
P414
Text GLabel 5000 2750 0    60   Input ~ 0
P416
Text GLabel 5000 2850 0    60   Input ~ 0
P418
Text GLabel 5500 2450 2    60   Input ~ 0
P411
Text GLabel 5500 2550 2    60   Input ~ 0
P413
Text GLabel 5500 2650 2    60   Input ~ 0
P415
Text GLabel 5500 2750 2    60   Input ~ 0
P417
Text GLabel 5500 2850 2    60   Input ~ 0
P419
Text GLabel 5000 2950 0    60   Input ~ 0
P420
Text GLabel 5000 3050 0    60   Input ~ 0
P422
Text GLabel 5000 3150 0    60   Input ~ 0
P424
Text GLabel 5000 3250 0    60   Input ~ 0
P426
Text GLabel 5000 3350 0    60   Input ~ 0
P428
Text GLabel 5500 2950 2    60   Input ~ 0
P421
Text GLabel 5500 3050 2    60   Input ~ 0
P423
Text GLabel 5500 3150 2    60   Input ~ 0
P425
Text GLabel 5500 3250 2    60   Input ~ 0
P427
Text GLabel 5500 3350 2    60   Input ~ 0
P429
Text GLabel 5000 3450 0    60   Input ~ 0
P430
Text GLabel 5000 3550 0    60   Input ~ 0
P432
Text GLabel 5000 3650 0    60   Input ~ 0
P434
Text GLabel 5000 3750 0    60   Input ~ 0
P436
Text GLabel 5000 3850 0    60   Input ~ 0
P438
Text GLabel 5500 3450 2    60   Input ~ 0
P431
Text GLabel 5500 3550 2    60   Input ~ 0
P433
Text GLabel 5500 3650 2    60   Input ~ 0
P435
Text GLabel 5500 3750 2    60   Input ~ 0
P437
Text GLabel 5500 3850 2    60   Input ~ 0
P439
Text GLabel 5000 3950 0    60   Input ~ 0
P440
Text GLabel 5000 4050 0    60   Input ~ 0
P442
Text GLabel 5000 4150 0    60   Input ~ 0
P444
Text GLabel 5000 4250 0    60   Input ~ 0
P446
Text GLabel 5000 4350 0    60   Input ~ 0
P448
Text GLabel 5500 3950 2    60   Input ~ 0
P441
Text GLabel 5500 4050 2    60   Input ~ 0
P443
Text GLabel 5500 4150 2    60   Input ~ 0
P445
Text GLabel 5500 4250 2    60   Input ~ 0
P447
Text GLabel 5500 4350 2    60   Input ~ 0
P449
Text GLabel 5000 4450 0    60   Input ~ 0
P450
Text GLabel 5000 4550 0    60   Input ~ 0
P452
Text GLabel 5000 4650 0    60   Input ~ 0
P454
Text GLabel 5000 4750 0    60   Input ~ 0
P456
Text GLabel 5000 4850 0    60   Input ~ 0
P458
Text GLabel 5500 4450 2    60   Input ~ 0
P451
Text GLabel 5500 4550 2    60   Input ~ 0
P453
Text GLabel 5500 4650 2    60   Input ~ 0
P455
Text GLabel 5500 4750 2    60   Input ~ 0
P457
Text GLabel 5500 4850 2    60   Input ~ 0
P459
Text GLabel 5000 4950 0    60   Input ~ 0
P460
Text GLabel 5000 5050 0    60   Input ~ 0
P462
Text GLabel 5000 5150 0    60   Input ~ 0
P464
Text GLabel 5000 5250 0    60   Input ~ 0
P466
Text GLabel 5000 5350 0    60   Input ~ 0
P468
Text GLabel 5500 4950 2    60   Input ~ 0
P461
Text GLabel 5500 5050 2    60   Input ~ 0
P463
Text GLabel 5500 5150 2    60   Input ~ 0
P465
Text GLabel 5500 5250 2    60   Input ~ 0
P467
Text GLabel 5500 5350 2    60   Input ~ 0
P469
Text GLabel 5000 5450 0    60   Input ~ 0
P470
Text GLabel 5000 5550 0    60   Input ~ 0
P472
Text GLabel 5000 5650 0    60   Input ~ 0
P474
Text GLabel 5000 5750 0    60   Input ~ 0
P476
Text GLabel 5000 5850 0    60   Input ~ 0
P478
Text GLabel 5500 5450 2    60   Input ~ 0
P471
Text GLabel 5500 5550 2    60   Input ~ 0
P473
Text GLabel 5500 5650 2    60   Input ~ 0
P475
Text GLabel 5500 5750 2    60   Input ~ 0
P477
Text GLabel 5500 5850 2    60   Input ~ 0
P479
Text GLabel 5000 5950 0    60   Input ~ 0
P480
Text GLabel 5000 6050 0    60   Input ~ 0
P482
Text GLabel 5500 5950 2    60   Input ~ 0
P481
Text GLabel 5500 6050 2    60   Input ~ 0
P483
Text GLabel 3800 3350 0    60   Input ~ 0
P300
Text GLabel 3800 3450 0    60   Input ~ 0
P302
Text GLabel 3800 3550 0    60   Input ~ 0
P304
Text GLabel 3800 3650 0    60   Input ~ 0
P306
Text GLabel 3800 3750 0    60   Input ~ 0
P308
Text GLabel 4300 3350 2    60   Input ~ 0
P301
Text GLabel 4300 3450 2    60   Input ~ 0
P303
Text GLabel 4300 3550 2    60   Input ~ 0
P305
Text GLabel 4300 3650 2    60   Input ~ 0
P307
Text GLabel 4300 3750 2    60   Input ~ 0
P309
Text GLabel 3800 3850 0    60   Input ~ 0
P310
Text GLabel 3800 3950 0    60   Input ~ 0
P312
Text GLabel 3800 4050 0    60   Input ~ 0
P314
Text GLabel 3800 4150 0    60   Input ~ 0
P316
Text GLabel 3800 4250 0    60   Input ~ 0
P318
Text GLabel 4300 3850 2    60   Input ~ 0
P311
Text GLabel 4300 3950 2    60   Input ~ 0
P313
Text GLabel 4300 4050 2    60   Input ~ 0
P315
Text GLabel 4300 4150 2    60   Input ~ 0
P317
Text GLabel 4300 4250 2    60   Input ~ 0
P319
Text GLabel 3800 4350 0    60   Input ~ 0
P320
Text GLabel 3800 4450 0    60   Input ~ 0
P322
Text GLabel 3800 4550 0    60   Input ~ 0
P324
Text GLabel 3800 4650 0    60   Input ~ 0
P326
Text GLabel 3800 4750 0    60   Input ~ 0
P328
Text GLabel 4300 4350 2    60   Input ~ 0
P321
Text GLabel 4300 4450 2    60   Input ~ 0
P323
Text GLabel 4300 4550 2    60   Input ~ 0
P325
Text GLabel 4300 4650 2    60   Input ~ 0
P327
Text GLabel 4300 4750 2    60   Input ~ 0
P329
Text GLabel 3800 4850 0    60   Input ~ 0
P330
Text GLabel 3800 4950 0    60   Input ~ 0
P332
Text GLabel 3800 5050 0    60   Input ~ 0
P334
Text GLabel 3800 5150 0    60   Input ~ 0
P336
Text GLabel 3800 5250 0    60   Input ~ 0
P338
Text GLabel 4300 4850 2    60   Input ~ 0
P331
Text GLabel 4300 4950 2    60   Input ~ 0
P333
Text GLabel 4300 5050 2    60   Input ~ 0
P335
Text GLabel 4300 5150 2    60   Input ~ 0
P337
Text GLabel 4300 5250 2    60   Input ~ 0
P339
Text GLabel 3800 5350 0    60   Input ~ 0
P340
Text GLabel 3800 5450 0    60   Input ~ 0
P342
Text GLabel 3800 5550 0    60   Input ~ 0
P344
Text GLabel 3800 5650 0    60   Input ~ 0
P346
Text GLabel 3800 5750 0    60   Input ~ 0
P348
Text GLabel 4300 5350 2    60   Input ~ 0
P341
Text GLabel 4300 5450 2    60   Input ~ 0
P343
Text GLabel 4300 5550 2    60   Input ~ 0
P345
Text GLabel 4300 5650 2    60   Input ~ 0
P347
Text GLabel 4300 5750 2    60   Input ~ 0
P349
Text GLabel 3800 5850 0    60   Input ~ 0
P350
Text GLabel 3800 5950 0    60   Input ~ 0
P352
Text GLabel 3800 6050 0    60   Input ~ 0
P354
Text GLabel 4300 5850 2    60   Input ~ 0
P351
Text GLabel 4300 5950 2    60   Input ~ 0
P353
Text GLabel 4300 6050 2    60   Input ~ 0
P355
Text GLabel 5000 1150 0    60   Input ~ 0
P384
Text GLabel 5000 1250 0    60   Input ~ 0
P386
Text GLabel 5000 1350 0    60   Input ~ 0
P388
Text GLabel 5500 1150 2    60   Input ~ 0
P385
Text GLabel 5500 1250 2    60   Input ~ 0
P387
Text GLabel 5500 1350 2    60   Input ~ 0
P389
Text GLabel 5000 1450 0    60   Input ~ 0
P390
Text GLabel 5000 1550 0    60   Input ~ 0
P392
Text GLabel 5000 1650 0    60   Input ~ 0
P394
Text GLabel 5000 1750 0    60   Input ~ 0
P396
Text GLabel 5000 1850 0    60   Input ~ 0
P398
Text GLabel 5500 1450 2    60   Input ~ 0
P391
Text GLabel 5500 1550 2    60   Input ~ 0
P393
Text GLabel 5500 1650 2    60   Input ~ 0
P395
Text GLabel 5500 1750 2    60   Input ~ 0
P397
Text GLabel 5500 1850 2    60   Input ~ 0
P399
$Comp
L mdm:MDM-100 JA1
U 1 1 5A02A1C9
P 2850 3600
F 0 "JA1" H 2850 6365 50  0000 C CNN
F 1 "M83513-24-H-100p" H 2850 6274 50  0000 C CNN
F 2 "mdm-cabletester:M83513-24-H-100p" H 2850 6273 50  0001 C CNN
F 3 "" H 1150 750 50  0000 C CNN
	1    2850 3600
	1    0    0    -1  
$EndComp
$Comp
L mdm:MDM-100 JA2
U 1 1 5A02A1D0
P 4050 3600
F 0 "JA2" H 4050 6365 50  0000 C CNN
F 1 "M83513-24-H-100p" H 4050 6274 50  0000 C CNN
F 2 "mdm-cabletester:M83513-24-H-100p" H 2350 750 50  0001 C CNN
F 3 "" H 2350 750 50  0000 C CNN
	1    4050 3600
	1    0    0    -1  
$EndComp
$Comp
L mdm:MDM-100 JA3
U 1 1 5A02A1D7
P 5250 3600
F 0 "JA3" H 5250 6365 50  0000 C CNN
F 1 "M83513-24-H-100p" H 5250 6274 50  0000 C CNN
F 2 "mdm-cabletester:M83513-24-H-100p" H 3550 750 50  0001 C CNN
F 3 "" H 3550 750 50  0000 C CNN
	1    5250 3600
	1    0    0    -1  
$EndComp
$Comp
L mdm:MDM-100 JA4
U 1 1 5A02A1DE
P 6450 3600
F 0 "JA4" H 6450 6365 50  0000 C CNN
F 1 "M83513-24-H-100p" H 6450 6274 50  0000 C CNN
F 2 "mdm-cabletester:M83513-24-H-100p" H 4750 750 50  0001 C CNN
F 3 "" H 4750 750 50  0000 C CNN
	1    6450 3600
	1    0    0    -1  
$EndComp
NoConn ~ 2600 1050
NoConn ~ 3100 1050
NoConn ~ 3800 1050
NoConn ~ 4300 1050
NoConn ~ 5000 1050
NoConn ~ 5500 1050
NoConn ~ 6200 1050
NoConn ~ 6700 1050
Text GLabel 6200 5550 0    60   Input ~ 0
P600
Text GLabel 6200 5650 0    60   Input ~ 0
P602
Text GLabel 6200 5750 0    60   Input ~ 0
P604
Text GLabel 6200 5850 0    60   Input ~ 0
P606
Text GLabel 6200 5950 0    60   Input ~ 0
P608
Text GLabel 6700 5550 2    60   Input ~ 0
P601
Text GLabel 6700 5650 2    60   Input ~ 0
P603
Text GLabel 6700 5750 2    60   Input ~ 0
P605
Text GLabel 6700 5850 2    60   Input ~ 0
P607
Text GLabel 6700 5950 2    60   Input ~ 0
P609
Text GLabel 6200 6050 0    60   Input ~ 0
P610
Text GLabel 6700 6050 2    60   Input ~ 0
P611
Text GLabel 8550 2150 0    60   Input ~ 0
P640
Text GLabel 8550 2250 0    60   Input ~ 0
P642
Text GLabel 8550 2350 0    60   Input ~ 0
P644
Text GLabel 8550 2450 0    60   Input ~ 0
P646
Text GLabel 8550 2550 0    60   Input ~ 0
P648
Text GLabel 9050 2150 2    60   Input ~ 0
P641
Text GLabel 9050 2250 2    60   Input ~ 0
P643
Text GLabel 9050 2350 2    60   Input ~ 0
P645
Text GLabel 9050 2450 2    60   Input ~ 0
P647
Text GLabel 9050 2550 2    60   Input ~ 0
P649
Text GLabel 8550 2650 0    60   Input ~ 0
P650
Text GLabel 8550 2750 0    60   Input ~ 0
P652
Text GLabel 8550 2850 0    60   Input ~ 0
P654
Text GLabel 8550 2950 0    60   Input ~ 0
P656
Text GLabel 8550 3050 0    60   Input ~ 0
P658
Text GLabel 9050 2650 2    60   Input ~ 0
P651
Text GLabel 9050 2750 2    60   Input ~ 0
P653
Text GLabel 9050 2850 2    60   Input ~ 0
P655
Text GLabel 9050 2950 2    60   Input ~ 0
P657
Text GLabel 9050 3050 2    60   Input ~ 0
P659
Text GLabel 8550 3150 0    60   Input ~ 0
P660
Text GLabel 8550 3250 0    60   Input ~ 0
P662
Text GLabel 8550 3350 0    60   Input ~ 0
P664
Text GLabel 8550 3450 0    60   Input ~ 0
P666
Text GLabel 8550 3550 0    60   Input ~ 0
P668
Text GLabel 9050 3150 2    60   Input ~ 0
P661
Text GLabel 9050 3250 2    60   Input ~ 0
P663
Text GLabel 9050 3350 2    60   Input ~ 0
P665
Text GLabel 9050 3450 2    60   Input ~ 0
P667
Text GLabel 9050 3550 2    60   Input ~ 0
P669
Text GLabel 8550 3650 0    60   Input ~ 0
P670
Text GLabel 8550 3750 0    60   Input ~ 0
P672
Text GLabel 8550 3850 0    60   Input ~ 0
P674
Text GLabel 8550 3950 0    60   Input ~ 0
P676
Text GLabel 9050 4450 2    60   Input ~ 0
P678
Text GLabel 9050 3650 2    60   Input ~ 0
P671
Text GLabel 9050 3750 2    60   Input ~ 0
P673
Text GLabel 9050 3850 2    60   Input ~ 0
P675
Text GLabel 8550 4450 0    60   Input ~ 0
P677
Text GLabel 8550 4550 0    60   Input ~ 0
P679
Text GLabel 9050 4550 2    60   Input ~ 0
P680
Text GLabel 9050 4650 2    60   Input ~ 0
P682
Text GLabel 9050 4750 2    60   Input ~ 0
P684
Text GLabel 9050 4850 2    60   Input ~ 0
P686
Text GLabel 9050 4950 2    60   Input ~ 0
P688
Text GLabel 8550 4650 0    60   Input ~ 0
P681
Text GLabel 8550 4750 0    60   Input ~ 0
P683
Text GLabel 8550 4850 0    60   Input ~ 0
P685
Text GLabel 8550 4950 0    60   Input ~ 0
P687
Text GLabel 8550 5050 0    60   Input ~ 0
P689
Text GLabel 9050 5050 2    60   Input ~ 0
P690
Text GLabel 9050 5150 2    60   Input ~ 0
P692
Text GLabel 9050 5250 2    60   Input ~ 0
P694
Text GLabel 9050 5350 2    60   Input ~ 0
P696
Text GLabel 9050 5450 2    60   Input ~ 0
P698
Text GLabel 8550 5150 0    60   Input ~ 0
P691
Text GLabel 8550 5250 0    60   Input ~ 0
P693
Text GLabel 8550 5350 0    60   Input ~ 0
P695
Text GLabel 8550 5450 0    60   Input ~ 0
P697
Text GLabel 8550 5550 0    60   Input ~ 0
P699
Text GLabel 9050 5550 2    60   Input ~ 0
P700
Text GLabel 9050 5650 2    60   Input ~ 0
P702
Text GLabel 9050 5750 2    60   Input ~ 0
P704
Text GLabel 9050 5850 2    60   Input ~ 0
P706
Text GLabel 9050 5950 2    60   Input ~ 0
P708
Text GLabel 8550 5650 0    60   Input ~ 0
P701
Text GLabel 8550 5750 0    60   Input ~ 0
P703
Text GLabel 8550 5850 0    60   Input ~ 0
P705
Text GLabel 8550 5950 0    60   Input ~ 0
P707
Text GLabel 8550 6050 0    60   Input ~ 0
P709
Text GLabel 9050 6050 2    60   Input ~ 0
P710
Text GLabel 9050 6150 2    60   Input ~ 0
P712
Text GLabel 8550 6750 0    60   Input ~ 0
P714
Text GLabel 8550 6850 0    60   Input ~ 0
P716
Text GLabel 8550 6950 0    60   Input ~ 0
P718
Text GLabel 8550 6150 0    60   Input ~ 0
P711
Text GLabel 8550 6250 0    60   Input ~ 0
P713
Text GLabel 9050 6750 2    60   Input ~ 0
P715
Text GLabel 9050 6850 2    60   Input ~ 0
P717
Text GLabel 9050 6950 2    60   Input ~ 0
P719
Text GLabel 8550 7050 0    60   Input ~ 0
P720
Text GLabel 8550 7150 0    60   Input ~ 0
P722
Text GLabel 8550 7250 0    60   Input ~ 0
P724
Text GLabel 8550 7350 0    60   Input ~ 0
P726
Text GLabel 8550 7450 0    60   Input ~ 0
P728
Text GLabel 9050 7050 2    60   Input ~ 0
P721
Text GLabel 9050 7150 2    60   Input ~ 0
P723
Text GLabel 9050 7250 2    60   Input ~ 0
P725
Text GLabel 9050 7350 2    60   Input ~ 0
P727
Text GLabel 9050 7450 2    60   Input ~ 0
P729
Text GLabel 8550 7550 0    60   Input ~ 0
P730
Text GLabel 8550 7650 0    60   Input ~ 0
P732
Text GLabel 8550 7750 0    60   Input ~ 0
P734
Text GLabel 8550 7850 0    60   Input ~ 0
P736
Text GLabel 8550 7950 0    60   Input ~ 0
P738
Text GLabel 9050 7550 2    60   Input ~ 0
P731
Text GLabel 9050 7650 2    60   Input ~ 0
P733
Text GLabel 9050 7750 2    60   Input ~ 0
P735
Text GLabel 9050 7850 2    60   Input ~ 0
P737
Text GLabel 9050 7950 2    60   Input ~ 0
P739
Text GLabel 8550 8050 0    60   Input ~ 0
P740
Text GLabel 8550 8150 0    60   Input ~ 0
P742
Text GLabel 8550 8250 0    60   Input ~ 0
P744
Text GLabel 8550 8350 0    60   Input ~ 0
P746
Text GLabel 8550 8450 0    60   Input ~ 0
P748
Text GLabel 9050 8050 2    60   Input ~ 0
P741
Text GLabel 9050 8150 2    60   Input ~ 0
P743
Text GLabel 9050 8250 2    60   Input ~ 0
P745
Text GLabel 9050 8350 2    60   Input ~ 0
P747
Text GLabel 9050 8450 2    60   Input ~ 0
P749
Text GLabel 8550 8550 0    60   Input ~ 0
P750
Text GLabel 10150 2150 0    60   Input ~ 0
P768
Text GLabel 10650 2150 2    60   Input ~ 0
P769
Text GLabel 10150 2250 0    60   Input ~ 0
P770
Text GLabel 10150 2350 0    60   Input ~ 0
P772
Text GLabel 10150 2450 0    60   Input ~ 0
P774
Text GLabel 10150 2550 0    60   Input ~ 0
P776
Text GLabel 10150 2650 0    60   Input ~ 0
P778
Text GLabel 10650 2250 2    60   Input ~ 0
P771
Text GLabel 10650 2350 2    60   Input ~ 0
P773
Text GLabel 10650 2450 2    60   Input ~ 0
P775
Text GLabel 10650 2550 2    60   Input ~ 0
P777
Text GLabel 10650 2650 2    60   Input ~ 0
P779
Text GLabel 10150 2750 0    60   Input ~ 0
P780
Text GLabel 10150 2850 0    60   Input ~ 0
P782
Text GLabel 10150 2950 0    60   Input ~ 0
P784
Text GLabel 10150 3050 0    60   Input ~ 0
P786
Text GLabel 10150 3150 0    60   Input ~ 0
P788
Text GLabel 10650 2750 2    60   Input ~ 0
P781
Text GLabel 10650 2850 2    60   Input ~ 0
P783
Text GLabel 10650 2950 2    60   Input ~ 0
P785
Text GLabel 10650 3050 2    60   Input ~ 0
P787
Text GLabel 10650 3150 2    60   Input ~ 0
P789
Text GLabel 10150 3250 0    60   Input ~ 0
P790
Text GLabel 10150 3350 0    60   Input ~ 0
P792
Text GLabel 10150 3450 0    60   Input ~ 0
P794
Text GLabel 10150 3550 0    60   Input ~ 0
P796
Text GLabel 10150 3650 0    60   Input ~ 0
P798
Text GLabel 10650 3250 2    60   Input ~ 0
P791
Text GLabel 10650 3350 2    60   Input ~ 0
P793
Text GLabel 10650 3450 2    60   Input ~ 0
P795
Text GLabel 10650 3550 2    60   Input ~ 0
P797
Text GLabel 10650 3650 2    60   Input ~ 0
P799
$Comp
L mdm:MDM-100 JA0
U 1 1 5A4F1234
P 1650 3600
F 0 "JA0" H 1650 6365 50  0000 C CNN
F 1 "M83513-24-H-100p" H 1650 6274 50  0000 C CNN
F 2 "mdm-cabletester:M83513-24-H-100p" H -50 750 50  0001 C CNN
F 3 "" H -50 750 50  0000 C CNN
	1    1650 3600
	1    0    0    -1  
$EndComp
Text GLabel 1400 1150 0    60   Input ~ 0
P000
Text GLabel 1400 1250 0    60   Input ~ 0
P002
Text GLabel 1400 1350 0    60   Input ~ 0
P004
Text GLabel 1400 1450 0    60   Input ~ 0
P006
Text GLabel 1400 1550 0    60   Input ~ 0
P008
Text GLabel 1900 1150 2    60   Input ~ 0
P001
Text GLabel 1900 1250 2    60   Input ~ 0
P003
Text GLabel 1900 1350 2    60   Input ~ 0
P005
Text GLabel 1900 1450 2    60   Input ~ 0
P007
Text GLabel 1900 1550 2    60   Input ~ 0
P009
Text GLabel 1400 1650 0    60   Input ~ 0
P010
Text GLabel 1400 1750 0    60   Input ~ 0
P012
Text GLabel 1400 1850 0    60   Input ~ 0
P014
Text GLabel 1400 1950 0    60   Input ~ 0
P016
Text GLabel 1400 2050 0    60   Input ~ 0
P018
Text GLabel 1900 1650 2    60   Input ~ 0
P011
Text GLabel 1900 1750 2    60   Input ~ 0
P013
Text GLabel 1900 1850 2    60   Input ~ 0
P015
Text GLabel 1900 1950 2    60   Input ~ 0
P017
Text GLabel 1900 2050 2    60   Input ~ 0
P019
Text GLabel 1400 2150 0    60   Input ~ 0
P020
Text GLabel 1400 2250 0    60   Input ~ 0
P022
Text GLabel 1400 2350 0    60   Input ~ 0
P024
Text GLabel 1400 2450 0    60   Input ~ 0
P026
Text GLabel 1400 2550 0    60   Input ~ 0
P028
Text GLabel 1900 2150 2    60   Input ~ 0
P021
Text GLabel 1900 2250 2    60   Input ~ 0
P023
Text GLabel 1900 2350 2    60   Input ~ 0
P025
Text GLabel 1900 2450 2    60   Input ~ 0
P027
Text GLabel 1900 2550 2    60   Input ~ 0
P029
Text GLabel 1400 2650 0    60   Input ~ 0
P030
Text GLabel 1400 2750 0    60   Input ~ 0
P032
Text GLabel 1400 2850 0    60   Input ~ 0
P034
Text GLabel 1400 2950 0    60   Input ~ 0
P036
Text GLabel 1400 3050 0    60   Input ~ 0
P038
Text GLabel 1900 2650 2    60   Input ~ 0
P031
Text GLabel 1900 2750 2    60   Input ~ 0
P033
Text GLabel 1900 2850 2    60   Input ~ 0
P035
Text GLabel 1900 2950 2    60   Input ~ 0
P037
Text GLabel 1900 3050 2    60   Input ~ 0
P039
Text GLabel 1400 3150 0    60   Input ~ 0
P040
Text GLabel 1400 3250 0    60   Input ~ 0
P042
Text GLabel 1400 3350 0    60   Input ~ 0
P044
Text GLabel 1400 3450 0    60   Input ~ 0
P046
Text GLabel 1400 3550 0    60   Input ~ 0
P048
Text GLabel 1900 3150 2    60   Input ~ 0
P041
Text GLabel 1900 3250 2    60   Input ~ 0
P043
Text GLabel 1900 3350 2    60   Input ~ 0
P045
Text GLabel 1900 3450 2    60   Input ~ 0
P047
Text GLabel 1900 3550 2    60   Input ~ 0
P049
Text GLabel 1400 3650 0    60   Input ~ 0
P050
Text GLabel 1400 3750 0    60   Input ~ 0
P052
Text GLabel 1400 3850 0    60   Input ~ 0
P054
Text GLabel 1400 3950 0    60   Input ~ 0
P056
Text GLabel 1400 4050 0    60   Input ~ 0
P058
Text GLabel 1900 3650 2    60   Input ~ 0
P051
Text GLabel 1900 3750 2    60   Input ~ 0
P053
Text GLabel 1900 3850 2    60   Input ~ 0
P055
Text GLabel 1900 3950 2    60   Input ~ 0
P057
Text GLabel 1900 4050 2    60   Input ~ 0
P059
Text GLabel 1400 4150 0    60   Input ~ 0
P060
Text GLabel 1400 4250 0    60   Input ~ 0
P062
Text GLabel 1400 4350 0    60   Input ~ 0
P064
Text GLabel 1400 4450 0    60   Input ~ 0
P066
Text GLabel 1400 4550 0    60   Input ~ 0
P068
Text GLabel 1900 4150 2    60   Input ~ 0
P061
Text GLabel 1900 4250 2    60   Input ~ 0
P063
Text GLabel 1900 4350 2    60   Input ~ 0
P065
Text GLabel 1900 4450 2    60   Input ~ 0
P067
Text GLabel 1900 4550 2    60   Input ~ 0
P069
Text GLabel 1400 4650 0    60   Input ~ 0
P070
Text GLabel 1400 4750 0    60   Input ~ 0
P072
Text GLabel 1400 4850 0    60   Input ~ 0
P074
Text GLabel 1400 4950 0    60   Input ~ 0
P076
Text GLabel 1400 5050 0    60   Input ~ 0
P078
Text GLabel 1900 4650 2    60   Input ~ 0
P071
Text GLabel 1900 4750 2    60   Input ~ 0
P073
Text GLabel 1900 4850 2    60   Input ~ 0
P075
Text GLabel 1900 4950 2    60   Input ~ 0
P077
Text GLabel 1900 5050 2    60   Input ~ 0
P079
Text GLabel 1400 5150 0    60   Input ~ 0
P080
Text GLabel 1400 5250 0    60   Input ~ 0
P082
Text GLabel 1400 5350 0    60   Input ~ 0
P084
Text GLabel 1400 5450 0    60   Input ~ 0
P086
Text GLabel 1400 5550 0    60   Input ~ 0
P088
Text GLabel 1900 5150 2    60   Input ~ 0
P081
Text GLabel 1900 5250 2    60   Input ~ 0
P083
Text GLabel 1900 5350 2    60   Input ~ 0
P085
Text GLabel 1900 5450 2    60   Input ~ 0
P087
Text GLabel 1900 5550 2    60   Input ~ 0
P089
Text GLabel 1400 5650 0    60   Input ~ 0
P090
Text GLabel 1400 5750 0    60   Input ~ 0
P092
Text GLabel 1400 5850 0    60   Input ~ 0
P094
Text GLabel 1400 5950 0    60   Input ~ 0
P096
Text GLabel 1400 6050 0    60   Input ~ 0
P098
Text GLabel 1900 5650 2    60   Input ~ 0
P091
Text GLabel 1900 5750 2    60   Input ~ 0
P093
Text GLabel 1900 5850 2    60   Input ~ 0
P095
Text GLabel 1900 5950 2    60   Input ~ 0
P097
Text GLabel 1900 6050 2    60   Input ~ 0
P099
NoConn ~ 1400 1050
NoConn ~ 1900 1050
Text GLabel 6700 5450 2    60   Input ~ 0
P599
Text GLabel 6700 5350 2    60   Input ~ 0
P597
Text GLabel 6700 5250 2    60   Input ~ 0
P595
Text GLabel 6700 5150 2    60   Input ~ 0
P593
Text GLabel 6700 5050 2    60   Input ~ 0
P591
Text GLabel 6200 5450 0    60   Input ~ 0
P598
Text GLabel 6200 5350 0    60   Input ~ 0
P596
Text GLabel 6200 5250 0    60   Input ~ 0
P594
Text GLabel 6200 5150 0    60   Input ~ 0
P592
Text GLabel 6200 5050 0    60   Input ~ 0
P590
Text GLabel 6700 4950 2    60   Input ~ 0
P589
Text GLabel 6700 4850 2    60   Input ~ 0
P587
Text GLabel 6700 4750 2    60   Input ~ 0
P585
Text GLabel 6700 4650 2    60   Input ~ 0
P583
Text GLabel 6700 4550 2    60   Input ~ 0
P581
Text GLabel 6200 4950 0    60   Input ~ 0
P588
Text GLabel 6200 4850 0    60   Input ~ 0
P586
Text GLabel 6200 4750 0    60   Input ~ 0
P584
Text GLabel 6200 4650 0    60   Input ~ 0
P582
Text GLabel 6200 4550 0    60   Input ~ 0
P580
Text GLabel 6700 4450 2    60   Input ~ 0
P579
Text GLabel 6700 4350 2    60   Input ~ 0
P577
Text GLabel 6700 4250 2    60   Input ~ 0
P575
Text GLabel 6700 4150 2    60   Input ~ 0
P573
Text GLabel 6700 4050 2    60   Input ~ 0
P571
Text GLabel 6200 4450 0    60   Input ~ 0
P578
Text GLabel 6200 4350 0    60   Input ~ 0
P576
Text GLabel 6200 4250 0    60   Input ~ 0
P574
Text GLabel 6200 4150 0    60   Input ~ 0
P572
Text GLabel 6200 4050 0    60   Input ~ 0
P570
Text GLabel 6700 3950 2    60   Input ~ 0
P569
Text GLabel 6700 3850 2    60   Input ~ 0
P567
Text GLabel 6700 3750 2    60   Input ~ 0
P565
Text GLabel 6700 3650 2    60   Input ~ 0
P563
Text GLabel 6700 3550 2    60   Input ~ 0
P561
Text GLabel 6200 3950 0    60   Input ~ 0
P568
Text GLabel 6200 3850 0    60   Input ~ 0
P566
Text GLabel 6200 3750 0    60   Input ~ 0
P564
Text GLabel 6200 3650 0    60   Input ~ 0
P562
Text GLabel 6200 3550 0    60   Input ~ 0
P560
Text GLabel 6700 3450 2    60   Input ~ 0
P559
Text GLabel 6700 3350 2    60   Input ~ 0
P557
Text GLabel 6700 3250 2    60   Input ~ 0
P555
Text GLabel 6700 3150 2    60   Input ~ 0
P553
Text GLabel 6700 3050 2    60   Input ~ 0
P551
Text GLabel 6200 3450 0    60   Input ~ 0
P558
Text GLabel 6200 3350 0    60   Input ~ 0
P556
Text GLabel 6200 3250 0    60   Input ~ 0
P554
Text GLabel 6200 3150 0    60   Input ~ 0
P552
Text GLabel 6200 3050 0    60   Input ~ 0
P550
Text GLabel 6700 2950 2    60   Input ~ 0
P549
Text GLabel 6700 2850 2    60   Input ~ 0
P547
Text GLabel 6700 2750 2    60   Input ~ 0
P545
Text GLabel 6700 2650 2    60   Input ~ 0
P543
Text GLabel 6700 2550 2    60   Input ~ 0
P541
Text GLabel 6200 2950 0    60   Input ~ 0
P548
Text GLabel 6200 2850 0    60   Input ~ 0
P546
Text GLabel 6200 2750 0    60   Input ~ 0
P544
Text GLabel 6200 2650 0    60   Input ~ 0
P542
Text GLabel 6200 2550 0    60   Input ~ 0
P540
Text GLabel 6700 2450 2    60   Input ~ 0
P539
Text GLabel 6700 2350 2    60   Input ~ 0
P537
Text GLabel 6700 2250 2    60   Input ~ 0
P535
Text GLabel 6700 2150 2    60   Input ~ 0
P533
Text GLabel 6700 2050 2    60   Input ~ 0
P531
Text GLabel 6200 2450 0    60   Input ~ 0
P538
Text GLabel 6200 2350 0    60   Input ~ 0
P536
Text GLabel 6200 2250 0    60   Input ~ 0
P534
Text GLabel 6200 2150 0    60   Input ~ 0
P532
Text GLabel 6200 2050 0    60   Input ~ 0
P530
Text GLabel 6700 1950 2    60   Input ~ 0
P529
Text GLabel 6700 1850 2    60   Input ~ 0
P527
Text GLabel 6700 1750 2    60   Input ~ 0
P525
Text GLabel 6700 1650 2    60   Input ~ 0
P523
Text GLabel 6700 1550 2    60   Input ~ 0
P521
Text GLabel 6200 1950 0    60   Input ~ 0
P528
Text GLabel 6200 1850 0    60   Input ~ 0
P526
Text GLabel 6200 1750 0    60   Input ~ 0
P524
Text GLabel 6200 1650 0    60   Input ~ 0
P522
Text GLabel 6200 1550 0    60   Input ~ 0
P520
Text GLabel 6700 1450 2    60   Input ~ 0
P519
Text GLabel 6700 1350 2    60   Input ~ 0
P517
Text GLabel 6700 1250 2    60   Input ~ 0
P515
Text GLabel 6700 1150 2    60   Input ~ 0
P513
Text GLabel 6200 1450 0    60   Input ~ 0
P518
Text GLabel 6200 1350 0    60   Input ~ 0
P516
Text GLabel 6200 1250 0    60   Input ~ 0
P514
Text GLabel 6200 1150 0    60   Input ~ 0
P512
Text GLabel 10150 3750 0    60   Input ~ 0
P800
Text GLabel 10150 3850 0    60   Input ~ 0
P802
Text GLabel 10150 3950 0    60   Input ~ 0
P804
Text GLabel 10650 4450 2    60   Input ~ 0
P806
Text GLabel 10650 4550 2    60   Input ~ 0
P808
Text GLabel 10650 3750 2    60   Input ~ 0
P801
Text GLabel 10650 3850 2    60   Input ~ 0
P803
Text GLabel 10150 4450 0    60   Input ~ 0
P805
Text GLabel 10150 4550 0    60   Input ~ 0
P807
Text GLabel 10150 4650 0    60   Input ~ 0
P809
Text GLabel 10650 4650 2    60   Input ~ 0
P810
Text GLabel 10650 4750 2    60   Input ~ 0
P812
Text GLabel 10650 4850 2    60   Input ~ 0
P814
Text GLabel 10650 4950 2    60   Input ~ 0
P816
Text GLabel 10650 5050 2    60   Input ~ 0
P818
Text GLabel 10150 4750 0    60   Input ~ 0
P811
Text GLabel 10150 4850 0    60   Input ~ 0
P813
Text GLabel 10150 4950 0    60   Input ~ 0
P815
Text GLabel 10150 5050 0    60   Input ~ 0
P817
Text GLabel 10150 5150 0    60   Input ~ 0
P819
Text GLabel 10650 5150 2    60   Input ~ 0
P820
Text GLabel 10650 5250 2    60   Input ~ 0
P822
Text GLabel 10650 5350 2    60   Input ~ 0
P824
Text GLabel 10650 5450 2    60   Input ~ 0
P826
Text GLabel 10650 5550 2    60   Input ~ 0
P828
Text GLabel 10150 5250 0    60   Input ~ 0
P821
Text GLabel 10150 5350 0    60   Input ~ 0
P823
Text GLabel 10150 5450 0    60   Input ~ 0
P825
Text GLabel 10150 5550 0    60   Input ~ 0
P827
Text GLabel 10150 5650 0    60   Input ~ 0
P829
Text GLabel 10650 5650 2    60   Input ~ 0
P830
Text GLabel 10650 5750 2    60   Input ~ 0
P832
Text GLabel 10650 5850 2    60   Input ~ 0
P834
Text GLabel 10650 5950 2    60   Input ~ 0
P836
Text GLabel 10650 6050 2    60   Input ~ 0
P838
Text GLabel 10150 5750 0    60   Input ~ 0
P831
Text GLabel 10150 5850 0    60   Input ~ 0
P833
Text GLabel 10150 5950 0    60   Input ~ 0
P835
Text GLabel 10150 6050 0    60   Input ~ 0
P837
Text GLabel 10150 6150 0    60   Input ~ 0
P839
Text GLabel 10650 6150 2    60   Input ~ 0
P840
Text GLabel 10150 6750 0    60   Input ~ 0
P842
Text GLabel 10150 6850 0    60   Input ~ 0
P844
Text GLabel 10150 6950 0    60   Input ~ 0
P846
Text GLabel 10150 7050 0    60   Input ~ 0
P848
Text GLabel 10150 6250 0    60   Input ~ 0
P841
Text GLabel 10650 6750 2    60   Input ~ 0
P843
Text GLabel 10650 6850 2    60   Input ~ 0
P845
Text GLabel 10650 6950 2    60   Input ~ 0
P847
Text GLabel 10650 7050 2    60   Input ~ 0
P849
Text GLabel 10150 7150 0    60   Input ~ 0
P850
Text GLabel 10150 7250 0    60   Input ~ 0
P852
Text GLabel 10150 7350 0    60   Input ~ 0
P854
Text GLabel 10150 7450 0    60   Input ~ 0
P856
Text GLabel 10150 7550 0    60   Input ~ 0
P858
Text GLabel 10650 7150 2    60   Input ~ 0
P851
Text GLabel 10650 7250 2    60   Input ~ 0
P853
Text GLabel 10650 7350 2    60   Input ~ 0
P855
Text GLabel 10650 7450 2    60   Input ~ 0
P857
Text GLabel 10650 7550 2    60   Input ~ 0
P859
Text GLabel 10150 7650 0    60   Input ~ 0
P860
Text GLabel 10150 7750 0    60   Input ~ 0
P862
Text GLabel 10150 7850 0    60   Input ~ 0
P864
Text GLabel 10150 7950 0    60   Input ~ 0
P866
Text GLabel 10150 8050 0    60   Input ~ 0
P868
Text GLabel 10650 7650 2    60   Input ~ 0
P861
Text GLabel 10650 7750 2    60   Input ~ 0
P863
Text GLabel 10650 7850 2    60   Input ~ 0
P865
Text GLabel 10650 7950 2    60   Input ~ 0
P867
Text GLabel 10650 8050 2    60   Input ~ 0
P869
Text GLabel 10150 8150 0    60   Input ~ 0
P870
Text GLabel 10150 8250 0    60   Input ~ 0
P872
Text GLabel 10150 8350 0    60   Input ~ 0
P874
Text GLabel 10150 8450 0    60   Input ~ 0
P876
Text GLabel 10150 8550 0    60   Input ~ 0
P878
Text GLabel 10650 8150 2    60   Input ~ 0
P871
Text GLabel 10650 8250 2    60   Input ~ 0
P873
Text GLabel 10650 8350 2    60   Input ~ 0
P875
Text GLabel 10650 8450 2    60   Input ~ 0
P877
$EndSCHEMATC
