EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A2 23386 16535
encoding utf-8
Sheet 32 62
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 18500 11700 0    60   Input ~ 0
MUX_A0
Text HLabel 18500 11900 0    60   Input ~ 0
MUX_A1
Text HLabel 18500 12100 0    60   Input ~ 0
MUX_A2
Text HLabel 20900 12200 2    60   Input ~ 0
MUX_A3
Text HLabel 20900 12000 2    60   Input ~ 0
MUX_A4
Text HLabel 20900 11700 2    60   Input ~ 0
MUX_EN
$Sheet
S 18450 5700 600  4150
U 5A168420
F0 "sheet5A1683F7" 60
F1 "mux.sch" 60
F2 "MUX_IN01" B L 18450 5800 60 
F3 "MUX_IN02" B L 18450 5900 60 
F4 "MUX_IN03" B L 18450 6000 60 
F5 "MUX_IN04" B L 18450 6100 60 
F6 "MUX_IN05" B L 18450 6200 60 
F7 "MUX_IN06" B L 18450 6300 60 
F8 "MUX_IN07" B L 18450 6400 60 
F9 "MUX_IN08" B L 18450 6500 60 
F10 "MUX_IN09" B L 18450 6600 60 
F11 "MUX_IN10" B L 18450 6700 60 
F12 "MUX_IN11" B L 18450 6800 60 
F13 "MUX_IN12" B L 18450 6900 60 
F14 "MUX_IN14" B L 18450 7100 60 
F15 "MUX_IN15" B L 18450 7200 60 
F16 "MUX_IN16" B L 18450 7300 60 
F17 "MUX_OUT" B L 18450 9050 60 
F18 "MUX_IN32" B L 18450 8900 60 
F19 "MUX_IN31" B L 18450 8800 60 
F20 "MUX_IN30" B L 18450 8700 60 
F21 "MUX_IN29" B L 18450 8600 60 
F22 "MUX_IN28" B L 18450 8500 60 
F23 "MUX_IN27" B L 18450 8400 60 
F24 "MUX_IN26" B L 18450 8300 60 
F25 "MUX_IN25" B L 18450 8200 60 
F26 "MUX_IN24" B L 18450 8100 60 
F27 "MUX_IN23" B L 18450 8000 60 
F28 "MUX_IN22" B L 18450 7900 60 
F29 "MUX_IN21" B L 18450 7800 60 
F30 "MUX_IN20" B L 18450 7700 60 
F31 "MUX_IN19" B L 18450 7600 60 
F32 "MUX_IN18" B L 18450 7500 60 
F33 "MUX_IN17" B L 18450 7400 60 
F34 "MUX_A0" I L 18450 9200 60 
F35 "MUX_A1" I L 18450 9300 60 
F36 "MUX_A2" I L 18450 9400 60 
F37 "MUX_A3" I L 18450 9500 60 
F38 "MUX_A4" I L 18450 9600 60 
F39 "MUX_EN" I L 18450 9750 60 
F40 "MUX_IN13" B L 18450 7000 60 
$EndSheet
Wire Wire Line
	18450 5800 17900 5800
Text Label 18000 5800 0    60   ~ 0
OUT00
Wire Wire Line
	18450 5900 17900 5900
Text Label 18000 5900 0    60   ~ 0
OUT01
Wire Wire Line
	18450 6000 17900 6000
Text Label 18000 6000 0    60   ~ 0
OUT02
Wire Wire Line
	18450 6100 17900 6100
Text Label 18000 6100 0    60   ~ 0
OUT03
Wire Wire Line
	18450 6200 17900 6200
Text Label 18000 6200 0    60   ~ 0
OUT04
Wire Wire Line
	18450 6300 17900 6300
Text Label 18000 6300 0    60   ~ 0
OUT05
Wire Wire Line
	18450 6400 17900 6400
Text Label 18000 6400 0    60   ~ 0
OUT06
Wire Wire Line
	18450 6500 17900 6500
Text Label 18000 6500 0    60   ~ 0
OUT07
Wire Wire Line
	18450 6600 17900 6600
Text Label 18000 6600 0    60   ~ 0
OUT08
Wire Wire Line
	18450 6700 17900 6700
Text Label 18000 6700 0    60   ~ 0
OUT09
Wire Wire Line
	18450 6800 17900 6800
Text Label 18000 6800 0    60   ~ 0
OUT10
Wire Wire Line
	18450 6900 17900 6900
Text Label 18000 6900 0    60   ~ 0
OUT11
Wire Wire Line
	18450 7000 17900 7000
Text Label 18000 7000 0    60   ~ 0
OUT12
Wire Wire Line
	18450 7100 17900 7100
Text Label 18000 7100 0    60   ~ 0
OUT13
Wire Wire Line
	18450 7200 17900 7200
Text Label 18000 7200 0    60   ~ 0
OUT14
Wire Wire Line
	18450 7300 17900 7300
Text Label 18000 7300 0    60   ~ 0
OUT15
NoConn ~ 18450 8700
NoConn ~ 18450 8800
NoConn ~ 18450 8900
Text HLabel 18450 9050 0    60   BiDi ~ 0
MUX_OUT
$Comp
L mega-cable-tester-rescue:C C89
U 1 1 5A22EF7C
P 19200 11350
AR Path="/5A220C88/5A22EF7C" Ref="C89"  Part="1" 
AR Path="/5A018C64/5A22EF7C" Ref="C27"  Part="1" 
AR Path="/5A22EF7C" Ref="C89"  Part="1" 
F 0 "C89" H 19225 11450 50  0000 L CNN
F 1 "0.1uF" H 19225 11250 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 19238 11200 50  0001 C CNN
F 3 "" H 19200 11350 50  0000 C CNN
	1    19200 11350
	-1   0    0    1   
$EndComp
$Comp
L logic-level-conversion:74HC4050 U8
U 1 1 5A22EF85
P 19700 11400
AR Path="/5A220C88/5A22EF85" Ref="U8"  Part="1" 
AR Path="/5A018C64/5A22EF85" Ref="U6"  Part="1" 
F 0 "U8" H 19700 11547 60  0000 C CNN
F 1 "CD74HC4050PWR" H 19700 11441 60  0000 C CNN
F 2 "Housings_SSOP:TSSOP-16_4.4x5mm_Pitch0.65mm" H 23800 -3900 60  0001 C CNN
F 3 "" H 23800 -3900 60  0001 C CNN
	1    19700 11400
	1    0    0    -1  
$EndComp
Connection ~ 19200 11500
Wire Wire Line
	20900 12100 20200 12100
Wire Wire Line
	20200 11600 20900 11600
Wire Wire Line
	20200 11900 20900 11900
Wire Wire Line
	19200 11600 18500 11600
Wire Wire Line
	18500 11800 19200 11800
Wire Wire Line
	19200 12000 18500 12000
Wire Wire Line
	18450 9200 18050 9200
Text Label 18050 9200 0    60   ~ 0
SAFE_A5
Text Label 18050 9300 0    60   ~ 0
SAFE_A6
Text Label 18050 9400 0    60   ~ 0
SAFE_A7
Text Label 18050 9500 0    60   ~ 0
SAFE_A8
Text Label 18050 9600 0    60   ~ 0
SAFE_A9
Text Label 18050 9750 0    60   ~ 0
SAFE_EN
Wire Wire Line
	18050 9300 18450 9300
Wire Wire Line
	18450 9400 18050 9400
Wire Wire Line
	18050 9500 18450 9500
Wire Wire Line
	18050 9600 18450 9600
Wire Wire Line
	18050 9750 18450 9750
Wire Wire Line
	20900 11700 20200 11700
Wire Wire Line
	20900 12000 20200 12000
Wire Wire Line
	20900 12200 20200 12200
Wire Wire Line
	19200 12100 18500 12100
Wire Wire Line
	19200 11900 18500 11900
Wire Wire Line
	19200 11700 18500 11700
Text Label 18550 11600 0    60   ~ 0
SAFE_A0
Text Label 18550 11800 0    60   ~ 0
SAFE_A1
Text Label 18550 12000 0    60   ~ 0
SAFE_A2
Text Label 20250 12100 0    60   ~ 0
SAFE_A3
Text Label 20250 11900 0    60   ~ 0
SAFE_A4
Text Label 20250 11600 0    60   ~ 0
SAFE_EN
Text GLabel 19200 11200 2    60   Input ~ 0
GND
Text GLabel 19200 12200 0    60   Input ~ 0
GND
Wire Wire Line
	18500 11500 19200 11500
Text GLabel 18500 11500 0    60   Input ~ 0
A2V5+
Text HLabel 18500 13000 0    60   Input ~ 0
MUX_A5
Text HLabel 18500 13200 0    60   Input ~ 0
MUX_A6
Text HLabel 18500 13400 0    60   Input ~ 0
MUX_A7
Text HLabel 20900 13500 2    60   Input ~ 0
MUX_A8
Text HLabel 20900 13300 2    60   Input ~ 0
MUX_A9
$Comp
L mega-cable-tester-rescue:C C90
U 1 1 5A3194F7
P 19200 12650
AR Path="/5A220C88/5A3194F7" Ref="C90"  Part="1" 
AR Path="/5A018C64/5A3194F7" Ref="C28"  Part="1" 
AR Path="/5A3194F7" Ref="C90"  Part="1" 
F 0 "C90" H 19225 12750 50  0000 L CNN
F 1 "0.1uF" H 19225 12550 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 19238 12500 50  0001 C CNN
F 3 "" H 19200 12650 50  0000 C CNN
	1    19200 12650
	-1   0    0    1   
$EndComp
$Comp
L logic-level-conversion:74HC4050 U9
U 1 1 5A3194FD
P 19700 12700
AR Path="/5A220C88/5A3194FD" Ref="U9"  Part="1" 
AR Path="/5A018C64/5A3194FD" Ref="U7"  Part="1" 
F 0 "U9" H 19700 12847 60  0000 C CNN
F 1 "CD74HC4050PWR" H 19700 12741 60  0000 C CNN
F 2 "Housings_SSOP:TSSOP-16_4.4x5mm_Pitch0.65mm" H 23800 -2600 60  0001 C CNN
F 3 "" H 23800 -2600 60  0001 C CNN
	1    19700 12700
	1    0    0    -1  
$EndComp
Connection ~ 19200 12800
Wire Wire Line
	20900 13400 20200 13400
Wire Wire Line
	20200 13200 20900 13200
Wire Wire Line
	19200 12900 18500 12900
Wire Wire Line
	18500 13100 19200 13100
Wire Wire Line
	19200 13300 18500 13300
Wire Wire Line
	20900 13300 20200 13300
Wire Wire Line
	20900 13500 20200 13500
Wire Wire Line
	19200 13400 18500 13400
Wire Wire Line
	19200 13200 18500 13200
Wire Wire Line
	19200 13000 18500 13000
Text Label 18550 12900 0    60   ~ 0
SAFE_A5
Text Label 18550 13100 0    60   ~ 0
SAFE_A6
Text Label 18550 13300 0    60   ~ 0
SAFE_A7
Text Label 20250 13400 0    60   ~ 0
SAFE_A8
Text Label 20250 13200 0    60   ~ 0
SAFE_A9
Text GLabel 19200 12500 2    60   Input ~ 0
GND
Text GLabel 19200 13500 0    60   Input ~ 0
GND
Wire Wire Line
	18500 12800 19200 12800
Text GLabel 18500 12800 0    60   Input ~ 0
A2V5+
NoConn ~ 20200 12900
Text GLabel 20200 13000 2    60   Input ~ 0
GND
Text Notes 18900 10950 0    60   ~ 0
Logic Level Converters (3.3V to 2.5V)
Wire Wire Line
	18450 7400 17900 7400
Text Label 18000 7400 0    60   ~ 0
OUT16
Wire Wire Line
	18450 7500 17900 7500
Text Label 18000 7500 0    60   ~ 0
OUT17
Wire Wire Line
	18450 7600 17900 7600
Text Label 18000 7600 0    60   ~ 0
OUT18
Wire Wire Line
	18450 7700 17900 7700
Text Label 18000 7700 0    60   ~ 0
OUT19
Wire Wire Line
	18450 7800 17900 7800
Text Label 18000 7800 0    60   ~ 0
OUT20
Wire Wire Line
	18450 7900 17900 7900
Text Label 18000 7900 0    60   ~ 0
OUT21
Wire Wire Line
	18450 8000 17900 8000
Text Label 18000 8000 0    60   ~ 0
OUT22
Wire Wire Line
	18450 8100 17900 8100
Text Label 18000 8100 0    60   ~ 0
OUT23
Wire Wire Line
	18450 8200 17900 8200
Text Label 18000 8200 0    60   ~ 0
OUT24
Wire Wire Line
	18450 8300 17900 8300
Text Label 18000 8300 0    60   ~ 0
OUT25
Wire Wire Line
	18450 8400 17900 8400
Text Label 18000 8400 0    60   ~ 0
OUT26
Wire Wire Line
	18450 8500 17900 8500
Text Label 18000 8500 0    60   ~ 0
OUT27
Text GLabel 5300 6000 0    60   Input ~ 0
P610
Text GLabel 5300 6100 0    60   Input ~ 0
P611
Text GLabel 5300 6200 0    60   Input ~ 0
P612
Text GLabel 5300 6300 0    60   Input ~ 0
P613
Text GLabel 5300 6400 0    60   Input ~ 0
P614
Text GLabel 5300 6500 0    60   Input ~ 0
P615
Text GLabel 5300 6600 0    60   Input ~ 0
P616
Text GLabel 5300 6700 0    60   Input ~ 0
P617
Text GLabel 5300 6800 0    60   Input ~ 0
P618
Text GLabel 5300 6900 0    60   Input ~ 0
P619
Text GLabel 5300 7000 0    60   Input ~ 0
P620
Text GLabel 5300 7100 0    60   Input ~ 0
P621
Text GLabel 5300 7200 0    60   Input ~ 0
P622
Text GLabel 5300 7300 0    60   Input ~ 0
P623
Text GLabel 5300 7400 0    60   Input ~ 0
P624
Text GLabel 5300 7500 0    60   Input ~ 0
P625
Text GLabel 5300 7600 0    60   Input ~ 0
P626
Text GLabel 5300 7700 0    60   Input ~ 0
P627
Text GLabel 5300 7800 0    60   Input ~ 0
P628
Text GLabel 5300 7900 0    60   Input ~ 0
P629
Text GLabel 4200 8200 0    60   Input ~ 0
P600
Text GLabel 4200 8300 0    60   Input ~ 0
P601
Text GLabel 4200 8400 0    60   Input ~ 0
P602
Text GLabel 4200 8500 0    60   Input ~ 0
P603
Text GLabel 4200 8600 0    60   Input ~ 0
P604
Text GLabel 4200 8700 0    60   Input ~ 0
P605
Text GLabel 4200 8800 0    60   Input ~ 0
P606
Text GLabel 4200 8900 0    60   Input ~ 0
P607
Text GLabel 5300 5800 0    60   Input ~ 0
P608
Text GLabel 5300 5900 0    60   Input ~ 0
P609
Text GLabel 5300 8000 0    60   Input ~ 0
P630
Text GLabel 5300 8100 0    60   Input ~ 0
P631
Text GLabel 5300 8200 0    60   Input ~ 0
P632
Text GLabel 5300 8300 0    60   Input ~ 0
P633
Text GLabel 5300 8400 0    60   Input ~ 0
P634
Text GLabel 5300 8500 0    60   Input ~ 0
P635
Text GLabel 5300 8600 0    60   Input ~ 0
P636
Text GLabel 5300 8700 0    60   Input ~ 0
P637
Text GLabel 5300 8800 0    60   Input ~ 0
P638
Text GLabel 5300 8900 0    60   Input ~ 0
P639
Text GLabel 6400 5800 0    60   Input ~ 0
P640
Text GLabel 6400 5900 0    60   Input ~ 0
P641
Text GLabel 6400 6000 0    60   Input ~ 0
P642
Text GLabel 6400 6100 0    60   Input ~ 0
P643
Text GLabel 6400 6200 0    60   Input ~ 0
P644
Text GLabel 6400 6300 0    60   Input ~ 0
P645
Text GLabel 6400 6400 0    60   Input ~ 0
P646
Text GLabel 6400 6500 0    60   Input ~ 0
P647
Text GLabel 6400 6600 0    60   Input ~ 0
P648
Text GLabel 6400 6700 0    60   Input ~ 0
P649
Text GLabel 6400 6800 0    60   Input ~ 0
P650
Text GLabel 6400 6900 0    60   Input ~ 0
P651
Text GLabel 6400 7000 0    60   Input ~ 0
P652
Text GLabel 6400 7100 0    60   Input ~ 0
P653
Text GLabel 6400 7200 0    60   Input ~ 0
P654
Text GLabel 6400 7300 0    60   Input ~ 0
P655
Text GLabel 6400 7400 0    60   Input ~ 0
P656
Text GLabel 6400 7500 0    60   Input ~ 0
P657
Text GLabel 6400 7600 0    60   Input ~ 0
P658
Text GLabel 6400 7700 0    60   Input ~ 0
P659
Text GLabel 6400 7800 0    60   Input ~ 0
P660
Text GLabel 6400 7900 0    60   Input ~ 0
P661
Text GLabel 6400 8000 0    60   Input ~ 0
P662
Text GLabel 6400 8100 0    60   Input ~ 0
P663
Text GLabel 6400 8200 0    60   Input ~ 0
P664
Text GLabel 6400 8300 0    60   Input ~ 0
P665
Text GLabel 6400 8400 0    60   Input ~ 0
P666
Text GLabel 6400 8500 0    60   Input ~ 0
P667
Text GLabel 6400 8600 0    60   Input ~ 0
P668
Text GLabel 6400 8700 0    60   Input ~ 0
P669
Text GLabel 6400 8800 0    60   Input ~ 0
P670
Text GLabel 6400 8900 0    60   Input ~ 0
P671
Text GLabel 7500 5800 0    60   Input ~ 0
P672
Text GLabel 7500 5900 0    60   Input ~ 0
P673
Text GLabel 7500 6000 0    60   Input ~ 0
P674
Text GLabel 7500 6100 0    60   Input ~ 0
P675
Text GLabel 7500 6200 0    60   Input ~ 0
P676
Text GLabel 7500 6300 0    60   Input ~ 0
P677
Text GLabel 7500 6400 0    60   Input ~ 0
P678
Text GLabel 7500 6500 0    60   Input ~ 0
P679
Text GLabel 7500 6600 0    60   Input ~ 0
P680
Text GLabel 7500 6700 0    60   Input ~ 0
P681
Text GLabel 7500 6800 0    60   Input ~ 0
P682
Text GLabel 7500 6900 0    60   Input ~ 0
P683
Text GLabel 7500 7000 0    60   Input ~ 0
P684
Text GLabel 7500 7100 0    60   Input ~ 0
P685
Text GLabel 7500 7200 0    60   Input ~ 0
P686
Text GLabel 7500 7300 0    60   Input ~ 0
P687
Text GLabel 7500 7400 0    60   Input ~ 0
P688
Text GLabel 7500 7500 0    60   Input ~ 0
P689
Text GLabel 7500 7600 0    60   Input ~ 0
P690
Text GLabel 7500 7700 0    60   Input ~ 0
P691
Text GLabel 7500 7800 0    60   Input ~ 0
P692
Text GLabel 7500 7900 0    60   Input ~ 0
P693
Text GLabel 7500 8000 0    60   Input ~ 0
P694
Text GLabel 7500 8100 0    60   Input ~ 0
P695
Text GLabel 7500 8200 0    60   Input ~ 0
P696
Text GLabel 7500 8300 0    60   Input ~ 0
P697
Text GLabel 7500 8400 0    60   Input ~ 0
P698
Text GLabel 7500 8500 0    60   Input ~ 0
P699
Text GLabel 8600 6400 0    60   Input ~ 0
P710
Text GLabel 8600 6500 0    60   Input ~ 0
P711
Text GLabel 8600 6600 0    60   Input ~ 0
P712
Text GLabel 8600 6700 0    60   Input ~ 0
P713
Text GLabel 8600 6800 0    60   Input ~ 0
P714
Text GLabel 8600 6900 0    60   Input ~ 0
P715
Text GLabel 8600 7000 0    60   Input ~ 0
P716
Text GLabel 8600 7100 0    60   Input ~ 0
P717
Text GLabel 8600 7200 0    60   Input ~ 0
P718
Text GLabel 8600 7300 0    60   Input ~ 0
P719
Text GLabel 8600 7400 0    60   Input ~ 0
P720
Text GLabel 8600 7500 0    60   Input ~ 0
P721
Text GLabel 8600 7600 0    60   Input ~ 0
P722
Text GLabel 8600 7700 0    60   Input ~ 0
P723
Text GLabel 8600 7800 0    60   Input ~ 0
P724
Text GLabel 8600 7900 0    60   Input ~ 0
P725
Text GLabel 8600 8000 0    60   Input ~ 0
P726
Text GLabel 8600 8100 0    60   Input ~ 0
P727
Text GLabel 8600 8200 0    60   Input ~ 0
P728
Text GLabel 8600 8300 0    60   Input ~ 0
P729
Text GLabel 7500 8600 0    60   Input ~ 0
P700
Text GLabel 7500 8700 0    60   Input ~ 0
P701
Text GLabel 7500 8800 0    60   Input ~ 0
P702
Text GLabel 7500 8900 0    60   Input ~ 0
P703
Text GLabel 8600 5800 0    60   Input ~ 0
P704
Text GLabel 8600 5900 0    60   Input ~ 0
P705
Text GLabel 8600 6000 0    60   Input ~ 0
P706
Text GLabel 8600 6100 0    60   Input ~ 0
P707
Text GLabel 8600 6200 0    60   Input ~ 0
P708
Text GLabel 8600 6300 0    60   Input ~ 0
P709
Text GLabel 8600 8400 0    60   Input ~ 0
P730
Text GLabel 8600 8500 0    60   Input ~ 0
P731
Text GLabel 8600 8600 0    60   Input ~ 0
P732
Text GLabel 8600 8700 0    60   Input ~ 0
P733
Text GLabel 8600 8800 0    60   Input ~ 0
P734
Text GLabel 8600 8900 0    60   Input ~ 0
P735
Text GLabel 9700 5800 0    60   Input ~ 0
P736
Text GLabel 9700 5900 0    60   Input ~ 0
P737
Text GLabel 9700 6000 0    60   Input ~ 0
P738
Text GLabel 9700 6100 0    60   Input ~ 0
P739
Text GLabel 9700 6200 0    60   Input ~ 0
P740
Text GLabel 9700 6300 0    60   Input ~ 0
P741
Text GLabel 9700 6400 0    60   Input ~ 0
P742
Text GLabel 9700 6500 0    60   Input ~ 0
P743
Text GLabel 9700 6600 0    60   Input ~ 0
P744
Text GLabel 9700 6700 0    60   Input ~ 0
P745
Text GLabel 9700 6800 0    60   Input ~ 0
P746
Text GLabel 9700 6900 0    60   Input ~ 0
P747
Text GLabel 9700 7000 0    60   Input ~ 0
P748
Text GLabel 9700 7100 0    60   Input ~ 0
P749
Text GLabel 9700 7200 0    60   Input ~ 0
P750
Text GLabel 9700 7300 0    60   Input ~ 0
P751
Text GLabel 9700 7400 0    60   Input ~ 0
P752
Text GLabel 9700 7500 0    60   Input ~ 0
P753
Text GLabel 9700 7600 0    60   Input ~ 0
P754
Text GLabel 9700 7700 0    60   Input ~ 0
P755
Text GLabel 9700 7800 0    60   Input ~ 0
P756
Text GLabel 9700 7900 0    60   Input ~ 0
P757
Text GLabel 9700 8000 0    60   Input ~ 0
P758
Text GLabel 9700 8100 0    60   Input ~ 0
P759
Text GLabel 9700 8200 0    60   Input ~ 0
P760
Text GLabel 9700 8300 0    60   Input ~ 0
P761
Text GLabel 9700 8400 0    60   Input ~ 0
P762
Text GLabel 9700 8500 0    60   Input ~ 0
P763
Text GLabel 9700 8600 0    60   Input ~ 0
P764
Text GLabel 9700 8700 0    60   Input ~ 0
P765
Text GLabel 9700 8800 0    60   Input ~ 0
P766
Text GLabel 9700 8900 0    60   Input ~ 0
P767
Text GLabel 10800 5800 0    60   Input ~ 0
P768
Text GLabel 10800 5900 0    60   Input ~ 0
P769
Text GLabel 10800 6000 0    60   Input ~ 0
P770
Text GLabel 10800 6100 0    60   Input ~ 0
P771
Text GLabel 10800 6200 0    60   Input ~ 0
P772
Text GLabel 10800 6300 0    60   Input ~ 0
P773
Text GLabel 10800 6400 0    60   Input ~ 0
P774
Text GLabel 10800 6500 0    60   Input ~ 0
P775
Text GLabel 10800 6600 0    60   Input ~ 0
P776
Text GLabel 10800 6700 0    60   Input ~ 0
P777
Text GLabel 10800 6800 0    60   Input ~ 0
P778
Text GLabel 10800 6900 0    60   Input ~ 0
P779
Text GLabel 10800 7000 0    60   Input ~ 0
P780
Text GLabel 10800 7100 0    60   Input ~ 0
P781
Text GLabel 10800 7200 0    60   Input ~ 0
P782
Text GLabel 10800 7300 0    60   Input ~ 0
P783
Text GLabel 10800 7400 0    60   Input ~ 0
P784
Text GLabel 10800 7500 0    60   Input ~ 0
P785
Text GLabel 10800 7600 0    60   Input ~ 0
P786
Text GLabel 10800 7700 0    60   Input ~ 0
P787
Text GLabel 10800 7800 0    60   Input ~ 0
P788
Text GLabel 10800 7900 0    60   Input ~ 0
P789
Text GLabel 10800 8000 0    60   Input ~ 0
P790
Text GLabel 10800 8100 0    60   Input ~ 0
P791
Text GLabel 10800 8200 0    60   Input ~ 0
P792
Text GLabel 10800 8300 0    60   Input ~ 0
P793
Text GLabel 10800 8400 0    60   Input ~ 0
P794
Text GLabel 10800 8500 0    60   Input ~ 0
P795
Text GLabel 10800 8600 0    60   Input ~ 0
P796
Text GLabel 10800 8700 0    60   Input ~ 0
P797
Text GLabel 10800 8800 0    60   Input ~ 0
P798
Text GLabel 10800 8900 0    60   Input ~ 0
P799
Text GLabel 11900 6800 0    60   Input ~ 0
P810
Text GLabel 11900 6900 0    60   Input ~ 0
P811
Text GLabel 11900 7000 0    60   Input ~ 0
P812
Text GLabel 11900 7100 0    60   Input ~ 0
P813
Text GLabel 11900 7200 0    60   Input ~ 0
P814
Text GLabel 11900 7300 0    60   Input ~ 0
P815
Text GLabel 11900 7400 0    60   Input ~ 0
P816
Text GLabel 11900 7500 0    60   Input ~ 0
P817
Text GLabel 11900 7600 0    60   Input ~ 0
P818
Text GLabel 11900 7700 0    60   Input ~ 0
P819
Text GLabel 11900 7800 0    60   Input ~ 0
P820
Text GLabel 11900 7900 0    60   Input ~ 0
P821
Text GLabel 11900 8000 0    60   Input ~ 0
P822
Text GLabel 11900 8100 0    60   Input ~ 0
P823
Text GLabel 11900 8200 0    60   Input ~ 0
P824
Text GLabel 11900 8300 0    60   Input ~ 0
P825
Text GLabel 11900 8400 0    60   Input ~ 0
P826
Text GLabel 11900 8500 0    60   Input ~ 0
P827
Text GLabel 11900 8600 0    60   Input ~ 0
P828
Text GLabel 11900 8700 0    60   Input ~ 0
P829
Text GLabel 11900 5800 0    60   Input ~ 0
P800
Text GLabel 11900 5900 0    60   Input ~ 0
P801
Text GLabel 11900 6000 0    60   Input ~ 0
P802
Text GLabel 11900 6100 0    60   Input ~ 0
P803
Text GLabel 11900 6200 0    60   Input ~ 0
P804
Text GLabel 11900 6300 0    60   Input ~ 0
P805
Text GLabel 11900 6400 0    60   Input ~ 0
P806
Text GLabel 11900 6500 0    60   Input ~ 0
P807
Text GLabel 11900 6600 0    60   Input ~ 0
P808
Text GLabel 11900 6700 0    60   Input ~ 0
P809
Text GLabel 11900 8800 0    60   Input ~ 0
P830
Text GLabel 11900 8900 0    60   Input ~ 0
P831
Text GLabel 13000 5800 0    60   Input ~ 0
P832
Text GLabel 13000 5900 0    60   Input ~ 0
P833
Text GLabel 13000 6000 0    60   Input ~ 0
P834
Text GLabel 13000 6100 0    60   Input ~ 0
P835
Text GLabel 13000 6200 0    60   Input ~ 0
P836
Text GLabel 13000 6300 0    60   Input ~ 0
P837
Text GLabel 13000 6400 0    60   Input ~ 0
P838
Text GLabel 13000 6500 0    60   Input ~ 0
P839
Text GLabel 13000 6600 0    60   Input ~ 0
P840
Text GLabel 13000 6700 0    60   Input ~ 0
P841
Text GLabel 13000 6800 0    60   Input ~ 0
P842
Text GLabel 13000 6900 0    60   Input ~ 0
P843
Text GLabel 13000 7000 0    60   Input ~ 0
P844
Text GLabel 13000 7100 0    60   Input ~ 0
P845
Text GLabel 13000 7200 0    60   Input ~ 0
P846
Text GLabel 13000 7300 0    60   Input ~ 0
P847
Text GLabel 13000 7400 0    60   Input ~ 0
P848
Text GLabel 13000 7500 0    60   Input ~ 0
P849
Text GLabel 13000 7600 0    60   Input ~ 0
P850
Text GLabel 13000 7700 0    60   Input ~ 0
P851
Text GLabel 13000 7800 0    60   Input ~ 0
P852
Text GLabel 13000 7900 0    60   Input ~ 0
P853
Text GLabel 13000 8000 0    60   Input ~ 0
P854
Text GLabel 13000 8100 0    60   Input ~ 0
P855
Text GLabel 13000 8200 0    60   Input ~ 0
P856
Text GLabel 13000 8300 0    60   Input ~ 0
P857
Text GLabel 13000 8400 0    60   Input ~ 0
P858
Text GLabel 13000 8500 0    60   Input ~ 0
P859
Text GLabel 13000 8600 0    60   Input ~ 0
P860
Text GLabel 13000 8700 0    60   Input ~ 0
P861
Text GLabel 13000 8800 0    60   Input ~ 0
P862
Text GLabel 13000 8900 0    60   Input ~ 0
P863
Text GLabel 14100 5800 0    60   Input ~ 0
P864
Text GLabel 14100 5900 0    60   Input ~ 0
P865
Text GLabel 14100 6000 0    60   Input ~ 0
P866
Text GLabel 14100 6100 0    60   Input ~ 0
P867
Text GLabel 14100 6200 0    60   Input ~ 0
P868
Text GLabel 14100 6300 0    60   Input ~ 0
P869
Text GLabel 14100 6400 0    60   Input ~ 0
P870
Text GLabel 14100 6500 0    60   Input ~ 0
P871
Text GLabel 14100 6600 0    60   Input ~ 0
P872
Text GLabel 14100 6700 0    60   Input ~ 0
P873
Text GLabel 14100 6800 0    60   Input ~ 0
P874
Text GLabel 14100 6900 0    60   Input ~ 0
P875
Text GLabel 14100 7000 0    60   Input ~ 0
P876
Text GLabel 14100 7100 0    60   Input ~ 0
P877
Text GLabel 14100 7200 0    60   Input ~ 0
P878
Text GLabel 14100 7300 0    60   Input ~ 0
P879
Text GLabel 14100 7400 0    60   Input ~ 0
P880
Text GLabel 14100 7500 0    60   Input ~ 0
P881
Text GLabel 14100 7600 0    60   Input ~ 0
P882
Text GLabel 14100 7700 0    60   Input ~ 0
P883
Text GLabel 14100 7800 0    60   Input ~ 0
P884
Text GLabel 14100 7900 0    60   Input ~ 0
P885
Text GLabel 14100 8000 0    60   Input ~ 0
P886
Text GLabel 14100 8100 0    60   Input ~ 0
P887
Text GLabel 14100 8200 0    60   Input ~ 0
P888
Text GLabel 14100 8300 0    60   Input ~ 0
P889
Text GLabel 14100 8400 0    60   Input ~ 0
P890
Text GLabel 14100 8500 0    60   Input ~ 0
P891
Text GLabel 14100 8600 0    60   Input ~ 0
P892
Text GLabel 14100 8700 0    60   Input ~ 0
P893
Text GLabel 14100 8800 0    60   Input ~ 0
P894
Text GLabel 14100 8900 0    60   Input ~ 0
P895
Text GLabel 2000 5800 0    60   Input ~ 0
P512
Text GLabel 2000 5900 0    60   Input ~ 0
P513
Text GLabel 2000 6000 0    60   Input ~ 0
P514
Text GLabel 2000 6100 0    60   Input ~ 0
P515
Text GLabel 2000 6200 0    60   Input ~ 0
P516
Text GLabel 2000 6300 0    60   Input ~ 0
P517
Text GLabel 2000 6400 0    60   Input ~ 0
P518
Text GLabel 2000 6500 0    60   Input ~ 0
P519
Text GLabel 2000 6600 0    60   Input ~ 0
P520
Text GLabel 2000 6700 0    60   Input ~ 0
P521
Text GLabel 2000 6800 0    60   Input ~ 0
P522
Text GLabel 2000 6900 0    60   Input ~ 0
P523
Text GLabel 2000 7000 0    60   Input ~ 0
P524
Text GLabel 2000 7100 0    60   Input ~ 0
P525
Text GLabel 2000 7200 0    60   Input ~ 0
P526
Text GLabel 2000 7300 0    60   Input ~ 0
P527
Text GLabel 2000 7400 0    60   Input ~ 0
P528
Text GLabel 2000 7500 0    60   Input ~ 0
P529
Text GLabel 2000 7600 0    60   Input ~ 0
P530
Text GLabel 2000 7700 0    60   Input ~ 0
P531
Text GLabel 2000 7800 0    60   Input ~ 0
P532
Text GLabel 2000 7900 0    60   Input ~ 0
P533
Text GLabel 2000 8000 0    60   Input ~ 0
P534
Text GLabel 2000 8100 0    60   Input ~ 0
P535
Text GLabel 2000 8200 0    60   Input ~ 0
P536
Text GLabel 2000 8300 0    60   Input ~ 0
P537
Text GLabel 2000 8400 0    60   Input ~ 0
P538
Text GLabel 2000 8500 0    60   Input ~ 0
P539
Text GLabel 2000 8600 0    60   Input ~ 0
P540
Text GLabel 2000 8700 0    60   Input ~ 0
P541
Text GLabel 2000 8800 0    60   Input ~ 0
P542
Text GLabel 2000 8900 0    60   Input ~ 0
P543
Text GLabel 3100 5800 0    60   Input ~ 0
P544
Text GLabel 3100 5900 0    60   Input ~ 0
P545
Text GLabel 3100 6000 0    60   Input ~ 0
P546
Text GLabel 3100 6100 0    60   Input ~ 0
P547
Text GLabel 3100 6200 0    60   Input ~ 0
P548
Text GLabel 3100 6300 0    60   Input ~ 0
P549
Text GLabel 3100 6400 0    60   Input ~ 0
P550
Text GLabel 3100 6500 0    60   Input ~ 0
P551
Text GLabel 3100 6600 0    60   Input ~ 0
P552
Text GLabel 3100 6700 0    60   Input ~ 0
P553
Text GLabel 3100 6800 0    60   Input ~ 0
P554
Text GLabel 3100 6900 0    60   Input ~ 0
P555
Text GLabel 3100 7000 0    60   Input ~ 0
P556
Text GLabel 3100 7100 0    60   Input ~ 0
P557
Text GLabel 3100 7200 0    60   Input ~ 0
P558
Text GLabel 3100 7300 0    60   Input ~ 0
P559
Text GLabel 3100 7400 0    60   Input ~ 0
P560
Text GLabel 3100 7500 0    60   Input ~ 0
P561
Text GLabel 3100 7600 0    60   Input ~ 0
P562
Text GLabel 3100 7700 0    60   Input ~ 0
P563
Text GLabel 3100 7800 0    60   Input ~ 0
P564
Text GLabel 3100 7900 0    60   Input ~ 0
P565
Text GLabel 3100 8000 0    60   Input ~ 0
P566
Text GLabel 3100 8100 0    60   Input ~ 0
P567
Text GLabel 3100 8200 0    60   Input ~ 0
P568
Text GLabel 3100 8300 0    60   Input ~ 0
P569
Text GLabel 3100 8400 0    60   Input ~ 0
P570
Text GLabel 3100 8500 0    60   Input ~ 0
P571
Text GLabel 3100 8600 0    60   Input ~ 0
P572
Text GLabel 3100 8700 0    60   Input ~ 0
P573
Text GLabel 3100 8800 0    60   Input ~ 0
P574
Text GLabel 3100 8900 0    60   Input ~ 0
P575
Text GLabel 4200 5800 0    60   Input ~ 0
P576
Text GLabel 4200 5900 0    60   Input ~ 0
P577
Text GLabel 4200 6000 0    60   Input ~ 0
P578
Text GLabel 4200 6100 0    60   Input ~ 0
P579
Text GLabel 4200 6200 0    60   Input ~ 0
P580
Text GLabel 4200 6300 0    60   Input ~ 0
P581
Text GLabel 4200 6400 0    60   Input ~ 0
P582
Text GLabel 4200 6500 0    60   Input ~ 0
P583
Text GLabel 4200 6600 0    60   Input ~ 0
P584
Text GLabel 4200 6700 0    60   Input ~ 0
P585
Text GLabel 4200 6800 0    60   Input ~ 0
P586
Text GLabel 4200 6900 0    60   Input ~ 0
P587
Text GLabel 4200 7000 0    60   Input ~ 0
P588
Text GLabel 4200 7100 0    60   Input ~ 0
P589
Text GLabel 4200 7200 0    60   Input ~ 0
P590
Text GLabel 4200 7300 0    60   Input ~ 0
P591
Text GLabel 4200 7400 0    60   Input ~ 0
P592
Text GLabel 4200 7500 0    60   Input ~ 0
P593
Text GLabel 4200 7600 0    60   Input ~ 0
P594
Text GLabel 4200 7700 0    60   Input ~ 0
P595
Text GLabel 4200 7800 0    60   Input ~ 0
P596
Text GLabel 4200 7900 0    60   Input ~ 0
P597
Text GLabel 4200 8000 0    60   Input ~ 0
P598
Text GLabel 4200 8100 0    60   Input ~ 0
P599
Text GLabel 5250 2500 0    60   Input ~ 0
P110
Text GLabel 5250 2600 0    60   Input ~ 0
P111
Text GLabel 5250 2700 0    60   Input ~ 0
P112
Text GLabel 5250 2800 0    60   Input ~ 0
P113
Text GLabel 5250 2900 0    60   Input ~ 0
P114
Text GLabel 5250 3000 0    60   Input ~ 0
P115
Text GLabel 5250 3100 0    60   Input ~ 0
P116
Text GLabel 5250 3200 0    60   Input ~ 0
P117
Text GLabel 5250 3300 0    60   Input ~ 0
P118
Text GLabel 5250 3400 0    60   Input ~ 0
P119
Text GLabel 5250 3500 0    60   Input ~ 0
P120
Text GLabel 5250 3600 0    60   Input ~ 0
P121
Text GLabel 5250 3700 0    60   Input ~ 0
P122
Text GLabel 5250 3800 0    60   Input ~ 0
P123
Text GLabel 5250 3900 0    60   Input ~ 0
P124
Text GLabel 5250 4000 0    60   Input ~ 0
P125
Text GLabel 5250 4100 0    60   Input ~ 0
P126
Text GLabel 5250 4200 0    60   Input ~ 0
P127
Text GLabel 6350 1100 0    60   Input ~ 0
P128
Text GLabel 6350 1200 0    60   Input ~ 0
P129
Text GLabel 5250 1500 0    60   Input ~ 0
P100
Text GLabel 5250 1600 0    60   Input ~ 0
P101
Text GLabel 5250 1700 0    60   Input ~ 0
P102
Text GLabel 5250 1800 0    60   Input ~ 0
P103
Text GLabel 5250 1900 0    60   Input ~ 0
P104
Text GLabel 5250 2000 0    60   Input ~ 0
P105
Text GLabel 5250 2100 0    60   Input ~ 0
P106
Text GLabel 5250 2200 0    60   Input ~ 0
P107
Text GLabel 5250 2300 0    60   Input ~ 0
P108
Text GLabel 5250 2400 0    60   Input ~ 0
P109
Text GLabel 6350 1300 0    60   Input ~ 0
P130
Text GLabel 6350 1400 0    60   Input ~ 0
P131
Text GLabel 6350 1500 0    60   Input ~ 0
P132
Text GLabel 6350 1600 0    60   Input ~ 0
P133
Text GLabel 6350 1700 0    60   Input ~ 0
P134
Text GLabel 6350 1800 0    60   Input ~ 0
P135
Text GLabel 6350 1900 0    60   Input ~ 0
P136
Text GLabel 6350 2000 0    60   Input ~ 0
P137
Text GLabel 6350 2100 0    60   Input ~ 0
P138
Text GLabel 6350 2200 0    60   Input ~ 0
P139
Text GLabel 6350 2300 0    60   Input ~ 0
P140
Text GLabel 6350 2400 0    60   Input ~ 0
P141
Text GLabel 6350 2500 0    60   Input ~ 0
P142
Text GLabel 6350 2600 0    60   Input ~ 0
P143
Text GLabel 6350 2700 0    60   Input ~ 0
P144
Text GLabel 6350 2800 0    60   Input ~ 0
P145
Text GLabel 6350 2900 0    60   Input ~ 0
P146
Text GLabel 6350 3000 0    60   Input ~ 0
P147
Text GLabel 6350 3100 0    60   Input ~ 0
P148
Text GLabel 6350 3200 0    60   Input ~ 0
P149
Text GLabel 6350 3300 0    60   Input ~ 0
P150
Text GLabel 6350 3400 0    60   Input ~ 0
P151
Text GLabel 6350 3500 0    60   Input ~ 0
P152
Text GLabel 6350 3600 0    60   Input ~ 0
P153
Text GLabel 6350 3700 0    60   Input ~ 0
P154
Text GLabel 6350 3800 0    60   Input ~ 0
P155
Text GLabel 6350 3900 0    60   Input ~ 0
P156
Text GLabel 6350 4000 0    60   Input ~ 0
P157
Text GLabel 6350 4100 0    60   Input ~ 0
P158
Text GLabel 6350 4200 0    60   Input ~ 0
P159
Text GLabel 7450 1100 0    60   Input ~ 0
P160
Text GLabel 7450 1200 0    60   Input ~ 0
P161
Text GLabel 7450 1300 0    60   Input ~ 0
P162
Text GLabel 7450 1400 0    60   Input ~ 0
P163
Text GLabel 7450 1500 0    60   Input ~ 0
P164
Text GLabel 7450 1600 0    60   Input ~ 0
P165
Text GLabel 7450 1700 0    60   Input ~ 0
P166
Text GLabel 7450 1800 0    60   Input ~ 0
P167
Text GLabel 7450 1900 0    60   Input ~ 0
P168
Text GLabel 7450 2000 0    60   Input ~ 0
P169
Text GLabel 7450 2100 0    60   Input ~ 0
P170
Text GLabel 7450 2200 0    60   Input ~ 0
P171
Text GLabel 7450 2300 0    60   Input ~ 0
P172
Text GLabel 7450 2400 0    60   Input ~ 0
P173
Text GLabel 7450 2500 0    60   Input ~ 0
P174
Text GLabel 7450 2600 0    60   Input ~ 0
P175
Text GLabel 7450 2700 0    60   Input ~ 0
P176
Text GLabel 7450 2800 0    60   Input ~ 0
P177
Text GLabel 7450 2900 0    60   Input ~ 0
P178
Text GLabel 7450 3000 0    60   Input ~ 0
P179
Text GLabel 7450 3100 0    60   Input ~ 0
P180
Text GLabel 7450 3200 0    60   Input ~ 0
P181
Text GLabel 7450 3300 0    60   Input ~ 0
P182
Text GLabel 7450 3400 0    60   Input ~ 0
P183
Text GLabel 7450 3500 0    60   Input ~ 0
P184
Text GLabel 7450 3600 0    60   Input ~ 0
P185
Text GLabel 7450 3700 0    60   Input ~ 0
P186
Text GLabel 7450 3800 0    60   Input ~ 0
P187
Text GLabel 7450 3900 0    60   Input ~ 0
P188
Text GLabel 7450 4000 0    60   Input ~ 0
P189
Text GLabel 7450 4100 0    60   Input ~ 0
P190
Text GLabel 7450 4200 0    60   Input ~ 0
P191
Text GLabel 8550 1100 0    60   Input ~ 0
P192
Text GLabel 8550 1200 0    60   Input ~ 0
P193
Text GLabel 8550 1300 0    60   Input ~ 0
P194
Text GLabel 8550 1400 0    60   Input ~ 0
P195
Text GLabel 8550 1500 0    60   Input ~ 0
P196
Text GLabel 8550 1600 0    60   Input ~ 0
P197
Text GLabel 8550 1700 0    60   Input ~ 0
P198
Text GLabel 8550 1800 0    60   Input ~ 0
P199
Text GLabel 8550 2900 0    60   Input ~ 0
P210
Text GLabel 8550 3000 0    60   Input ~ 0
P211
Text GLabel 8550 3100 0    60   Input ~ 0
P212
Text GLabel 8550 3200 0    60   Input ~ 0
P213
Text GLabel 8550 3300 0    60   Input ~ 0
P214
Text GLabel 8550 3400 0    60   Input ~ 0
P215
Text GLabel 8550 3500 0    60   Input ~ 0
P216
Text GLabel 8550 3600 0    60   Input ~ 0
P217
Text GLabel 8550 3700 0    60   Input ~ 0
P218
Text GLabel 8550 3800 0    60   Input ~ 0
P219
Text GLabel 8550 3900 0    60   Input ~ 0
P220
Text GLabel 8550 4000 0    60   Input ~ 0
P221
Text GLabel 8550 4100 0    60   Input ~ 0
P222
Text GLabel 8550 4200 0    60   Input ~ 0
P223
Text GLabel 9650 1100 0    60   Input ~ 0
P224
Text GLabel 9650 1200 0    60   Input ~ 0
P225
Text GLabel 9650 1300 0    60   Input ~ 0
P226
Text GLabel 9650 1400 0    60   Input ~ 0
P227
Text GLabel 9650 1500 0    60   Input ~ 0
P228
Text GLabel 9650 1600 0    60   Input ~ 0
P229
Text GLabel 8550 1900 0    60   Input ~ 0
P200
Text GLabel 8550 2000 0    60   Input ~ 0
P201
Text GLabel 8550 2100 0    60   Input ~ 0
P202
Text GLabel 8550 2200 0    60   Input ~ 0
P203
Text GLabel 8550 2300 0    60   Input ~ 0
P204
Text GLabel 8550 2400 0    60   Input ~ 0
P205
Text GLabel 8550 2500 0    60   Input ~ 0
P206
Text GLabel 8550 2600 0    60   Input ~ 0
P207
Text GLabel 8550 2700 0    60   Input ~ 0
P208
Text GLabel 8550 2800 0    60   Input ~ 0
P209
Text GLabel 9650 1700 0    60   Input ~ 0
P230
Text GLabel 9650 1800 0    60   Input ~ 0
P231
Text GLabel 9650 1900 0    60   Input ~ 0
P232
Text GLabel 9650 2000 0    60   Input ~ 0
P233
Text GLabel 9650 2100 0    60   Input ~ 0
P234
Text GLabel 9650 2200 0    60   Input ~ 0
P235
Text GLabel 9650 2300 0    60   Input ~ 0
P236
Text GLabel 9650 2400 0    60   Input ~ 0
P237
Text GLabel 9650 2500 0    60   Input ~ 0
P238
Text GLabel 9650 2600 0    60   Input ~ 0
P239
Text GLabel 9650 2700 0    60   Input ~ 0
P240
Text GLabel 9650 2800 0    60   Input ~ 0
P241
Text GLabel 9650 2900 0    60   Input ~ 0
P242
Text GLabel 9650 3000 0    60   Input ~ 0
P243
Text GLabel 9650 3100 0    60   Input ~ 0
P244
Text GLabel 9650 3200 0    60   Input ~ 0
P245
Text GLabel 9650 3300 0    60   Input ~ 0
P246
Text GLabel 9650 3400 0    60   Input ~ 0
P247
Text GLabel 9650 3500 0    60   Input ~ 0
P248
Text GLabel 9650 3600 0    60   Input ~ 0
P249
Text GLabel 9650 3700 0    60   Input ~ 0
P250
Text GLabel 9650 3800 0    60   Input ~ 0
P251
Text GLabel 9650 3900 0    60   Input ~ 0
P252
Text GLabel 9650 4000 0    60   Input ~ 0
P253
Text GLabel 9650 4100 0    60   Input ~ 0
P254
Text GLabel 9650 4200 0    60   Input ~ 0
P255
Text GLabel 10750 1100 0    60   Input ~ 0
P256
Text GLabel 10750 1200 0    60   Input ~ 0
P257
Text GLabel 10750 1300 0    60   Input ~ 0
P258
Text GLabel 10750 1400 0    60   Input ~ 0
P259
Text GLabel 10750 1500 0    60   Input ~ 0
P260
Text GLabel 10750 1600 0    60   Input ~ 0
P261
Text GLabel 10750 1700 0    60   Input ~ 0
P262
Text GLabel 10750 1800 0    60   Input ~ 0
P263
Text GLabel 10750 1900 0    60   Input ~ 0
P264
Text GLabel 10750 2000 0    60   Input ~ 0
P265
Text GLabel 10750 2100 0    60   Input ~ 0
P266
Text GLabel 10750 2200 0    60   Input ~ 0
P267
Text GLabel 10750 2300 0    60   Input ~ 0
P268
Text GLabel 10750 2400 0    60   Input ~ 0
P269
Text GLabel 10750 2500 0    60   Input ~ 0
P270
Text GLabel 10750 2600 0    60   Input ~ 0
P271
Text GLabel 10750 2700 0    60   Input ~ 0
P272
Text GLabel 10750 2800 0    60   Input ~ 0
P273
Text GLabel 10750 2900 0    60   Input ~ 0
P274
Text GLabel 10750 3000 0    60   Input ~ 0
P275
Text GLabel 10750 3100 0    60   Input ~ 0
P276
Text GLabel 10750 3200 0    60   Input ~ 0
P277
Text GLabel 10750 3300 0    60   Input ~ 0
P278
Text GLabel 10750 3400 0    60   Input ~ 0
P279
Text GLabel 10750 3500 0    60   Input ~ 0
P280
Text GLabel 10750 3600 0    60   Input ~ 0
P281
Text GLabel 10750 3700 0    60   Input ~ 0
P282
Text GLabel 10750 3800 0    60   Input ~ 0
P283
Text GLabel 10750 3900 0    60   Input ~ 0
P284
Text GLabel 10750 4000 0    60   Input ~ 0
P285
Text GLabel 10750 4100 0    60   Input ~ 0
P286
Text GLabel 10750 4200 0    60   Input ~ 0
P287
Text GLabel 11850 1100 0    60   Input ~ 0
P288
Text GLabel 11850 1200 0    60   Input ~ 0
P289
Text GLabel 11850 1300 0    60   Input ~ 0
P290
Text GLabel 11850 1400 0    60   Input ~ 0
P291
Text GLabel 11850 1500 0    60   Input ~ 0
P292
Text GLabel 11850 1600 0    60   Input ~ 0
P293
Text GLabel 11850 1700 0    60   Input ~ 0
P294
Text GLabel 11850 1800 0    60   Input ~ 0
P295
Text GLabel 11850 1900 0    60   Input ~ 0
P296
Text GLabel 11850 2000 0    60   Input ~ 0
P297
Text GLabel 11850 2100 0    60   Input ~ 0
P298
Text GLabel 11850 2200 0    60   Input ~ 0
P299
Text GLabel 11850 3300 0    60   Input ~ 0
P310
Text GLabel 11850 3400 0    60   Input ~ 0
P311
Text GLabel 11850 3500 0    60   Input ~ 0
P312
Text GLabel 11850 3600 0    60   Input ~ 0
P313
Text GLabel 11850 3700 0    60   Input ~ 0
P314
Text GLabel 11850 3800 0    60   Input ~ 0
P315
Text GLabel 11850 3900 0    60   Input ~ 0
P316
Text GLabel 11850 4000 0    60   Input ~ 0
P317
Text GLabel 11850 4100 0    60   Input ~ 0
P318
Text GLabel 11850 4200 0    60   Input ~ 0
P319
Text GLabel 12950 1100 0    60   Input ~ 0
P320
Text GLabel 12950 1200 0    60   Input ~ 0
P321
Text GLabel 12950 1300 0    60   Input ~ 0
P322
Text GLabel 12950 1400 0    60   Input ~ 0
P323
Text GLabel 12950 1500 0    60   Input ~ 0
P324
Text GLabel 12950 1600 0    60   Input ~ 0
P325
Text GLabel 12950 1700 0    60   Input ~ 0
P326
Text GLabel 12950 1800 0    60   Input ~ 0
P327
Text GLabel 12950 1900 0    60   Input ~ 0
P328
Text GLabel 12950 2000 0    60   Input ~ 0
P329
Text GLabel 11850 2300 0    60   Input ~ 0
P300
Text GLabel 11850 2400 0    60   Input ~ 0
P301
Text GLabel 11850 2500 0    60   Input ~ 0
P302
Text GLabel 11850 2600 0    60   Input ~ 0
P303
Text GLabel 11850 2700 0    60   Input ~ 0
P304
Text GLabel 11850 2800 0    60   Input ~ 0
P305
Text GLabel 11850 2900 0    60   Input ~ 0
P306
Text GLabel 11850 3000 0    60   Input ~ 0
P307
Text GLabel 11850 3100 0    60   Input ~ 0
P308
Text GLabel 11850 3200 0    60   Input ~ 0
P309
Text GLabel 12950 2100 0    60   Input ~ 0
P330
Text GLabel 12950 2200 0    60   Input ~ 0
P331
Text GLabel 12950 2300 0    60   Input ~ 0
P332
Text GLabel 12950 2400 0    60   Input ~ 0
P333
Text GLabel 12950 2500 0    60   Input ~ 0
P334
Text GLabel 12950 2600 0    60   Input ~ 0
P335
Text GLabel 12950 2700 0    60   Input ~ 0
P336
Text GLabel 12950 2800 0    60   Input ~ 0
P337
Text GLabel 12950 2900 0    60   Input ~ 0
P338
Text GLabel 12950 3000 0    60   Input ~ 0
P339
Text GLabel 12950 3100 0    60   Input ~ 0
P340
Text GLabel 12950 3200 0    60   Input ~ 0
P341
Text GLabel 12950 3300 0    60   Input ~ 0
P342
Text GLabel 12950 3400 0    60   Input ~ 0
P343
Text GLabel 12950 3500 0    60   Input ~ 0
P344
Text GLabel 12950 3600 0    60   Input ~ 0
P345
Text GLabel 12950 3700 0    60   Input ~ 0
P346
Text GLabel 12950 3800 0    60   Input ~ 0
P347
Text GLabel 12950 3900 0    60   Input ~ 0
P348
Text GLabel 12950 4000 0    60   Input ~ 0
P349
Text GLabel 12950 4100 0    60   Input ~ 0
P350
Text GLabel 12950 4200 0    60   Input ~ 0
P351
Text GLabel 14050 1100 0    60   Input ~ 0
P352
Text GLabel 14050 1200 0    60   Input ~ 0
P353
Text GLabel 14050 1300 0    60   Input ~ 0
P354
Text GLabel 14050 1400 0    60   Input ~ 0
P355
Text GLabel 14050 1500 0    60   Input ~ 0
P356
Text GLabel 14050 1600 0    60   Input ~ 0
P357
Text GLabel 14050 1700 0    60   Input ~ 0
P358
Text GLabel 14050 1800 0    60   Input ~ 0
P359
Text GLabel 14050 1900 0    60   Input ~ 0
P360
Text GLabel 14050 2000 0    60   Input ~ 0
P361
Text GLabel 14050 2100 0    60   Input ~ 0
P362
Text GLabel 14050 2200 0    60   Input ~ 0
P363
Text GLabel 14050 2300 0    60   Input ~ 0
P364
Text GLabel 14050 2400 0    60   Input ~ 0
P365
Text GLabel 14050 2500 0    60   Input ~ 0
P366
Text GLabel 14050 2600 0    60   Input ~ 0
P367
Text GLabel 14050 2700 0    60   Input ~ 0
P368
Text GLabel 14050 2800 0    60   Input ~ 0
P369
Text GLabel 14050 2900 0    60   Input ~ 0
P370
Text GLabel 14050 3000 0    60   Input ~ 0
P371
Text GLabel 14050 3100 0    60   Input ~ 0
P372
Text GLabel 14050 3200 0    60   Input ~ 0
P373
Text GLabel 14050 3300 0    60   Input ~ 0
P374
Text GLabel 14050 3400 0    60   Input ~ 0
P375
Text GLabel 14050 3500 0    60   Input ~ 0
P376
Text GLabel 14050 3600 0    60   Input ~ 0
P377
Text GLabel 14050 3700 0    60   Input ~ 0
P378
Text GLabel 14050 3800 0    60   Input ~ 0
P379
Text GLabel 14050 3900 0    60   Input ~ 0
P380
Text GLabel 14050 4000 0    60   Input ~ 0
P381
Text GLabel 14050 4100 0    60   Input ~ 0
P382
Text GLabel 14050 4200 0    60   Input ~ 0
P383
Text GLabel 15150 1100 0    60   Input ~ 0
P384
Text GLabel 15150 1200 0    60   Input ~ 0
P385
Text GLabel 15150 1300 0    60   Input ~ 0
P386
Text GLabel 15150 1400 0    60   Input ~ 0
P387
Text GLabel 15150 1500 0    60   Input ~ 0
P388
Text GLabel 15150 1600 0    60   Input ~ 0
P389
Text GLabel 15150 1700 0    60   Input ~ 0
P390
Text GLabel 15150 1800 0    60   Input ~ 0
P391
Text GLabel 15150 1900 0    60   Input ~ 0
P392
Text GLabel 15150 2000 0    60   Input ~ 0
P393
Text GLabel 15150 2100 0    60   Input ~ 0
P394
Text GLabel 15150 2200 0    60   Input ~ 0
P395
Text GLabel 15150 2300 0    60   Input ~ 0
P396
Text GLabel 15150 2400 0    60   Input ~ 0
P397
Text GLabel 15150 2500 0    60   Input ~ 0
P398
Text GLabel 15150 2600 0    60   Input ~ 0
P399
Text GLabel 15150 3700 0    60   Input ~ 0
P410
Text GLabel 15150 3800 0    60   Input ~ 0
P411
Text GLabel 15150 3900 0    60   Input ~ 0
P412
Text GLabel 15150 4000 0    60   Input ~ 0
P413
Text GLabel 15150 4100 0    60   Input ~ 0
P414
Text GLabel 15150 4200 0    60   Input ~ 0
P415
Text GLabel 16250 1100 0    60   Input ~ 0
P416
Text GLabel 16250 1200 0    60   Input ~ 0
P417
Text GLabel 16250 1300 0    60   Input ~ 0
P418
Text GLabel 16250 1400 0    60   Input ~ 0
P419
Text GLabel 16250 1500 0    60   Input ~ 0
P420
Text GLabel 16250 1600 0    60   Input ~ 0
P421
Text GLabel 16250 1700 0    60   Input ~ 0
P422
Text GLabel 16250 1800 0    60   Input ~ 0
P423
Text GLabel 16250 1900 0    60   Input ~ 0
P424
Text GLabel 16250 2000 0    60   Input ~ 0
P425
Text GLabel 16250 2100 0    60   Input ~ 0
P426
Text GLabel 16250 2200 0    60   Input ~ 0
P427
Text GLabel 16250 2300 0    60   Input ~ 0
P428
Text GLabel 16250 2400 0    60   Input ~ 0
P429
Text GLabel 15150 2700 0    60   Input ~ 0
P400
Text GLabel 15150 2800 0    60   Input ~ 0
P401
Text GLabel 15150 2900 0    60   Input ~ 0
P402
Text GLabel 15150 3000 0    60   Input ~ 0
P403
Text GLabel 15150 3100 0    60   Input ~ 0
P404
Text GLabel 15150 3200 0    60   Input ~ 0
P405
Text GLabel 15150 3300 0    60   Input ~ 0
P406
Text GLabel 15150 3400 0    60   Input ~ 0
P407
Text GLabel 15150 3500 0    60   Input ~ 0
P408
Text GLabel 15150 3600 0    60   Input ~ 0
P409
Text GLabel 16250 2500 0    60   Input ~ 0
P430
Text GLabel 16250 2600 0    60   Input ~ 0
P431
Text GLabel 16250 2700 0    60   Input ~ 0
P432
Text GLabel 16250 2800 0    60   Input ~ 0
P433
Text GLabel 16250 2900 0    60   Input ~ 0
P434
Text GLabel 16250 3000 0    60   Input ~ 0
P435
Text GLabel 16250 3100 0    60   Input ~ 0
P436
Text GLabel 16250 3200 0    60   Input ~ 0
P437
Text GLabel 16250 3300 0    60   Input ~ 0
P438
Text GLabel 16250 3400 0    60   Input ~ 0
P439
Text GLabel 16250 3500 0    60   Input ~ 0
P440
Text GLabel 16250 3600 0    60   Input ~ 0
P441
Text GLabel 16250 3700 0    60   Input ~ 0
P442
Text GLabel 16250 3800 0    60   Input ~ 0
P443
Text GLabel 16250 3900 0    60   Input ~ 0
P444
Text GLabel 16250 4000 0    60   Input ~ 0
P445
Text GLabel 16250 4100 0    60   Input ~ 0
P446
Text GLabel 16250 4200 0    60   Input ~ 0
P447
Text GLabel 17350 1100 0    60   Input ~ 0
P448
Text GLabel 17350 1200 0    60   Input ~ 0
P449
Text GLabel 17350 1300 0    60   Input ~ 0
P450
Text GLabel 17350 1400 0    60   Input ~ 0
P451
Text GLabel 17350 1500 0    60   Input ~ 0
P452
Text GLabel 17350 1600 0    60   Input ~ 0
P453
Text GLabel 17350 1700 0    60   Input ~ 0
P454
Text GLabel 17350 1800 0    60   Input ~ 0
P455
Text GLabel 17350 1900 0    60   Input ~ 0
P456
Text GLabel 17350 2000 0    60   Input ~ 0
P457
Text GLabel 17350 2100 0    60   Input ~ 0
P458
Text GLabel 17350 2200 0    60   Input ~ 0
P459
Text GLabel 17350 2300 0    60   Input ~ 0
P460
Text GLabel 17350 2400 0    60   Input ~ 0
P461
Text GLabel 17350 2500 0    60   Input ~ 0
P462
Text GLabel 17350 2600 0    60   Input ~ 0
P463
Text GLabel 17350 2700 0    60   Input ~ 0
P464
Text GLabel 17350 2800 0    60   Input ~ 0
P465
Text GLabel 17350 2900 0    60   Input ~ 0
P466
Text GLabel 17350 3000 0    60   Input ~ 0
P467
Text GLabel 17350 3100 0    60   Input ~ 0
P468
Text GLabel 17350 3200 0    60   Input ~ 0
P469
Text GLabel 17350 3300 0    60   Input ~ 0
P470
Text GLabel 17350 3400 0    60   Input ~ 0
P471
Text GLabel 17350 3500 0    60   Input ~ 0
P472
Text GLabel 17350 3600 0    60   Input ~ 0
P473
Text GLabel 17350 3700 0    60   Input ~ 0
P474
Text GLabel 17350 3800 0    60   Input ~ 0
P475
Text GLabel 17350 3900 0    60   Input ~ 0
P476
Text GLabel 17350 4000 0    60   Input ~ 0
P477
Text GLabel 17350 4100 0    60   Input ~ 0
P478
Text GLabel 17350 4200 0    60   Input ~ 0
P479
Text GLabel 18450 1100 0    60   Input ~ 0
P480
Text GLabel 18450 1200 0    60   Input ~ 0
P481
Text GLabel 18450 1300 0    60   Input ~ 0
P482
Text GLabel 18450 1400 0    60   Input ~ 0
P483
Text GLabel 18450 1500 0    60   Input ~ 0
P484
Text GLabel 18450 1600 0    60   Input ~ 0
P485
Text GLabel 18450 1700 0    60   Input ~ 0
P486
Text GLabel 18450 1800 0    60   Input ~ 0
P487
Text GLabel 18450 1900 0    60   Input ~ 0
P488
Text GLabel 18450 2000 0    60   Input ~ 0
P489
Text GLabel 18450 2100 0    60   Input ~ 0
P490
Text GLabel 18450 2200 0    60   Input ~ 0
P491
Text GLabel 18450 2300 0    60   Input ~ 0
P492
Text GLabel 18450 2400 0    60   Input ~ 0
P493
Text GLabel 18450 2500 0    60   Input ~ 0
P494
Text GLabel 18450 2600 0    60   Input ~ 0
P495
Text GLabel 18450 2700 0    60   Input ~ 0
P496
Text GLabel 18450 2800 0    60   Input ~ 0
P497
Text GLabel 18450 2900 0    60   Input ~ 0
P498
Text GLabel 18450 3000 0    60   Input ~ 0
P499
Text GLabel 18450 4100 0    60   Input ~ 0
P510
Text GLabel 18450 4200 0    60   Input ~ 0
P511
Text GLabel 18450 3100 0    60   Input ~ 0
P500
Text GLabel 18450 3200 0    60   Input ~ 0
P501
Text GLabel 18450 3300 0    60   Input ~ 0
P502
Text GLabel 18450 3400 0    60   Input ~ 0
P503
Text GLabel 18450 3500 0    60   Input ~ 0
P504
Text GLabel 18450 3600 0    60   Input ~ 0
P505
Text GLabel 18450 3700 0    60   Input ~ 0
P506
Text GLabel 18450 3800 0    60   Input ~ 0
P507
Text GLabel 18450 3900 0    60   Input ~ 0
P508
Text GLabel 18450 4000 0    60   Input ~ 0
P509
Text GLabel 1950 2100 0    60   Input ~ 0
P010
Text GLabel 1950 2200 0    60   Input ~ 0
P011
Text GLabel 1950 2300 0    60   Input ~ 0
P012
Text GLabel 1950 2400 0    60   Input ~ 0
P013
Text GLabel 1950 2500 0    60   Input ~ 0
P014
Text GLabel 1950 2600 0    60   Input ~ 0
P015
Text GLabel 1950 2700 0    60   Input ~ 0
P016
Text GLabel 1950 2800 0    60   Input ~ 0
P017
Text GLabel 1950 2900 0    60   Input ~ 0
P018
Text GLabel 1950 3000 0    60   Input ~ 0
P019
Text GLabel 1950 3100 0    60   Input ~ 0
P020
Text GLabel 1950 3200 0    60   Input ~ 0
P021
Text GLabel 1950 3300 0    60   Input ~ 0
P022
Text GLabel 1950 3400 0    60   Input ~ 0
P023
Text GLabel 1950 3500 0    60   Input ~ 0
P024
Text GLabel 1950 3600 0    60   Input ~ 0
P025
Text GLabel 1950 3700 0    60   Input ~ 0
P026
Text GLabel 1950 3800 0    60   Input ~ 0
P027
Text GLabel 1950 3900 0    60   Input ~ 0
P028
Text GLabel 1950 4000 0    60   Input ~ 0
P029
Text GLabel 1950 1100 0    60   Input ~ 0
P000
Text GLabel 1950 1200 0    60   Input ~ 0
P001
Text GLabel 1950 1300 0    60   Input ~ 0
P002
Text GLabel 1950 1400 0    60   Input ~ 0
P003
Text GLabel 1950 1500 0    60   Input ~ 0
P004
Text GLabel 1950 1600 0    60   Input ~ 0
P005
Text GLabel 1950 1700 0    60   Input ~ 0
P006
Text GLabel 1950 1800 0    60   Input ~ 0
P007
Text GLabel 1950 1900 0    60   Input ~ 0
P008
Text GLabel 1950 2000 0    60   Input ~ 0
P009
Text GLabel 1950 4100 0    60   Input ~ 0
P030
Text GLabel 1950 4200 0    60   Input ~ 0
P031
Text GLabel 3050 1100 0    60   Input ~ 0
P032
Text GLabel 3050 1200 0    60   Input ~ 0
P033
Text GLabel 3050 1300 0    60   Input ~ 0
P034
Text GLabel 3050 1400 0    60   Input ~ 0
P035
Text GLabel 3050 1500 0    60   Input ~ 0
P036
Text GLabel 3050 1600 0    60   Input ~ 0
P037
Text GLabel 3050 1700 0    60   Input ~ 0
P038
Text GLabel 3050 1800 0    60   Input ~ 0
P039
Text GLabel 3050 1900 0    60   Input ~ 0
P040
Text GLabel 3050 2000 0    60   Input ~ 0
P041
Text GLabel 3050 2100 0    60   Input ~ 0
P042
Text GLabel 3050 2200 0    60   Input ~ 0
P043
Text GLabel 3050 2300 0    60   Input ~ 0
P044
Text GLabel 3050 2400 0    60   Input ~ 0
P045
Text GLabel 3050 2500 0    60   Input ~ 0
P046
Text GLabel 3050 2600 0    60   Input ~ 0
P047
Text GLabel 3050 2700 0    60   Input ~ 0
P048
Text GLabel 3050 2800 0    60   Input ~ 0
P049
Text GLabel 3050 2900 0    60   Input ~ 0
P050
Text GLabel 3050 3000 0    60   Input ~ 0
P051
Text GLabel 3050 3100 0    60   Input ~ 0
P052
Text GLabel 3050 3200 0    60   Input ~ 0
P053
Text GLabel 3050 3300 0    60   Input ~ 0
P054
Text GLabel 3050 3400 0    60   Input ~ 0
P055
Text GLabel 3050 3500 0    60   Input ~ 0
P056
Text GLabel 3050 3600 0    60   Input ~ 0
P057
Text GLabel 3050 3700 0    60   Input ~ 0
P058
Text GLabel 3050 3800 0    60   Input ~ 0
P059
Text GLabel 3050 3900 0    60   Input ~ 0
P060
Text GLabel 3050 4000 0    60   Input ~ 0
P061
Text GLabel 3050 4100 0    60   Input ~ 0
P062
Text GLabel 3050 4200 0    60   Input ~ 0
P063
Text GLabel 4150 1100 0    60   Input ~ 0
P064
Text GLabel 4150 1200 0    60   Input ~ 0
P065
Text GLabel 4150 1300 0    60   Input ~ 0
P066
Text GLabel 4150 1400 0    60   Input ~ 0
P067
Text GLabel 4150 1500 0    60   Input ~ 0
P068
Text GLabel 4150 1600 0    60   Input ~ 0
P069
Text GLabel 4150 1700 0    60   Input ~ 0
P070
Text GLabel 4150 1800 0    60   Input ~ 0
P071
Text GLabel 4150 1900 0    60   Input ~ 0
P072
Text GLabel 4150 2000 0    60   Input ~ 0
P073
Text GLabel 4150 2100 0    60   Input ~ 0
P074
Text GLabel 4150 2200 0    60   Input ~ 0
P075
Text GLabel 4150 2300 0    60   Input ~ 0
P076
Text GLabel 4150 2400 0    60   Input ~ 0
P077
Text GLabel 4150 2500 0    60   Input ~ 0
P078
Text GLabel 4150 2600 0    60   Input ~ 0
P079
Text GLabel 4150 2700 0    60   Input ~ 0
P080
Text GLabel 4150 2800 0    60   Input ~ 0
P081
Text GLabel 4150 2900 0    60   Input ~ 0
P082
Text GLabel 4150 3000 0    60   Input ~ 0
P083
Text GLabel 4150 3100 0    60   Input ~ 0
P084
Text GLabel 4150 3200 0    60   Input ~ 0
P085
Text GLabel 4150 3300 0    60   Input ~ 0
P086
Text GLabel 4150 3400 0    60   Input ~ 0
P087
Text GLabel 4150 3500 0    60   Input ~ 0
P088
Text GLabel 4150 3600 0    60   Input ~ 0
P089
Text GLabel 4150 3700 0    60   Input ~ 0
P090
Text GLabel 4150 3800 0    60   Input ~ 0
P091
Text GLabel 4150 3900 0    60   Input ~ 0
P092
Text GLabel 4150 4000 0    60   Input ~ 0
P093
Text GLabel 4150 4100 0    60   Input ~ 0
P094
Text GLabel 4150 4200 0    60   Input ~ 0
P095
Text GLabel 5250 1100 0    60   Input ~ 0
P096
Text GLabel 5250 1200 0    60   Input ~ 0
P097
Text GLabel 5250 1300 0    60   Input ~ 0
P098
Text GLabel 5250 1400 0    60   Input ~ 0
P099
Wire Wire Line
	1950 4350 1550 4350
Text Label 1600 4350 0    60   ~ 0
OUT00
Wire Wire Line
	3050 4350 2650 4350
Text Label 2700 4350 0    60   ~ 0
OUT01
Wire Wire Line
	4150 4350 3750 4350
Text Label 3800 4350 0    60   ~ 0
OUT02
Wire Wire Line
	5250 4350 4850 4350
Text Label 4900 4350 0    60   ~ 0
OUT03
Wire Wire Line
	6350 4350 5950 4350
Text Label 6000 4350 0    60   ~ 0
OUT04
Wire Wire Line
	7450 4350 7050 4350
Text Label 7100 4350 0    60   ~ 0
OUT05
Wire Wire Line
	8550 4350 8150 4350
Text Label 8200 4350 0    60   ~ 0
OUT06
Wire Wire Line
	9650 4350 9250 4350
Text Label 9300 4350 0    60   ~ 0
OUT07
Wire Wire Line
	10750 4350 10350 4350
Text Label 10400 4350 0    60   ~ 0
OUT08
Wire Wire Line
	11850 4350 11450 4350
Text Label 11500 4350 0    60   ~ 0
OUT09
Wire Wire Line
	12950 4350 12550 4350
Text Label 12600 4350 0    60   ~ 0
OUT10
Wire Wire Line
	14050 4350 13650 4350
Text Label 13700 4350 0    60   ~ 0
OUT11
Wire Wire Line
	15150 4350 14750 4350
Text Label 14800 4350 0    60   ~ 0
OUT12
Wire Wire Line
	16250 4350 15850 4350
Text Label 15900 4350 0    60   ~ 0
OUT13
Wire Wire Line
	17350 4350 16950 4350
Text Label 17000 4350 0    60   ~ 0
OUT14
$Sheet
S 18450 1000 600  4150
U 5A33FD79
F0 "sheet5A33FB23" 60
F1 "mux.sch" 60
F2 "MUX_IN01" B L 18450 1100 60 
F3 "MUX_IN02" B L 18450 1200 60 
F4 "MUX_IN03" B L 18450 1300 60 
F5 "MUX_IN04" B L 18450 1400 60 
F6 "MUX_IN05" B L 18450 1500 60 
F7 "MUX_IN06" B L 18450 1600 60 
F8 "MUX_IN07" B L 18450 1700 60 
F9 "MUX_IN08" B L 18450 1800 60 
F10 "MUX_IN09" B L 18450 1900 60 
F11 "MUX_IN10" B L 18450 2000 60 
F12 "MUX_IN11" B L 18450 2100 60 
F13 "MUX_IN12" B L 18450 2200 60 
F14 "MUX_IN14" B L 18450 2400 60 
F15 "MUX_IN15" B L 18450 2500 60 
F16 "MUX_IN16" B L 18450 2600 60 
F17 "MUX_OUT" B L 18450 4350 60 
F18 "MUX_IN32" B L 18450 4200 60 
F19 "MUX_IN31" B L 18450 4100 60 
F20 "MUX_IN30" B L 18450 4000 60 
F21 "MUX_IN29" B L 18450 3900 60 
F22 "MUX_IN28" B L 18450 3800 60 
F23 "MUX_IN27" B L 18450 3700 60 
F24 "MUX_IN26" B L 18450 3600 60 
F25 "MUX_IN25" B L 18450 3500 60 
F26 "MUX_IN24" B L 18450 3400 60 
F27 "MUX_IN23" B L 18450 3300 60 
F28 "MUX_IN22" B L 18450 3200 60 
F29 "MUX_IN21" B L 18450 3100 60 
F30 "MUX_IN20" B L 18450 3000 60 
F31 "MUX_IN19" B L 18450 2900 60 
F32 "MUX_IN18" B L 18450 2800 60 
F33 "MUX_IN17" B L 18450 2700 60 
F34 "MUX_A0" I L 18450 4500 60 
F35 "MUX_A1" I L 18450 4600 60 
F36 "MUX_A2" I L 18450 4700 60 
F37 "MUX_A3" I L 18450 4800 60 
F38 "MUX_A4" I L 18450 4900 60 
F39 "MUX_EN" I L 18450 5050 60 
F40 "MUX_IN13" B L 18450 2300 60 
$EndSheet
Wire Wire Line
	18450 4350 18050 4350
Text Label 18100 4350 0    60   ~ 0
OUT15
Wire Wire Line
	18450 4500 18050 4500
Text Label 18050 4500 0    60   ~ 0
SAFE_A0
Text Label 18050 4600 0    60   ~ 0
SAFE_A1
Text Label 18050 4700 0    60   ~ 0
SAFE_A2
Text Label 18050 4800 0    60   ~ 0
SAFE_A3
Text Label 18050 4900 0    60   ~ 0
SAFE_A4
Text Label 18050 5050 0    60   ~ 0
SAFE_EN
Wire Wire Line
	18050 4600 18450 4600
Wire Wire Line
	18450 4700 18050 4700
Wire Wire Line
	18050 4800 18450 4800
Wire Wire Line
	18050 4900 18450 4900
Wire Wire Line
	18050 5050 18450 5050
$Sheet
S 17350 1000 600  4150
U 5A33FE25
F0 "sheet5A33FB24" 60
F1 "mux.sch" 60
F2 "MUX_IN01" B L 17350 1100 60 
F3 "MUX_IN02" B L 17350 1200 60 
F4 "MUX_IN03" B L 17350 1300 60 
F5 "MUX_IN04" B L 17350 1400 60 
F6 "MUX_IN05" B L 17350 1500 60 
F7 "MUX_IN06" B L 17350 1600 60 
F8 "MUX_IN07" B L 17350 1700 60 
F9 "MUX_IN08" B L 17350 1800 60 
F10 "MUX_IN09" B L 17350 1900 60 
F11 "MUX_IN10" B L 17350 2000 60 
F12 "MUX_IN11" B L 17350 2100 60 
F13 "MUX_IN12" B L 17350 2200 60 
F14 "MUX_IN14" B L 17350 2400 60 
F15 "MUX_IN15" B L 17350 2500 60 
F16 "MUX_IN16" B L 17350 2600 60 
F17 "MUX_OUT" B L 17350 4350 60 
F18 "MUX_IN32" B L 17350 4200 60 
F19 "MUX_IN31" B L 17350 4100 60 
F20 "MUX_IN30" B L 17350 4000 60 
F21 "MUX_IN29" B L 17350 3900 60 
F22 "MUX_IN28" B L 17350 3800 60 
F23 "MUX_IN27" B L 17350 3700 60 
F24 "MUX_IN26" B L 17350 3600 60 
F25 "MUX_IN25" B L 17350 3500 60 
F26 "MUX_IN24" B L 17350 3400 60 
F27 "MUX_IN23" B L 17350 3300 60 
F28 "MUX_IN22" B L 17350 3200 60 
F29 "MUX_IN21" B L 17350 3100 60 
F30 "MUX_IN20" B L 17350 3000 60 
F31 "MUX_IN19" B L 17350 2900 60 
F32 "MUX_IN18" B L 17350 2800 60 
F33 "MUX_IN17" B L 17350 2700 60 
F34 "MUX_A0" I L 17350 4500 60 
F35 "MUX_A1" I L 17350 4600 60 
F36 "MUX_A2" I L 17350 4700 60 
F37 "MUX_A3" I L 17350 4800 60 
F38 "MUX_A4" I L 17350 4900 60 
F39 "MUX_EN" I L 17350 5050 60 
F40 "MUX_IN13" B L 17350 2300 60 
$EndSheet
Wire Wire Line
	17350 4500 16950 4500
Text Label 16950 4500 0    60   ~ 0
SAFE_A0
Text Label 16950 4600 0    60   ~ 0
SAFE_A1
Text Label 16950 4700 0    60   ~ 0
SAFE_A2
Text Label 16950 4800 0    60   ~ 0
SAFE_A3
Text Label 16950 4900 0    60   ~ 0
SAFE_A4
Text Label 16950 5050 0    60   ~ 0
SAFE_EN
Wire Wire Line
	16950 4600 17350 4600
Wire Wire Line
	17350 4700 16950 4700
Wire Wire Line
	16950 4800 17350 4800
Wire Wire Line
	16950 4900 17350 4900
Wire Wire Line
	16950 5050 17350 5050
$Sheet
S 16250 1000 600  4150
U 5A33FECF
F0 "sheet5A33FB25" 60
F1 "mux.sch" 60
F2 "MUX_IN01" B L 16250 1100 60 
F3 "MUX_IN02" B L 16250 1200 60 
F4 "MUX_IN03" B L 16250 1300 60 
F5 "MUX_IN04" B L 16250 1400 60 
F6 "MUX_IN05" B L 16250 1500 60 
F7 "MUX_IN06" B L 16250 1600 60 
F8 "MUX_IN07" B L 16250 1700 60 
F9 "MUX_IN08" B L 16250 1800 60 
F10 "MUX_IN09" B L 16250 1900 60 
F11 "MUX_IN10" B L 16250 2000 60 
F12 "MUX_IN11" B L 16250 2100 60 
F13 "MUX_IN12" B L 16250 2200 60 
F14 "MUX_IN14" B L 16250 2400 60 
F15 "MUX_IN15" B L 16250 2500 60 
F16 "MUX_IN16" B L 16250 2600 60 
F17 "MUX_OUT" B L 16250 4350 60 
F18 "MUX_IN32" B L 16250 4200 60 
F19 "MUX_IN31" B L 16250 4100 60 
F20 "MUX_IN30" B L 16250 4000 60 
F21 "MUX_IN29" B L 16250 3900 60 
F22 "MUX_IN28" B L 16250 3800 60 
F23 "MUX_IN27" B L 16250 3700 60 
F24 "MUX_IN26" B L 16250 3600 60 
F25 "MUX_IN25" B L 16250 3500 60 
F26 "MUX_IN24" B L 16250 3400 60 
F27 "MUX_IN23" B L 16250 3300 60 
F28 "MUX_IN22" B L 16250 3200 60 
F29 "MUX_IN21" B L 16250 3100 60 
F30 "MUX_IN20" B L 16250 3000 60 
F31 "MUX_IN19" B L 16250 2900 60 
F32 "MUX_IN18" B L 16250 2800 60 
F33 "MUX_IN17" B L 16250 2700 60 
F34 "MUX_A0" I L 16250 4500 60 
F35 "MUX_A1" I L 16250 4600 60 
F36 "MUX_A2" I L 16250 4700 60 
F37 "MUX_A3" I L 16250 4800 60 
F38 "MUX_A4" I L 16250 4900 60 
F39 "MUX_EN" I L 16250 5050 60 
F40 "MUX_IN13" B L 16250 2300 60 
$EndSheet
Wire Wire Line
	16250 4500 15850 4500
Text Label 15850 4500 0    60   ~ 0
SAFE_A0
Text Label 15850 4600 0    60   ~ 0
SAFE_A1
Text Label 15850 4700 0    60   ~ 0
SAFE_A2
Text Label 15850 4800 0    60   ~ 0
SAFE_A3
Text Label 15850 4900 0    60   ~ 0
SAFE_A4
Text Label 15850 5050 0    60   ~ 0
SAFE_EN
Wire Wire Line
	15850 4600 16250 4600
Wire Wire Line
	16250 4700 15850 4700
Wire Wire Line
	15850 4800 16250 4800
Wire Wire Line
	15850 4900 16250 4900
Wire Wire Line
	15850 5050 16250 5050
$Sheet
S 15150 1000 600  4150
U 5A33FF79
F0 "sheet5A33FB26" 60
F1 "mux.sch" 60
F2 "MUX_IN01" B L 15150 1100 60 
F3 "MUX_IN02" B L 15150 1200 60 
F4 "MUX_IN03" B L 15150 1300 60 
F5 "MUX_IN04" B L 15150 1400 60 
F6 "MUX_IN05" B L 15150 1500 60 
F7 "MUX_IN06" B L 15150 1600 60 
F8 "MUX_IN07" B L 15150 1700 60 
F9 "MUX_IN08" B L 15150 1800 60 
F10 "MUX_IN09" B L 15150 1900 60 
F11 "MUX_IN10" B L 15150 2000 60 
F12 "MUX_IN11" B L 15150 2100 60 
F13 "MUX_IN12" B L 15150 2200 60 
F14 "MUX_IN14" B L 15150 2400 60 
F15 "MUX_IN15" B L 15150 2500 60 
F16 "MUX_IN16" B L 15150 2600 60 
F17 "MUX_OUT" B L 15150 4350 60 
F18 "MUX_IN32" B L 15150 4200 60 
F19 "MUX_IN31" B L 15150 4100 60 
F20 "MUX_IN30" B L 15150 4000 60 
F21 "MUX_IN29" B L 15150 3900 60 
F22 "MUX_IN28" B L 15150 3800 60 
F23 "MUX_IN27" B L 15150 3700 60 
F24 "MUX_IN26" B L 15150 3600 60 
F25 "MUX_IN25" B L 15150 3500 60 
F26 "MUX_IN24" B L 15150 3400 60 
F27 "MUX_IN23" B L 15150 3300 60 
F28 "MUX_IN22" B L 15150 3200 60 
F29 "MUX_IN21" B L 15150 3100 60 
F30 "MUX_IN20" B L 15150 3000 60 
F31 "MUX_IN19" B L 15150 2900 60 
F32 "MUX_IN18" B L 15150 2800 60 
F33 "MUX_IN17" B L 15150 2700 60 
F34 "MUX_A0" I L 15150 4500 60 
F35 "MUX_A1" I L 15150 4600 60 
F36 "MUX_A2" I L 15150 4700 60 
F37 "MUX_A3" I L 15150 4800 60 
F38 "MUX_A4" I L 15150 4900 60 
F39 "MUX_EN" I L 15150 5050 60 
F40 "MUX_IN13" B L 15150 2300 60 
$EndSheet
Wire Wire Line
	15150 4500 14750 4500
Text Label 14750 4500 0    60   ~ 0
SAFE_A0
Text Label 14750 4600 0    60   ~ 0
SAFE_A1
Text Label 14750 4700 0    60   ~ 0
SAFE_A2
Text Label 14750 4800 0    60   ~ 0
SAFE_A3
Text Label 14750 4900 0    60   ~ 0
SAFE_A4
Text Label 14750 5050 0    60   ~ 0
SAFE_EN
Wire Wire Line
	14750 4600 15150 4600
Wire Wire Line
	15150 4700 14750 4700
Wire Wire Line
	14750 4800 15150 4800
Wire Wire Line
	14750 4900 15150 4900
Wire Wire Line
	14750 5050 15150 5050
$Sheet
S 14050 1000 600  4150
U 5A340023
F0 "sheet5A33FB27" 60
F1 "mux.sch" 60
F2 "MUX_IN01" B L 14050 1100 60 
F3 "MUX_IN02" B L 14050 1200 60 
F4 "MUX_IN03" B L 14050 1300 60 
F5 "MUX_IN04" B L 14050 1400 60 
F6 "MUX_IN05" B L 14050 1500 60 
F7 "MUX_IN06" B L 14050 1600 60 
F8 "MUX_IN07" B L 14050 1700 60 
F9 "MUX_IN08" B L 14050 1800 60 
F10 "MUX_IN09" B L 14050 1900 60 
F11 "MUX_IN10" B L 14050 2000 60 
F12 "MUX_IN11" B L 14050 2100 60 
F13 "MUX_IN12" B L 14050 2200 60 
F14 "MUX_IN14" B L 14050 2400 60 
F15 "MUX_IN15" B L 14050 2500 60 
F16 "MUX_IN16" B L 14050 2600 60 
F17 "MUX_OUT" B L 14050 4350 60 
F18 "MUX_IN32" B L 14050 4200 60 
F19 "MUX_IN31" B L 14050 4100 60 
F20 "MUX_IN30" B L 14050 4000 60 
F21 "MUX_IN29" B L 14050 3900 60 
F22 "MUX_IN28" B L 14050 3800 60 
F23 "MUX_IN27" B L 14050 3700 60 
F24 "MUX_IN26" B L 14050 3600 60 
F25 "MUX_IN25" B L 14050 3500 60 
F26 "MUX_IN24" B L 14050 3400 60 
F27 "MUX_IN23" B L 14050 3300 60 
F28 "MUX_IN22" B L 14050 3200 60 
F29 "MUX_IN21" B L 14050 3100 60 
F30 "MUX_IN20" B L 14050 3000 60 
F31 "MUX_IN19" B L 14050 2900 60 
F32 "MUX_IN18" B L 14050 2800 60 
F33 "MUX_IN17" B L 14050 2700 60 
F34 "MUX_A0" I L 14050 4500 60 
F35 "MUX_A1" I L 14050 4600 60 
F36 "MUX_A2" I L 14050 4700 60 
F37 "MUX_A3" I L 14050 4800 60 
F38 "MUX_A4" I L 14050 4900 60 
F39 "MUX_EN" I L 14050 5050 60 
F40 "MUX_IN13" B L 14050 2300 60 
$EndSheet
Wire Wire Line
	14050 4500 13650 4500
Text Label 13650 4500 0    60   ~ 0
SAFE_A0
Text Label 13650 4600 0    60   ~ 0
SAFE_A1
Text Label 13650 4700 0    60   ~ 0
SAFE_A2
Text Label 13650 4800 0    60   ~ 0
SAFE_A3
Text Label 13650 4900 0    60   ~ 0
SAFE_A4
Text Label 13650 5050 0    60   ~ 0
SAFE_EN
Wire Wire Line
	13650 4600 14050 4600
Wire Wire Line
	14050 4700 13650 4700
Wire Wire Line
	13650 4800 14050 4800
Wire Wire Line
	13650 4900 14050 4900
Wire Wire Line
	13650 5050 14050 5050
$Sheet
S 12950 1000 600  4150
U 5A3400CD
F0 "sheet5A33FB28" 60
F1 "mux.sch" 60
F2 "MUX_IN01" B L 12950 1100 60 
F3 "MUX_IN02" B L 12950 1200 60 
F4 "MUX_IN03" B L 12950 1300 60 
F5 "MUX_IN04" B L 12950 1400 60 
F6 "MUX_IN05" B L 12950 1500 60 
F7 "MUX_IN06" B L 12950 1600 60 
F8 "MUX_IN07" B L 12950 1700 60 
F9 "MUX_IN08" B L 12950 1800 60 
F10 "MUX_IN09" B L 12950 1900 60 
F11 "MUX_IN10" B L 12950 2000 60 
F12 "MUX_IN11" B L 12950 2100 60 
F13 "MUX_IN12" B L 12950 2200 60 
F14 "MUX_IN14" B L 12950 2400 60 
F15 "MUX_IN15" B L 12950 2500 60 
F16 "MUX_IN16" B L 12950 2600 60 
F17 "MUX_OUT" B L 12950 4350 60 
F18 "MUX_IN32" B L 12950 4200 60 
F19 "MUX_IN31" B L 12950 4100 60 
F20 "MUX_IN30" B L 12950 4000 60 
F21 "MUX_IN29" B L 12950 3900 60 
F22 "MUX_IN28" B L 12950 3800 60 
F23 "MUX_IN27" B L 12950 3700 60 
F24 "MUX_IN26" B L 12950 3600 60 
F25 "MUX_IN25" B L 12950 3500 60 
F26 "MUX_IN24" B L 12950 3400 60 
F27 "MUX_IN23" B L 12950 3300 60 
F28 "MUX_IN22" B L 12950 3200 60 
F29 "MUX_IN21" B L 12950 3100 60 
F30 "MUX_IN20" B L 12950 3000 60 
F31 "MUX_IN19" B L 12950 2900 60 
F32 "MUX_IN18" B L 12950 2800 60 
F33 "MUX_IN17" B L 12950 2700 60 
F34 "MUX_A0" I L 12950 4500 60 
F35 "MUX_A1" I L 12950 4600 60 
F36 "MUX_A2" I L 12950 4700 60 
F37 "MUX_A3" I L 12950 4800 60 
F38 "MUX_A4" I L 12950 4900 60 
F39 "MUX_EN" I L 12950 5050 60 
F40 "MUX_IN13" B L 12950 2300 60 
$EndSheet
Wire Wire Line
	12950 4500 12550 4500
Text Label 12550 4500 0    60   ~ 0
SAFE_A0
Text Label 12550 4600 0    60   ~ 0
SAFE_A1
Text Label 12550 4700 0    60   ~ 0
SAFE_A2
Text Label 12550 4800 0    60   ~ 0
SAFE_A3
Text Label 12550 4900 0    60   ~ 0
SAFE_A4
Text Label 12550 5050 0    60   ~ 0
SAFE_EN
Wire Wire Line
	12550 4600 12950 4600
Wire Wire Line
	12950 4700 12550 4700
Wire Wire Line
	12550 4800 12950 4800
Wire Wire Line
	12550 4900 12950 4900
Wire Wire Line
	12550 5050 12950 5050
$Sheet
S 11850 1000 600  4150
U 5A340177
F0 "sheet5A33FB29" 60
F1 "mux.sch" 60
F2 "MUX_IN01" B L 11850 1100 60 
F3 "MUX_IN02" B L 11850 1200 60 
F4 "MUX_IN03" B L 11850 1300 60 
F5 "MUX_IN04" B L 11850 1400 60 
F6 "MUX_IN05" B L 11850 1500 60 
F7 "MUX_IN06" B L 11850 1600 60 
F8 "MUX_IN07" B L 11850 1700 60 
F9 "MUX_IN08" B L 11850 1800 60 
F10 "MUX_IN09" B L 11850 1900 60 
F11 "MUX_IN10" B L 11850 2000 60 
F12 "MUX_IN11" B L 11850 2100 60 
F13 "MUX_IN12" B L 11850 2200 60 
F14 "MUX_IN14" B L 11850 2400 60 
F15 "MUX_IN15" B L 11850 2500 60 
F16 "MUX_IN16" B L 11850 2600 60 
F17 "MUX_OUT" B L 11850 4350 60 
F18 "MUX_IN32" B L 11850 4200 60 
F19 "MUX_IN31" B L 11850 4100 60 
F20 "MUX_IN30" B L 11850 4000 60 
F21 "MUX_IN29" B L 11850 3900 60 
F22 "MUX_IN28" B L 11850 3800 60 
F23 "MUX_IN27" B L 11850 3700 60 
F24 "MUX_IN26" B L 11850 3600 60 
F25 "MUX_IN25" B L 11850 3500 60 
F26 "MUX_IN24" B L 11850 3400 60 
F27 "MUX_IN23" B L 11850 3300 60 
F28 "MUX_IN22" B L 11850 3200 60 
F29 "MUX_IN21" B L 11850 3100 60 
F30 "MUX_IN20" B L 11850 3000 60 
F31 "MUX_IN19" B L 11850 2900 60 
F32 "MUX_IN18" B L 11850 2800 60 
F33 "MUX_IN17" B L 11850 2700 60 
F34 "MUX_A0" I L 11850 4500 60 
F35 "MUX_A1" I L 11850 4600 60 
F36 "MUX_A2" I L 11850 4700 60 
F37 "MUX_A3" I L 11850 4800 60 
F38 "MUX_A4" I L 11850 4900 60 
F39 "MUX_EN" I L 11850 5050 60 
F40 "MUX_IN13" B L 11850 2300 60 
$EndSheet
Wire Wire Line
	11850 4500 11450 4500
Text Label 11450 4500 0    60   ~ 0
SAFE_A0
Text Label 11450 4600 0    60   ~ 0
SAFE_A1
Text Label 11450 4700 0    60   ~ 0
SAFE_A2
Text Label 11450 4800 0    60   ~ 0
SAFE_A3
Text Label 11450 4900 0    60   ~ 0
SAFE_A4
Text Label 11450 5050 0    60   ~ 0
SAFE_EN
Wire Wire Line
	11450 4600 11850 4600
Wire Wire Line
	11850 4700 11450 4700
Wire Wire Line
	11450 4800 11850 4800
Wire Wire Line
	11450 4900 11850 4900
Wire Wire Line
	11450 5050 11850 5050
$Sheet
S 10750 1000 600  4150
U 5A340221
F0 "sheet5A33FB2A" 60
F1 "mux.sch" 60
F2 "MUX_IN01" B L 10750 1100 60 
F3 "MUX_IN02" B L 10750 1200 60 
F4 "MUX_IN03" B L 10750 1300 60 
F5 "MUX_IN04" B L 10750 1400 60 
F6 "MUX_IN05" B L 10750 1500 60 
F7 "MUX_IN06" B L 10750 1600 60 
F8 "MUX_IN07" B L 10750 1700 60 
F9 "MUX_IN08" B L 10750 1800 60 
F10 "MUX_IN09" B L 10750 1900 60 
F11 "MUX_IN10" B L 10750 2000 60 
F12 "MUX_IN11" B L 10750 2100 60 
F13 "MUX_IN12" B L 10750 2200 60 
F14 "MUX_IN14" B L 10750 2400 60 
F15 "MUX_IN15" B L 10750 2500 60 
F16 "MUX_IN16" B L 10750 2600 60 
F17 "MUX_OUT" B L 10750 4350 60 
F18 "MUX_IN32" B L 10750 4200 60 
F19 "MUX_IN31" B L 10750 4100 60 
F20 "MUX_IN30" B L 10750 4000 60 
F21 "MUX_IN29" B L 10750 3900 60 
F22 "MUX_IN28" B L 10750 3800 60 
F23 "MUX_IN27" B L 10750 3700 60 
F24 "MUX_IN26" B L 10750 3600 60 
F25 "MUX_IN25" B L 10750 3500 60 
F26 "MUX_IN24" B L 10750 3400 60 
F27 "MUX_IN23" B L 10750 3300 60 
F28 "MUX_IN22" B L 10750 3200 60 
F29 "MUX_IN21" B L 10750 3100 60 
F30 "MUX_IN20" B L 10750 3000 60 
F31 "MUX_IN19" B L 10750 2900 60 
F32 "MUX_IN18" B L 10750 2800 60 
F33 "MUX_IN17" B L 10750 2700 60 
F34 "MUX_A0" I L 10750 4500 60 
F35 "MUX_A1" I L 10750 4600 60 
F36 "MUX_A2" I L 10750 4700 60 
F37 "MUX_A3" I L 10750 4800 60 
F38 "MUX_A4" I L 10750 4900 60 
F39 "MUX_EN" I L 10750 5050 60 
F40 "MUX_IN13" B L 10750 2300 60 
$EndSheet
Wire Wire Line
	10750 4500 10350 4500
Text Label 10350 4500 0    60   ~ 0
SAFE_A0
Text Label 10350 4600 0    60   ~ 0
SAFE_A1
Text Label 10350 4700 0    60   ~ 0
SAFE_A2
Text Label 10350 4800 0    60   ~ 0
SAFE_A3
Text Label 10350 4900 0    60   ~ 0
SAFE_A4
Text Label 10350 5050 0    60   ~ 0
SAFE_EN
Wire Wire Line
	10350 4600 10750 4600
Wire Wire Line
	10750 4700 10350 4700
Wire Wire Line
	10350 4800 10750 4800
Wire Wire Line
	10350 4900 10750 4900
Wire Wire Line
	10350 5050 10750 5050
$Sheet
S 9650 1000 600  4150
U 5A3402CB
F0 "sheet5A33FB2B" 60
F1 "mux.sch" 60
F2 "MUX_IN01" B L 9650 1100 60 
F3 "MUX_IN02" B L 9650 1200 60 
F4 "MUX_IN03" B L 9650 1300 60 
F5 "MUX_IN04" B L 9650 1400 60 
F6 "MUX_IN05" B L 9650 1500 60 
F7 "MUX_IN06" B L 9650 1600 60 
F8 "MUX_IN07" B L 9650 1700 60 
F9 "MUX_IN08" B L 9650 1800 60 
F10 "MUX_IN09" B L 9650 1900 60 
F11 "MUX_IN10" B L 9650 2000 60 
F12 "MUX_IN11" B L 9650 2100 60 
F13 "MUX_IN12" B L 9650 2200 60 
F14 "MUX_IN14" B L 9650 2400 60 
F15 "MUX_IN15" B L 9650 2500 60 
F16 "MUX_IN16" B L 9650 2600 60 
F17 "MUX_OUT" B L 9650 4350 60 
F18 "MUX_IN32" B L 9650 4200 60 
F19 "MUX_IN31" B L 9650 4100 60 
F20 "MUX_IN30" B L 9650 4000 60 
F21 "MUX_IN29" B L 9650 3900 60 
F22 "MUX_IN28" B L 9650 3800 60 
F23 "MUX_IN27" B L 9650 3700 60 
F24 "MUX_IN26" B L 9650 3600 60 
F25 "MUX_IN25" B L 9650 3500 60 
F26 "MUX_IN24" B L 9650 3400 60 
F27 "MUX_IN23" B L 9650 3300 60 
F28 "MUX_IN22" B L 9650 3200 60 
F29 "MUX_IN21" B L 9650 3100 60 
F30 "MUX_IN20" B L 9650 3000 60 
F31 "MUX_IN19" B L 9650 2900 60 
F32 "MUX_IN18" B L 9650 2800 60 
F33 "MUX_IN17" B L 9650 2700 60 
F34 "MUX_A0" I L 9650 4500 60 
F35 "MUX_A1" I L 9650 4600 60 
F36 "MUX_A2" I L 9650 4700 60 
F37 "MUX_A3" I L 9650 4800 60 
F38 "MUX_A4" I L 9650 4900 60 
F39 "MUX_EN" I L 9650 5050 60 
F40 "MUX_IN13" B L 9650 2300 60 
$EndSheet
Wire Wire Line
	9650 4500 9250 4500
Text Label 9250 4500 0    60   ~ 0
SAFE_A0
Text Label 9250 4600 0    60   ~ 0
SAFE_A1
Text Label 9250 4700 0    60   ~ 0
SAFE_A2
Text Label 9250 4800 0    60   ~ 0
SAFE_A3
Text Label 9250 4900 0    60   ~ 0
SAFE_A4
Text Label 9250 5050 0    60   ~ 0
SAFE_EN
Wire Wire Line
	9250 4600 9650 4600
Wire Wire Line
	9650 4700 9250 4700
Wire Wire Line
	9250 4800 9650 4800
Wire Wire Line
	9250 4900 9650 4900
Wire Wire Line
	9250 5050 9650 5050
$Sheet
S 8550 1000 600  4150
U 5A340375
F0 "sheet5A33FB2C" 60
F1 "mux.sch" 60
F2 "MUX_IN01" B L 8550 1100 60 
F3 "MUX_IN02" B L 8550 1200 60 
F4 "MUX_IN03" B L 8550 1300 60 
F5 "MUX_IN04" B L 8550 1400 60 
F6 "MUX_IN05" B L 8550 1500 60 
F7 "MUX_IN06" B L 8550 1600 60 
F8 "MUX_IN07" B L 8550 1700 60 
F9 "MUX_IN08" B L 8550 1800 60 
F10 "MUX_IN09" B L 8550 1900 60 
F11 "MUX_IN10" B L 8550 2000 60 
F12 "MUX_IN11" B L 8550 2100 60 
F13 "MUX_IN12" B L 8550 2200 60 
F14 "MUX_IN14" B L 8550 2400 60 
F15 "MUX_IN15" B L 8550 2500 60 
F16 "MUX_IN16" B L 8550 2600 60 
F17 "MUX_OUT" B L 8550 4350 60 
F18 "MUX_IN32" B L 8550 4200 60 
F19 "MUX_IN31" B L 8550 4100 60 
F20 "MUX_IN30" B L 8550 4000 60 
F21 "MUX_IN29" B L 8550 3900 60 
F22 "MUX_IN28" B L 8550 3800 60 
F23 "MUX_IN27" B L 8550 3700 60 
F24 "MUX_IN26" B L 8550 3600 60 
F25 "MUX_IN25" B L 8550 3500 60 
F26 "MUX_IN24" B L 8550 3400 60 
F27 "MUX_IN23" B L 8550 3300 60 
F28 "MUX_IN22" B L 8550 3200 60 
F29 "MUX_IN21" B L 8550 3100 60 
F30 "MUX_IN20" B L 8550 3000 60 
F31 "MUX_IN19" B L 8550 2900 60 
F32 "MUX_IN18" B L 8550 2800 60 
F33 "MUX_IN17" B L 8550 2700 60 
F34 "MUX_A0" I L 8550 4500 60 
F35 "MUX_A1" I L 8550 4600 60 
F36 "MUX_A2" I L 8550 4700 60 
F37 "MUX_A3" I L 8550 4800 60 
F38 "MUX_A4" I L 8550 4900 60 
F39 "MUX_EN" I L 8550 5050 60 
F40 "MUX_IN13" B L 8550 2300 60 
$EndSheet
Wire Wire Line
	8550 4500 8150 4500
Text Label 8150 4500 0    60   ~ 0
SAFE_A0
Text Label 8150 4600 0    60   ~ 0
SAFE_A1
Text Label 8150 4700 0    60   ~ 0
SAFE_A2
Text Label 8150 4800 0    60   ~ 0
SAFE_A3
Text Label 8150 4900 0    60   ~ 0
SAFE_A4
Text Label 8150 5050 0    60   ~ 0
SAFE_EN
Wire Wire Line
	8150 4600 8550 4600
Wire Wire Line
	8550 4700 8150 4700
Wire Wire Line
	8150 4800 8550 4800
Wire Wire Line
	8150 4900 8550 4900
Wire Wire Line
	8150 5050 8550 5050
$Sheet
S 7450 1000 600  4150
U 5A34041F
F0 "sheet5A33FB2D" 60
F1 "mux.sch" 60
F2 "MUX_IN01" B L 7450 1100 60 
F3 "MUX_IN02" B L 7450 1200 60 
F4 "MUX_IN03" B L 7450 1300 60 
F5 "MUX_IN04" B L 7450 1400 60 
F6 "MUX_IN05" B L 7450 1500 60 
F7 "MUX_IN06" B L 7450 1600 60 
F8 "MUX_IN07" B L 7450 1700 60 
F9 "MUX_IN08" B L 7450 1800 60 
F10 "MUX_IN09" B L 7450 1900 60 
F11 "MUX_IN10" B L 7450 2000 60 
F12 "MUX_IN11" B L 7450 2100 60 
F13 "MUX_IN12" B L 7450 2200 60 
F14 "MUX_IN14" B L 7450 2400 60 
F15 "MUX_IN15" B L 7450 2500 60 
F16 "MUX_IN16" B L 7450 2600 60 
F17 "MUX_OUT" B L 7450 4350 60 
F18 "MUX_IN32" B L 7450 4200 60 
F19 "MUX_IN31" B L 7450 4100 60 
F20 "MUX_IN30" B L 7450 4000 60 
F21 "MUX_IN29" B L 7450 3900 60 
F22 "MUX_IN28" B L 7450 3800 60 
F23 "MUX_IN27" B L 7450 3700 60 
F24 "MUX_IN26" B L 7450 3600 60 
F25 "MUX_IN25" B L 7450 3500 60 
F26 "MUX_IN24" B L 7450 3400 60 
F27 "MUX_IN23" B L 7450 3300 60 
F28 "MUX_IN22" B L 7450 3200 60 
F29 "MUX_IN21" B L 7450 3100 60 
F30 "MUX_IN20" B L 7450 3000 60 
F31 "MUX_IN19" B L 7450 2900 60 
F32 "MUX_IN18" B L 7450 2800 60 
F33 "MUX_IN17" B L 7450 2700 60 
F34 "MUX_A0" I L 7450 4500 60 
F35 "MUX_A1" I L 7450 4600 60 
F36 "MUX_A2" I L 7450 4700 60 
F37 "MUX_A3" I L 7450 4800 60 
F38 "MUX_A4" I L 7450 4900 60 
F39 "MUX_EN" I L 7450 5050 60 
F40 "MUX_IN13" B L 7450 2300 60 
$EndSheet
Wire Wire Line
	7450 4500 7050 4500
Text Label 7050 4500 0    60   ~ 0
SAFE_A0
Text Label 7050 4600 0    60   ~ 0
SAFE_A1
Text Label 7050 4700 0    60   ~ 0
SAFE_A2
Text Label 7050 4800 0    60   ~ 0
SAFE_A3
Text Label 7050 4900 0    60   ~ 0
SAFE_A4
Text Label 7050 5050 0    60   ~ 0
SAFE_EN
Wire Wire Line
	7050 4600 7450 4600
Wire Wire Line
	7450 4700 7050 4700
Wire Wire Line
	7050 4800 7450 4800
Wire Wire Line
	7050 4900 7450 4900
Wire Wire Line
	7050 5050 7450 5050
$Sheet
S 6350 1000 600  4150
U 5A3404C9
F0 "sheet5A33FB2E" 60
F1 "mux.sch" 60
F2 "MUX_IN01" B L 6350 1100 60 
F3 "MUX_IN02" B L 6350 1200 60 
F4 "MUX_IN03" B L 6350 1300 60 
F5 "MUX_IN04" B L 6350 1400 60 
F6 "MUX_IN05" B L 6350 1500 60 
F7 "MUX_IN06" B L 6350 1600 60 
F8 "MUX_IN07" B L 6350 1700 60 
F9 "MUX_IN08" B L 6350 1800 60 
F10 "MUX_IN09" B L 6350 1900 60 
F11 "MUX_IN10" B L 6350 2000 60 
F12 "MUX_IN11" B L 6350 2100 60 
F13 "MUX_IN12" B L 6350 2200 60 
F14 "MUX_IN14" B L 6350 2400 60 
F15 "MUX_IN15" B L 6350 2500 60 
F16 "MUX_IN16" B L 6350 2600 60 
F17 "MUX_OUT" B L 6350 4350 60 
F18 "MUX_IN32" B L 6350 4200 60 
F19 "MUX_IN31" B L 6350 4100 60 
F20 "MUX_IN30" B L 6350 4000 60 
F21 "MUX_IN29" B L 6350 3900 60 
F22 "MUX_IN28" B L 6350 3800 60 
F23 "MUX_IN27" B L 6350 3700 60 
F24 "MUX_IN26" B L 6350 3600 60 
F25 "MUX_IN25" B L 6350 3500 60 
F26 "MUX_IN24" B L 6350 3400 60 
F27 "MUX_IN23" B L 6350 3300 60 
F28 "MUX_IN22" B L 6350 3200 60 
F29 "MUX_IN21" B L 6350 3100 60 
F30 "MUX_IN20" B L 6350 3000 60 
F31 "MUX_IN19" B L 6350 2900 60 
F32 "MUX_IN18" B L 6350 2800 60 
F33 "MUX_IN17" B L 6350 2700 60 
F34 "MUX_A0" I L 6350 4500 60 
F35 "MUX_A1" I L 6350 4600 60 
F36 "MUX_A2" I L 6350 4700 60 
F37 "MUX_A3" I L 6350 4800 60 
F38 "MUX_A4" I L 6350 4900 60 
F39 "MUX_EN" I L 6350 5050 60 
F40 "MUX_IN13" B L 6350 2300 60 
$EndSheet
Wire Wire Line
	6350 4500 5950 4500
Text Label 5950 4500 0    60   ~ 0
SAFE_A0
Text Label 5950 4600 0    60   ~ 0
SAFE_A1
Text Label 5950 4700 0    60   ~ 0
SAFE_A2
Text Label 5950 4800 0    60   ~ 0
SAFE_A3
Text Label 5950 4900 0    60   ~ 0
SAFE_A4
Text Label 5950 5050 0    60   ~ 0
SAFE_EN
Wire Wire Line
	5950 4600 6350 4600
Wire Wire Line
	6350 4700 5950 4700
Wire Wire Line
	5950 4800 6350 4800
Wire Wire Line
	5950 4900 6350 4900
Wire Wire Line
	5950 5050 6350 5050
$Sheet
S 5250 1000 600  4150
U 5A340573
F0 "sheet5A33FB2F" 60
F1 "mux.sch" 60
F2 "MUX_IN01" B L 5250 1100 60 
F3 "MUX_IN02" B L 5250 1200 60 
F4 "MUX_IN03" B L 5250 1300 60 
F5 "MUX_IN04" B L 5250 1400 60 
F6 "MUX_IN05" B L 5250 1500 60 
F7 "MUX_IN06" B L 5250 1600 60 
F8 "MUX_IN07" B L 5250 1700 60 
F9 "MUX_IN08" B L 5250 1800 60 
F10 "MUX_IN09" B L 5250 1900 60 
F11 "MUX_IN10" B L 5250 2000 60 
F12 "MUX_IN11" B L 5250 2100 60 
F13 "MUX_IN12" B L 5250 2200 60 
F14 "MUX_IN14" B L 5250 2400 60 
F15 "MUX_IN15" B L 5250 2500 60 
F16 "MUX_IN16" B L 5250 2600 60 
F17 "MUX_OUT" B L 5250 4350 60 
F18 "MUX_IN32" B L 5250 4200 60 
F19 "MUX_IN31" B L 5250 4100 60 
F20 "MUX_IN30" B L 5250 4000 60 
F21 "MUX_IN29" B L 5250 3900 60 
F22 "MUX_IN28" B L 5250 3800 60 
F23 "MUX_IN27" B L 5250 3700 60 
F24 "MUX_IN26" B L 5250 3600 60 
F25 "MUX_IN25" B L 5250 3500 60 
F26 "MUX_IN24" B L 5250 3400 60 
F27 "MUX_IN23" B L 5250 3300 60 
F28 "MUX_IN22" B L 5250 3200 60 
F29 "MUX_IN21" B L 5250 3100 60 
F30 "MUX_IN20" B L 5250 3000 60 
F31 "MUX_IN19" B L 5250 2900 60 
F32 "MUX_IN18" B L 5250 2800 60 
F33 "MUX_IN17" B L 5250 2700 60 
F34 "MUX_A0" I L 5250 4500 60 
F35 "MUX_A1" I L 5250 4600 60 
F36 "MUX_A2" I L 5250 4700 60 
F37 "MUX_A3" I L 5250 4800 60 
F38 "MUX_A4" I L 5250 4900 60 
F39 "MUX_EN" I L 5250 5050 60 
F40 "MUX_IN13" B L 5250 2300 60 
$EndSheet
Wire Wire Line
	5250 4500 4850 4500
Text Label 4850 4500 0    60   ~ 0
SAFE_A0
Text Label 4850 4600 0    60   ~ 0
SAFE_A1
Text Label 4850 4700 0    60   ~ 0
SAFE_A2
Text Label 4850 4800 0    60   ~ 0
SAFE_A3
Text Label 4850 4900 0    60   ~ 0
SAFE_A4
Text Label 4850 5050 0    60   ~ 0
SAFE_EN
Wire Wire Line
	4850 4600 5250 4600
Wire Wire Line
	5250 4700 4850 4700
Wire Wire Line
	4850 4800 5250 4800
Wire Wire Line
	4850 4900 5250 4900
Wire Wire Line
	4850 5050 5250 5050
$Sheet
S 4150 1000 600  4150
U 5A34061D
F0 "sheet5A33FB30" 60
F1 "mux.sch" 60
F2 "MUX_IN01" B L 4150 1100 60 
F3 "MUX_IN02" B L 4150 1200 60 
F4 "MUX_IN03" B L 4150 1300 60 
F5 "MUX_IN04" B L 4150 1400 60 
F6 "MUX_IN05" B L 4150 1500 60 
F7 "MUX_IN06" B L 4150 1600 60 
F8 "MUX_IN07" B L 4150 1700 60 
F9 "MUX_IN08" B L 4150 1800 60 
F10 "MUX_IN09" B L 4150 1900 60 
F11 "MUX_IN10" B L 4150 2000 60 
F12 "MUX_IN11" B L 4150 2100 60 
F13 "MUX_IN12" B L 4150 2200 60 
F14 "MUX_IN14" B L 4150 2400 60 
F15 "MUX_IN15" B L 4150 2500 60 
F16 "MUX_IN16" B L 4150 2600 60 
F17 "MUX_OUT" B L 4150 4350 60 
F18 "MUX_IN32" B L 4150 4200 60 
F19 "MUX_IN31" B L 4150 4100 60 
F20 "MUX_IN30" B L 4150 4000 60 
F21 "MUX_IN29" B L 4150 3900 60 
F22 "MUX_IN28" B L 4150 3800 60 
F23 "MUX_IN27" B L 4150 3700 60 
F24 "MUX_IN26" B L 4150 3600 60 
F25 "MUX_IN25" B L 4150 3500 60 
F26 "MUX_IN24" B L 4150 3400 60 
F27 "MUX_IN23" B L 4150 3300 60 
F28 "MUX_IN22" B L 4150 3200 60 
F29 "MUX_IN21" B L 4150 3100 60 
F30 "MUX_IN20" B L 4150 3000 60 
F31 "MUX_IN19" B L 4150 2900 60 
F32 "MUX_IN18" B L 4150 2800 60 
F33 "MUX_IN17" B L 4150 2700 60 
F34 "MUX_A0" I L 4150 4500 60 
F35 "MUX_A1" I L 4150 4600 60 
F36 "MUX_A2" I L 4150 4700 60 
F37 "MUX_A3" I L 4150 4800 60 
F38 "MUX_A4" I L 4150 4900 60 
F39 "MUX_EN" I L 4150 5050 60 
F40 "MUX_IN13" B L 4150 2300 60 
$EndSheet
Wire Wire Line
	4150 4500 3750 4500
Text Label 3750 4500 0    60   ~ 0
SAFE_A0
Text Label 3750 4600 0    60   ~ 0
SAFE_A1
Text Label 3750 4700 0    60   ~ 0
SAFE_A2
Text Label 3750 4800 0    60   ~ 0
SAFE_A3
Text Label 3750 4900 0    60   ~ 0
SAFE_A4
Text Label 3750 5050 0    60   ~ 0
SAFE_EN
Wire Wire Line
	3750 4600 4150 4600
Wire Wire Line
	4150 4700 3750 4700
Wire Wire Line
	3750 4800 4150 4800
Wire Wire Line
	3750 4900 4150 4900
Wire Wire Line
	3750 5050 4150 5050
$Sheet
S 3050 1000 600  4150
U 5A3406C7
F0 "sheet5A33FB31" 60
F1 "mux.sch" 60
F2 "MUX_IN01" B L 3050 1100 60 
F3 "MUX_IN02" B L 3050 1200 60 
F4 "MUX_IN03" B L 3050 1300 60 
F5 "MUX_IN04" B L 3050 1400 60 
F6 "MUX_IN05" B L 3050 1500 60 
F7 "MUX_IN06" B L 3050 1600 60 
F8 "MUX_IN07" B L 3050 1700 60 
F9 "MUX_IN08" B L 3050 1800 60 
F10 "MUX_IN09" B L 3050 1900 60 
F11 "MUX_IN10" B L 3050 2000 60 
F12 "MUX_IN11" B L 3050 2100 60 
F13 "MUX_IN12" B L 3050 2200 60 
F14 "MUX_IN14" B L 3050 2400 60 
F15 "MUX_IN15" B L 3050 2500 60 
F16 "MUX_IN16" B L 3050 2600 60 
F17 "MUX_OUT" B L 3050 4350 60 
F18 "MUX_IN32" B L 3050 4200 60 
F19 "MUX_IN31" B L 3050 4100 60 
F20 "MUX_IN30" B L 3050 4000 60 
F21 "MUX_IN29" B L 3050 3900 60 
F22 "MUX_IN28" B L 3050 3800 60 
F23 "MUX_IN27" B L 3050 3700 60 
F24 "MUX_IN26" B L 3050 3600 60 
F25 "MUX_IN25" B L 3050 3500 60 
F26 "MUX_IN24" B L 3050 3400 60 
F27 "MUX_IN23" B L 3050 3300 60 
F28 "MUX_IN22" B L 3050 3200 60 
F29 "MUX_IN21" B L 3050 3100 60 
F30 "MUX_IN20" B L 3050 3000 60 
F31 "MUX_IN19" B L 3050 2900 60 
F32 "MUX_IN18" B L 3050 2800 60 
F33 "MUX_IN17" B L 3050 2700 60 
F34 "MUX_A0" I L 3050 4500 60 
F35 "MUX_A1" I L 3050 4600 60 
F36 "MUX_A2" I L 3050 4700 60 
F37 "MUX_A3" I L 3050 4800 60 
F38 "MUX_A4" I L 3050 4900 60 
F39 "MUX_EN" I L 3050 5050 60 
F40 "MUX_IN13" B L 3050 2300 60 
$EndSheet
Wire Wire Line
	3050 4500 2650 4500
Text Label 2650 4500 0    60   ~ 0
SAFE_A0
Text Label 2650 4600 0    60   ~ 0
SAFE_A1
Text Label 2650 4700 0    60   ~ 0
SAFE_A2
Text Label 2650 4800 0    60   ~ 0
SAFE_A3
Text Label 2650 4900 0    60   ~ 0
SAFE_A4
Text Label 2650 5050 0    60   ~ 0
SAFE_EN
Wire Wire Line
	2650 4600 3050 4600
Wire Wire Line
	3050 4700 2650 4700
Wire Wire Line
	2650 4800 3050 4800
Wire Wire Line
	2650 4900 3050 4900
Wire Wire Line
	2650 5050 3050 5050
$Sheet
S 1950 1000 600  4150
U 5A340771
F0 "sheet5A33FB32" 60
F1 "mux.sch" 60
F2 "MUX_IN01" B L 1950 1100 60 
F3 "MUX_IN02" B L 1950 1200 60 
F4 "MUX_IN03" B L 1950 1300 60 
F5 "MUX_IN04" B L 1950 1400 60 
F6 "MUX_IN05" B L 1950 1500 60 
F7 "MUX_IN06" B L 1950 1600 60 
F8 "MUX_IN07" B L 1950 1700 60 
F9 "MUX_IN08" B L 1950 1800 60 
F10 "MUX_IN09" B L 1950 1900 60 
F11 "MUX_IN10" B L 1950 2000 60 
F12 "MUX_IN11" B L 1950 2100 60 
F13 "MUX_IN12" B L 1950 2200 60 
F14 "MUX_IN14" B L 1950 2400 60 
F15 "MUX_IN15" B L 1950 2500 60 
F16 "MUX_IN16" B L 1950 2600 60 
F17 "MUX_OUT" B L 1950 4350 60 
F18 "MUX_IN32" B L 1950 4200 60 
F19 "MUX_IN31" B L 1950 4100 60 
F20 "MUX_IN30" B L 1950 4000 60 
F21 "MUX_IN29" B L 1950 3900 60 
F22 "MUX_IN28" B L 1950 3800 60 
F23 "MUX_IN27" B L 1950 3700 60 
F24 "MUX_IN26" B L 1950 3600 60 
F25 "MUX_IN25" B L 1950 3500 60 
F26 "MUX_IN24" B L 1950 3400 60 
F27 "MUX_IN23" B L 1950 3300 60 
F28 "MUX_IN22" B L 1950 3200 60 
F29 "MUX_IN21" B L 1950 3100 60 
F30 "MUX_IN20" B L 1950 3000 60 
F31 "MUX_IN19" B L 1950 2900 60 
F32 "MUX_IN18" B L 1950 2800 60 
F33 "MUX_IN17" B L 1950 2700 60 
F34 "MUX_A0" I L 1950 4500 60 
F35 "MUX_A1" I L 1950 4600 60 
F36 "MUX_A2" I L 1950 4700 60 
F37 "MUX_A3" I L 1950 4800 60 
F38 "MUX_A4" I L 1950 4900 60 
F39 "MUX_EN" I L 1950 5050 60 
F40 "MUX_IN13" B L 1950 2300 60 
$EndSheet
Wire Wire Line
	1950 4500 1550 4500
Text Label 1550 4500 0    60   ~ 0
SAFE_A0
Text Label 1550 4600 0    60   ~ 0
SAFE_A1
Text Label 1550 4700 0    60   ~ 0
SAFE_A2
Text Label 1550 4800 0    60   ~ 0
SAFE_A3
Text Label 1550 4900 0    60   ~ 0
SAFE_A4
Text Label 1550 5050 0    60   ~ 0
SAFE_EN
Wire Wire Line
	1550 4600 1950 4600
Wire Wire Line
	1950 4700 1550 4700
Wire Wire Line
	1550 4800 1950 4800
Wire Wire Line
	1550 4900 1950 4900
Wire Wire Line
	1550 5050 1950 5050
Wire Wire Line
	2000 9050 1600 9050
Text Label 1650 9050 0    60   ~ 0
OUT16
Wire Wire Line
	3100 9050 2700 9050
Text Label 2750 9050 0    60   ~ 0
OUT17
Wire Wire Line
	4200 9050 3800 9050
Text Label 3850 9050 0    60   ~ 0
OUT18
Wire Wire Line
	5300 9050 4900 9050
Text Label 4950 9050 0    60   ~ 0
OUT19
Wire Wire Line
	6400 9050 6000 9050
Text Label 6050 9050 0    60   ~ 0
OUT20
Wire Wire Line
	7500 9050 7100 9050
Text Label 7150 9050 0    60   ~ 0
OUT21
Wire Wire Line
	8600 9050 8200 9050
Text Label 8250 9050 0    60   ~ 0
OUT22
Wire Wire Line
	9700 9050 9300 9050
Text Label 9350 9050 0    60   ~ 0
OUT23
Wire Wire Line
	10800 9050 10400 9050
Text Label 10450 9050 0    60   ~ 0
OUT24
Wire Wire Line
	11900 9050 11500 9050
Text Label 11550 9050 0    60   ~ 0
OUT25
Wire Wire Line
	13000 9050 12600 9050
Text Label 12650 9050 0    60   ~ 0
OUT26
Wire Wire Line
	14100 9050 13700 9050
Text Label 13750 9050 0    60   ~ 0
OUT27
Wire Wire Line
	14100 9200 13700 9200
Text Label 13700 9200 0    60   ~ 0
SAFE_A0
Text Label 13700 9300 0    60   ~ 0
SAFE_A1
Text Label 13700 9400 0    60   ~ 0
SAFE_A2
Text Label 13700 9500 0    60   ~ 0
SAFE_A3
Text Label 13700 9600 0    60   ~ 0
SAFE_A4
Text Label 13700 9750 0    60   ~ 0
SAFE_EN
Wire Wire Line
	13700 9300 14100 9300
Wire Wire Line
	14100 9400 13700 9400
Wire Wire Line
	13700 9500 14100 9500
Wire Wire Line
	13700 9600 14100 9600
Wire Wire Line
	13700 9750 14100 9750
$Sheet
S 13000 5700 600  4150
U 5A352561
F0 "sheet5A351FBC" 60
F1 "mux.sch" 60
F2 "MUX_IN01" B L 13000 5800 60 
F3 "MUX_IN02" B L 13000 5900 60 
F4 "MUX_IN03" B L 13000 6000 60 
F5 "MUX_IN04" B L 13000 6100 60 
F6 "MUX_IN05" B L 13000 6200 60 
F7 "MUX_IN06" B L 13000 6300 60 
F8 "MUX_IN07" B L 13000 6400 60 
F9 "MUX_IN08" B L 13000 6500 60 
F10 "MUX_IN09" B L 13000 6600 60 
F11 "MUX_IN10" B L 13000 6700 60 
F12 "MUX_IN11" B L 13000 6800 60 
F13 "MUX_IN12" B L 13000 6900 60 
F14 "MUX_IN14" B L 13000 7100 60 
F15 "MUX_IN15" B L 13000 7200 60 
F16 "MUX_IN16" B L 13000 7300 60 
F17 "MUX_OUT" B L 13000 9050 60 
F18 "MUX_IN32" B L 13000 8900 60 
F19 "MUX_IN31" B L 13000 8800 60 
F20 "MUX_IN30" B L 13000 8700 60 
F21 "MUX_IN29" B L 13000 8600 60 
F22 "MUX_IN28" B L 13000 8500 60 
F23 "MUX_IN27" B L 13000 8400 60 
F24 "MUX_IN26" B L 13000 8300 60 
F25 "MUX_IN25" B L 13000 8200 60 
F26 "MUX_IN24" B L 13000 8100 60 
F27 "MUX_IN23" B L 13000 8000 60 
F28 "MUX_IN22" B L 13000 7900 60 
F29 "MUX_IN21" B L 13000 7800 60 
F30 "MUX_IN20" B L 13000 7700 60 
F31 "MUX_IN19" B L 13000 7600 60 
F32 "MUX_IN18" B L 13000 7500 60 
F33 "MUX_IN17" B L 13000 7400 60 
F34 "MUX_A0" I L 13000 9200 60 
F35 "MUX_A1" I L 13000 9300 60 
F36 "MUX_A2" I L 13000 9400 60 
F37 "MUX_A3" I L 13000 9500 60 
F38 "MUX_A4" I L 13000 9600 60 
F39 "MUX_EN" I L 13000 9750 60 
F40 "MUX_IN13" B L 13000 7000 60 
$EndSheet
Wire Wire Line
	13000 9200 12600 9200
Text Label 12600 9200 0    60   ~ 0
SAFE_A0
Text Label 12600 9300 0    60   ~ 0
SAFE_A1
Text Label 12600 9400 0    60   ~ 0
SAFE_A2
Text Label 12600 9500 0    60   ~ 0
SAFE_A3
Text Label 12600 9600 0    60   ~ 0
SAFE_A4
Text Label 12600 9750 0    60   ~ 0
SAFE_EN
Wire Wire Line
	12600 9300 13000 9300
Wire Wire Line
	13000 9400 12600 9400
Wire Wire Line
	12600 9500 13000 9500
Wire Wire Line
	12600 9600 13000 9600
Wire Wire Line
	12600 9750 13000 9750
$Sheet
S 11900 5700 600  4150
U 5A35260B
F0 "sheet5A351FBD" 60
F1 "mux.sch" 60
F2 "MUX_IN01" B L 11900 5800 60 
F3 "MUX_IN02" B L 11900 5900 60 
F4 "MUX_IN03" B L 11900 6000 60 
F5 "MUX_IN04" B L 11900 6100 60 
F6 "MUX_IN05" B L 11900 6200 60 
F7 "MUX_IN06" B L 11900 6300 60 
F8 "MUX_IN07" B L 11900 6400 60 
F9 "MUX_IN08" B L 11900 6500 60 
F10 "MUX_IN09" B L 11900 6600 60 
F11 "MUX_IN10" B L 11900 6700 60 
F12 "MUX_IN11" B L 11900 6800 60 
F13 "MUX_IN12" B L 11900 6900 60 
F14 "MUX_IN14" B L 11900 7100 60 
F15 "MUX_IN15" B L 11900 7200 60 
F16 "MUX_IN16" B L 11900 7300 60 
F17 "MUX_OUT" B L 11900 9050 60 
F18 "MUX_IN32" B L 11900 8900 60 
F19 "MUX_IN31" B L 11900 8800 60 
F20 "MUX_IN30" B L 11900 8700 60 
F21 "MUX_IN29" B L 11900 8600 60 
F22 "MUX_IN28" B L 11900 8500 60 
F23 "MUX_IN27" B L 11900 8400 60 
F24 "MUX_IN26" B L 11900 8300 60 
F25 "MUX_IN25" B L 11900 8200 60 
F26 "MUX_IN24" B L 11900 8100 60 
F27 "MUX_IN23" B L 11900 8000 60 
F28 "MUX_IN22" B L 11900 7900 60 
F29 "MUX_IN21" B L 11900 7800 60 
F30 "MUX_IN20" B L 11900 7700 60 
F31 "MUX_IN19" B L 11900 7600 60 
F32 "MUX_IN18" B L 11900 7500 60 
F33 "MUX_IN17" B L 11900 7400 60 
F34 "MUX_A0" I L 11900 9200 60 
F35 "MUX_A1" I L 11900 9300 60 
F36 "MUX_A2" I L 11900 9400 60 
F37 "MUX_A3" I L 11900 9500 60 
F38 "MUX_A4" I L 11900 9600 60 
F39 "MUX_EN" I L 11900 9750 60 
F40 "MUX_IN13" B L 11900 7000 60 
$EndSheet
Wire Wire Line
	11900 9200 11500 9200
Text Label 11500 9200 0    60   ~ 0
SAFE_A0
Text Label 11500 9300 0    60   ~ 0
SAFE_A1
Text Label 11500 9400 0    60   ~ 0
SAFE_A2
Text Label 11500 9500 0    60   ~ 0
SAFE_A3
Text Label 11500 9600 0    60   ~ 0
SAFE_A4
Text Label 11500 9750 0    60   ~ 0
SAFE_EN
Wire Wire Line
	11500 9300 11900 9300
Wire Wire Line
	11900 9400 11500 9400
Wire Wire Line
	11500 9500 11900 9500
Wire Wire Line
	11500 9600 11900 9600
Wire Wire Line
	11500 9750 11900 9750
$Sheet
S 10800 5700 600  4150
U 5A3526B5
F0 "sheet5A351FBE" 60
F1 "mux.sch" 60
F2 "MUX_IN01" B L 10800 5800 60 
F3 "MUX_IN02" B L 10800 5900 60 
F4 "MUX_IN03" B L 10800 6000 60 
F5 "MUX_IN04" B L 10800 6100 60 
F6 "MUX_IN05" B L 10800 6200 60 
F7 "MUX_IN06" B L 10800 6300 60 
F8 "MUX_IN07" B L 10800 6400 60 
F9 "MUX_IN08" B L 10800 6500 60 
F10 "MUX_IN09" B L 10800 6600 60 
F11 "MUX_IN10" B L 10800 6700 60 
F12 "MUX_IN11" B L 10800 6800 60 
F13 "MUX_IN12" B L 10800 6900 60 
F14 "MUX_IN14" B L 10800 7100 60 
F15 "MUX_IN15" B L 10800 7200 60 
F16 "MUX_IN16" B L 10800 7300 60 
F17 "MUX_OUT" B L 10800 9050 60 
F18 "MUX_IN32" B L 10800 8900 60 
F19 "MUX_IN31" B L 10800 8800 60 
F20 "MUX_IN30" B L 10800 8700 60 
F21 "MUX_IN29" B L 10800 8600 60 
F22 "MUX_IN28" B L 10800 8500 60 
F23 "MUX_IN27" B L 10800 8400 60 
F24 "MUX_IN26" B L 10800 8300 60 
F25 "MUX_IN25" B L 10800 8200 60 
F26 "MUX_IN24" B L 10800 8100 60 
F27 "MUX_IN23" B L 10800 8000 60 
F28 "MUX_IN22" B L 10800 7900 60 
F29 "MUX_IN21" B L 10800 7800 60 
F30 "MUX_IN20" B L 10800 7700 60 
F31 "MUX_IN19" B L 10800 7600 60 
F32 "MUX_IN18" B L 10800 7500 60 
F33 "MUX_IN17" B L 10800 7400 60 
F34 "MUX_A0" I L 10800 9200 60 
F35 "MUX_A1" I L 10800 9300 60 
F36 "MUX_A2" I L 10800 9400 60 
F37 "MUX_A3" I L 10800 9500 60 
F38 "MUX_A4" I L 10800 9600 60 
F39 "MUX_EN" I L 10800 9750 60 
F40 "MUX_IN13" B L 10800 7000 60 
$EndSheet
Wire Wire Line
	10800 9200 10400 9200
Text Label 10400 9200 0    60   ~ 0
SAFE_A0
Text Label 10400 9300 0    60   ~ 0
SAFE_A1
Text Label 10400 9400 0    60   ~ 0
SAFE_A2
Text Label 10400 9500 0    60   ~ 0
SAFE_A3
Text Label 10400 9600 0    60   ~ 0
SAFE_A4
Text Label 10400 9750 0    60   ~ 0
SAFE_EN
Wire Wire Line
	10400 9300 10800 9300
Wire Wire Line
	10800 9400 10400 9400
Wire Wire Line
	10400 9500 10800 9500
Wire Wire Line
	10400 9600 10800 9600
Wire Wire Line
	10400 9750 10800 9750
$Sheet
S 9700 5700 600  4150
U 5A35275F
F0 "sheet5A351FBF" 60
F1 "mux.sch" 60
F2 "MUX_IN01" B L 9700 5800 60 
F3 "MUX_IN02" B L 9700 5900 60 
F4 "MUX_IN03" B L 9700 6000 60 
F5 "MUX_IN04" B L 9700 6100 60 
F6 "MUX_IN05" B L 9700 6200 60 
F7 "MUX_IN06" B L 9700 6300 60 
F8 "MUX_IN07" B L 9700 6400 60 
F9 "MUX_IN08" B L 9700 6500 60 
F10 "MUX_IN09" B L 9700 6600 60 
F11 "MUX_IN10" B L 9700 6700 60 
F12 "MUX_IN11" B L 9700 6800 60 
F13 "MUX_IN12" B L 9700 6900 60 
F14 "MUX_IN14" B L 9700 7100 60 
F15 "MUX_IN15" B L 9700 7200 60 
F16 "MUX_IN16" B L 9700 7300 60 
F17 "MUX_OUT" B L 9700 9050 60 
F18 "MUX_IN32" B L 9700 8900 60 
F19 "MUX_IN31" B L 9700 8800 60 
F20 "MUX_IN30" B L 9700 8700 60 
F21 "MUX_IN29" B L 9700 8600 60 
F22 "MUX_IN28" B L 9700 8500 60 
F23 "MUX_IN27" B L 9700 8400 60 
F24 "MUX_IN26" B L 9700 8300 60 
F25 "MUX_IN25" B L 9700 8200 60 
F26 "MUX_IN24" B L 9700 8100 60 
F27 "MUX_IN23" B L 9700 8000 60 
F28 "MUX_IN22" B L 9700 7900 60 
F29 "MUX_IN21" B L 9700 7800 60 
F30 "MUX_IN20" B L 9700 7700 60 
F31 "MUX_IN19" B L 9700 7600 60 
F32 "MUX_IN18" B L 9700 7500 60 
F33 "MUX_IN17" B L 9700 7400 60 
F34 "MUX_A0" I L 9700 9200 60 
F35 "MUX_A1" I L 9700 9300 60 
F36 "MUX_A2" I L 9700 9400 60 
F37 "MUX_A3" I L 9700 9500 60 
F38 "MUX_A4" I L 9700 9600 60 
F39 "MUX_EN" I L 9700 9750 60 
F40 "MUX_IN13" B L 9700 7000 60 
$EndSheet
Wire Wire Line
	9700 9200 9300 9200
Text Label 9300 9200 0    60   ~ 0
SAFE_A0
Text Label 9300 9300 0    60   ~ 0
SAFE_A1
Text Label 9300 9400 0    60   ~ 0
SAFE_A2
Text Label 9300 9500 0    60   ~ 0
SAFE_A3
Text Label 9300 9600 0    60   ~ 0
SAFE_A4
Text Label 9300 9750 0    60   ~ 0
SAFE_EN
Wire Wire Line
	9300 9300 9700 9300
Wire Wire Line
	9700 9400 9300 9400
Wire Wire Line
	9300 9500 9700 9500
Wire Wire Line
	9300 9600 9700 9600
Wire Wire Line
	9300 9750 9700 9750
$Sheet
S 8600 5700 600  4150
U 5A352809
F0 "sheet5A351FC0" 60
F1 "mux.sch" 60
F2 "MUX_IN01" B L 8600 5800 60 
F3 "MUX_IN02" B L 8600 5900 60 
F4 "MUX_IN03" B L 8600 6000 60 
F5 "MUX_IN04" B L 8600 6100 60 
F6 "MUX_IN05" B L 8600 6200 60 
F7 "MUX_IN06" B L 8600 6300 60 
F8 "MUX_IN07" B L 8600 6400 60 
F9 "MUX_IN08" B L 8600 6500 60 
F10 "MUX_IN09" B L 8600 6600 60 
F11 "MUX_IN10" B L 8600 6700 60 
F12 "MUX_IN11" B L 8600 6800 60 
F13 "MUX_IN12" B L 8600 6900 60 
F14 "MUX_IN14" B L 8600 7100 60 
F15 "MUX_IN15" B L 8600 7200 60 
F16 "MUX_IN16" B L 8600 7300 60 
F17 "MUX_OUT" B L 8600 9050 60 
F18 "MUX_IN32" B L 8600 8900 60 
F19 "MUX_IN31" B L 8600 8800 60 
F20 "MUX_IN30" B L 8600 8700 60 
F21 "MUX_IN29" B L 8600 8600 60 
F22 "MUX_IN28" B L 8600 8500 60 
F23 "MUX_IN27" B L 8600 8400 60 
F24 "MUX_IN26" B L 8600 8300 60 
F25 "MUX_IN25" B L 8600 8200 60 
F26 "MUX_IN24" B L 8600 8100 60 
F27 "MUX_IN23" B L 8600 8000 60 
F28 "MUX_IN22" B L 8600 7900 60 
F29 "MUX_IN21" B L 8600 7800 60 
F30 "MUX_IN20" B L 8600 7700 60 
F31 "MUX_IN19" B L 8600 7600 60 
F32 "MUX_IN18" B L 8600 7500 60 
F33 "MUX_IN17" B L 8600 7400 60 
F34 "MUX_A0" I L 8600 9200 60 
F35 "MUX_A1" I L 8600 9300 60 
F36 "MUX_A2" I L 8600 9400 60 
F37 "MUX_A3" I L 8600 9500 60 
F38 "MUX_A4" I L 8600 9600 60 
F39 "MUX_EN" I L 8600 9750 60 
F40 "MUX_IN13" B L 8600 7000 60 
$EndSheet
Wire Wire Line
	8600 9200 8200 9200
Text Label 8200 9200 0    60   ~ 0
SAFE_A0
Text Label 8200 9300 0    60   ~ 0
SAFE_A1
Text Label 8200 9400 0    60   ~ 0
SAFE_A2
Text Label 8200 9500 0    60   ~ 0
SAFE_A3
Text Label 8200 9600 0    60   ~ 0
SAFE_A4
Text Label 8200 9750 0    60   ~ 0
SAFE_EN
Wire Wire Line
	8200 9300 8600 9300
Wire Wire Line
	8600 9400 8200 9400
Wire Wire Line
	8200 9500 8600 9500
Wire Wire Line
	8200 9600 8600 9600
Wire Wire Line
	8200 9750 8600 9750
$Sheet
S 7500 5700 600  4150
U 5A3528B3
F0 "sheet5A351FC1" 60
F1 "mux.sch" 60
F2 "MUX_IN01" B L 7500 5800 60 
F3 "MUX_IN02" B L 7500 5900 60 
F4 "MUX_IN03" B L 7500 6000 60 
F5 "MUX_IN04" B L 7500 6100 60 
F6 "MUX_IN05" B L 7500 6200 60 
F7 "MUX_IN06" B L 7500 6300 60 
F8 "MUX_IN07" B L 7500 6400 60 
F9 "MUX_IN08" B L 7500 6500 60 
F10 "MUX_IN09" B L 7500 6600 60 
F11 "MUX_IN10" B L 7500 6700 60 
F12 "MUX_IN11" B L 7500 6800 60 
F13 "MUX_IN12" B L 7500 6900 60 
F14 "MUX_IN14" B L 7500 7100 60 
F15 "MUX_IN15" B L 7500 7200 60 
F16 "MUX_IN16" B L 7500 7300 60 
F17 "MUX_OUT" B L 7500 9050 60 
F18 "MUX_IN32" B L 7500 8900 60 
F19 "MUX_IN31" B L 7500 8800 60 
F20 "MUX_IN30" B L 7500 8700 60 
F21 "MUX_IN29" B L 7500 8600 60 
F22 "MUX_IN28" B L 7500 8500 60 
F23 "MUX_IN27" B L 7500 8400 60 
F24 "MUX_IN26" B L 7500 8300 60 
F25 "MUX_IN25" B L 7500 8200 60 
F26 "MUX_IN24" B L 7500 8100 60 
F27 "MUX_IN23" B L 7500 8000 60 
F28 "MUX_IN22" B L 7500 7900 60 
F29 "MUX_IN21" B L 7500 7800 60 
F30 "MUX_IN20" B L 7500 7700 60 
F31 "MUX_IN19" B L 7500 7600 60 
F32 "MUX_IN18" B L 7500 7500 60 
F33 "MUX_IN17" B L 7500 7400 60 
F34 "MUX_A0" I L 7500 9200 60 
F35 "MUX_A1" I L 7500 9300 60 
F36 "MUX_A2" I L 7500 9400 60 
F37 "MUX_A3" I L 7500 9500 60 
F38 "MUX_A4" I L 7500 9600 60 
F39 "MUX_EN" I L 7500 9750 60 
F40 "MUX_IN13" B L 7500 7000 60 
$EndSheet
Wire Wire Line
	7500 9200 7100 9200
Text Label 7100 9200 0    60   ~ 0
SAFE_A0
Text Label 7100 9300 0    60   ~ 0
SAFE_A1
Text Label 7100 9400 0    60   ~ 0
SAFE_A2
Text Label 7100 9500 0    60   ~ 0
SAFE_A3
Text Label 7100 9600 0    60   ~ 0
SAFE_A4
Text Label 7100 9750 0    60   ~ 0
SAFE_EN
Wire Wire Line
	7100 9300 7500 9300
Wire Wire Line
	7500 9400 7100 9400
Wire Wire Line
	7100 9500 7500 9500
Wire Wire Line
	7100 9600 7500 9600
Wire Wire Line
	7100 9750 7500 9750
$Sheet
S 6400 5700 600  4150
U 5A35295D
F0 "sheet5A351FC2" 60
F1 "mux.sch" 60
F2 "MUX_IN01" B L 6400 5800 60 
F3 "MUX_IN02" B L 6400 5900 60 
F4 "MUX_IN03" B L 6400 6000 60 
F5 "MUX_IN04" B L 6400 6100 60 
F6 "MUX_IN05" B L 6400 6200 60 
F7 "MUX_IN06" B L 6400 6300 60 
F8 "MUX_IN07" B L 6400 6400 60 
F9 "MUX_IN08" B L 6400 6500 60 
F10 "MUX_IN09" B L 6400 6600 60 
F11 "MUX_IN10" B L 6400 6700 60 
F12 "MUX_IN11" B L 6400 6800 60 
F13 "MUX_IN12" B L 6400 6900 60 
F14 "MUX_IN14" B L 6400 7100 60 
F15 "MUX_IN15" B L 6400 7200 60 
F16 "MUX_IN16" B L 6400 7300 60 
F17 "MUX_OUT" B L 6400 9050 60 
F18 "MUX_IN32" B L 6400 8900 60 
F19 "MUX_IN31" B L 6400 8800 60 
F20 "MUX_IN30" B L 6400 8700 60 
F21 "MUX_IN29" B L 6400 8600 60 
F22 "MUX_IN28" B L 6400 8500 60 
F23 "MUX_IN27" B L 6400 8400 60 
F24 "MUX_IN26" B L 6400 8300 60 
F25 "MUX_IN25" B L 6400 8200 60 
F26 "MUX_IN24" B L 6400 8100 60 
F27 "MUX_IN23" B L 6400 8000 60 
F28 "MUX_IN22" B L 6400 7900 60 
F29 "MUX_IN21" B L 6400 7800 60 
F30 "MUX_IN20" B L 6400 7700 60 
F31 "MUX_IN19" B L 6400 7600 60 
F32 "MUX_IN18" B L 6400 7500 60 
F33 "MUX_IN17" B L 6400 7400 60 
F34 "MUX_A0" I L 6400 9200 60 
F35 "MUX_A1" I L 6400 9300 60 
F36 "MUX_A2" I L 6400 9400 60 
F37 "MUX_A3" I L 6400 9500 60 
F38 "MUX_A4" I L 6400 9600 60 
F39 "MUX_EN" I L 6400 9750 60 
F40 "MUX_IN13" B L 6400 7000 60 
$EndSheet
Wire Wire Line
	6400 9200 6000 9200
Text Label 6000 9200 0    60   ~ 0
SAFE_A0
Text Label 6000 9300 0    60   ~ 0
SAFE_A1
Text Label 6000 9400 0    60   ~ 0
SAFE_A2
Text Label 6000 9500 0    60   ~ 0
SAFE_A3
Text Label 6000 9600 0    60   ~ 0
SAFE_A4
Text Label 6000 9750 0    60   ~ 0
SAFE_EN
Wire Wire Line
	6000 9300 6400 9300
Wire Wire Line
	6400 9400 6000 9400
Wire Wire Line
	6000 9500 6400 9500
Wire Wire Line
	6000 9600 6400 9600
Wire Wire Line
	6000 9750 6400 9750
$Sheet
S 5300 5700 600  4150
U 5A352A07
F0 "sheet5A351FC3" 60
F1 "mux.sch" 60
F2 "MUX_IN01" B L 5300 5800 60 
F3 "MUX_IN02" B L 5300 5900 60 
F4 "MUX_IN03" B L 5300 6000 60 
F5 "MUX_IN04" B L 5300 6100 60 
F6 "MUX_IN05" B L 5300 6200 60 
F7 "MUX_IN06" B L 5300 6300 60 
F8 "MUX_IN07" B L 5300 6400 60 
F9 "MUX_IN08" B L 5300 6500 60 
F10 "MUX_IN09" B L 5300 6600 60 
F11 "MUX_IN10" B L 5300 6700 60 
F12 "MUX_IN11" B L 5300 6800 60 
F13 "MUX_IN12" B L 5300 6900 60 
F14 "MUX_IN14" B L 5300 7100 60 
F15 "MUX_IN15" B L 5300 7200 60 
F16 "MUX_IN16" B L 5300 7300 60 
F17 "MUX_OUT" B L 5300 9050 60 
F18 "MUX_IN32" B L 5300 8900 60 
F19 "MUX_IN31" B L 5300 8800 60 
F20 "MUX_IN30" B L 5300 8700 60 
F21 "MUX_IN29" B L 5300 8600 60 
F22 "MUX_IN28" B L 5300 8500 60 
F23 "MUX_IN27" B L 5300 8400 60 
F24 "MUX_IN26" B L 5300 8300 60 
F25 "MUX_IN25" B L 5300 8200 60 
F26 "MUX_IN24" B L 5300 8100 60 
F27 "MUX_IN23" B L 5300 8000 60 
F28 "MUX_IN22" B L 5300 7900 60 
F29 "MUX_IN21" B L 5300 7800 60 
F30 "MUX_IN20" B L 5300 7700 60 
F31 "MUX_IN19" B L 5300 7600 60 
F32 "MUX_IN18" B L 5300 7500 60 
F33 "MUX_IN17" B L 5300 7400 60 
F34 "MUX_A0" I L 5300 9200 60 
F35 "MUX_A1" I L 5300 9300 60 
F36 "MUX_A2" I L 5300 9400 60 
F37 "MUX_A3" I L 5300 9500 60 
F38 "MUX_A4" I L 5300 9600 60 
F39 "MUX_EN" I L 5300 9750 60 
F40 "MUX_IN13" B L 5300 7000 60 
$EndSheet
Wire Wire Line
	5300 9200 4900 9200
Text Label 4900 9200 0    60   ~ 0
SAFE_A0
Text Label 4900 9300 0    60   ~ 0
SAFE_A1
Text Label 4900 9400 0    60   ~ 0
SAFE_A2
Text Label 4900 9500 0    60   ~ 0
SAFE_A3
Text Label 4900 9600 0    60   ~ 0
SAFE_A4
Text Label 4900 9750 0    60   ~ 0
SAFE_EN
Wire Wire Line
	4900 9300 5300 9300
Wire Wire Line
	5300 9400 4900 9400
Wire Wire Line
	4900 9500 5300 9500
Wire Wire Line
	4900 9600 5300 9600
Wire Wire Line
	4900 9750 5300 9750
$Sheet
S 4200 5700 600  4150
U 5A352AB1
F0 "sheet5A351FC4" 60
F1 "mux.sch" 60
F2 "MUX_IN01" B L 4200 5800 60 
F3 "MUX_IN02" B L 4200 5900 60 
F4 "MUX_IN03" B L 4200 6000 60 
F5 "MUX_IN04" B L 4200 6100 60 
F6 "MUX_IN05" B L 4200 6200 60 
F7 "MUX_IN06" B L 4200 6300 60 
F8 "MUX_IN07" B L 4200 6400 60 
F9 "MUX_IN08" B L 4200 6500 60 
F10 "MUX_IN09" B L 4200 6600 60 
F11 "MUX_IN10" B L 4200 6700 60 
F12 "MUX_IN11" B L 4200 6800 60 
F13 "MUX_IN12" B L 4200 6900 60 
F14 "MUX_IN14" B L 4200 7100 60 
F15 "MUX_IN15" B L 4200 7200 60 
F16 "MUX_IN16" B L 4200 7300 60 
F17 "MUX_OUT" B L 4200 9050 60 
F18 "MUX_IN32" B L 4200 8900 60 
F19 "MUX_IN31" B L 4200 8800 60 
F20 "MUX_IN30" B L 4200 8700 60 
F21 "MUX_IN29" B L 4200 8600 60 
F22 "MUX_IN28" B L 4200 8500 60 
F23 "MUX_IN27" B L 4200 8400 60 
F24 "MUX_IN26" B L 4200 8300 60 
F25 "MUX_IN25" B L 4200 8200 60 
F26 "MUX_IN24" B L 4200 8100 60 
F27 "MUX_IN23" B L 4200 8000 60 
F28 "MUX_IN22" B L 4200 7900 60 
F29 "MUX_IN21" B L 4200 7800 60 
F30 "MUX_IN20" B L 4200 7700 60 
F31 "MUX_IN19" B L 4200 7600 60 
F32 "MUX_IN18" B L 4200 7500 60 
F33 "MUX_IN17" B L 4200 7400 60 
F34 "MUX_A0" I L 4200 9200 60 
F35 "MUX_A1" I L 4200 9300 60 
F36 "MUX_A2" I L 4200 9400 60 
F37 "MUX_A3" I L 4200 9500 60 
F38 "MUX_A4" I L 4200 9600 60 
F39 "MUX_EN" I L 4200 9750 60 
F40 "MUX_IN13" B L 4200 7000 60 
$EndSheet
Wire Wire Line
	4200 9200 3800 9200
Text Label 3800 9200 0    60   ~ 0
SAFE_A0
Text Label 3800 9300 0    60   ~ 0
SAFE_A1
Text Label 3800 9400 0    60   ~ 0
SAFE_A2
Text Label 3800 9500 0    60   ~ 0
SAFE_A3
Text Label 3800 9600 0    60   ~ 0
SAFE_A4
Text Label 3800 9750 0    60   ~ 0
SAFE_EN
Wire Wire Line
	3800 9300 4200 9300
Wire Wire Line
	4200 9400 3800 9400
Wire Wire Line
	3800 9500 4200 9500
Wire Wire Line
	3800 9600 4200 9600
Wire Wire Line
	3800 9750 4200 9750
$Sheet
S 3100 5700 600  4150
U 5A352B5B
F0 "sheet5A351FC5" 60
F1 "mux.sch" 60
F2 "MUX_IN01" B L 3100 5800 60 
F3 "MUX_IN02" B L 3100 5900 60 
F4 "MUX_IN03" B L 3100 6000 60 
F5 "MUX_IN04" B L 3100 6100 60 
F6 "MUX_IN05" B L 3100 6200 60 
F7 "MUX_IN06" B L 3100 6300 60 
F8 "MUX_IN07" B L 3100 6400 60 
F9 "MUX_IN08" B L 3100 6500 60 
F10 "MUX_IN09" B L 3100 6600 60 
F11 "MUX_IN10" B L 3100 6700 60 
F12 "MUX_IN11" B L 3100 6800 60 
F13 "MUX_IN12" B L 3100 6900 60 
F14 "MUX_IN14" B L 3100 7100 60 
F15 "MUX_IN15" B L 3100 7200 60 
F16 "MUX_IN16" B L 3100 7300 60 
F17 "MUX_OUT" B L 3100 9050 60 
F18 "MUX_IN32" B L 3100 8900 60 
F19 "MUX_IN31" B L 3100 8800 60 
F20 "MUX_IN30" B L 3100 8700 60 
F21 "MUX_IN29" B L 3100 8600 60 
F22 "MUX_IN28" B L 3100 8500 60 
F23 "MUX_IN27" B L 3100 8400 60 
F24 "MUX_IN26" B L 3100 8300 60 
F25 "MUX_IN25" B L 3100 8200 60 
F26 "MUX_IN24" B L 3100 8100 60 
F27 "MUX_IN23" B L 3100 8000 60 
F28 "MUX_IN22" B L 3100 7900 60 
F29 "MUX_IN21" B L 3100 7800 60 
F30 "MUX_IN20" B L 3100 7700 60 
F31 "MUX_IN19" B L 3100 7600 60 
F32 "MUX_IN18" B L 3100 7500 60 
F33 "MUX_IN17" B L 3100 7400 60 
F34 "MUX_A0" I L 3100 9200 60 
F35 "MUX_A1" I L 3100 9300 60 
F36 "MUX_A2" I L 3100 9400 60 
F37 "MUX_A3" I L 3100 9500 60 
F38 "MUX_A4" I L 3100 9600 60 
F39 "MUX_EN" I L 3100 9750 60 
F40 "MUX_IN13" B L 3100 7000 60 
$EndSheet
Wire Wire Line
	3100 9200 2700 9200
Text Label 2700 9200 0    60   ~ 0
SAFE_A0
Text Label 2700 9300 0    60   ~ 0
SAFE_A1
Text Label 2700 9400 0    60   ~ 0
SAFE_A2
Text Label 2700 9500 0    60   ~ 0
SAFE_A3
Text Label 2700 9600 0    60   ~ 0
SAFE_A4
Text Label 2700 9750 0    60   ~ 0
SAFE_EN
Wire Wire Line
	2700 9300 3100 9300
Wire Wire Line
	3100 9400 2700 9400
Wire Wire Line
	2700 9500 3100 9500
Wire Wire Line
	2700 9600 3100 9600
Wire Wire Line
	2700 9750 3100 9750
$Sheet
S 2000 5700 600  4150
U 5A352C05
F0 "sheet5A351FC6" 60
F1 "mux.sch" 60
F2 "MUX_IN01" B L 2000 5800 60 
F3 "MUX_IN02" B L 2000 5900 60 
F4 "MUX_IN03" B L 2000 6000 60 
F5 "MUX_IN04" B L 2000 6100 60 
F6 "MUX_IN05" B L 2000 6200 60 
F7 "MUX_IN06" B L 2000 6300 60 
F8 "MUX_IN07" B L 2000 6400 60 
F9 "MUX_IN08" B L 2000 6500 60 
F10 "MUX_IN09" B L 2000 6600 60 
F11 "MUX_IN10" B L 2000 6700 60 
F12 "MUX_IN11" B L 2000 6800 60 
F13 "MUX_IN12" B L 2000 6900 60 
F14 "MUX_IN14" B L 2000 7100 60 
F15 "MUX_IN15" B L 2000 7200 60 
F16 "MUX_IN16" B L 2000 7300 60 
F17 "MUX_OUT" B L 2000 9050 60 
F18 "MUX_IN32" B L 2000 8900 60 
F19 "MUX_IN31" B L 2000 8800 60 
F20 "MUX_IN30" B L 2000 8700 60 
F21 "MUX_IN29" B L 2000 8600 60 
F22 "MUX_IN28" B L 2000 8500 60 
F23 "MUX_IN27" B L 2000 8400 60 
F24 "MUX_IN26" B L 2000 8300 60 
F25 "MUX_IN25" B L 2000 8200 60 
F26 "MUX_IN24" B L 2000 8100 60 
F27 "MUX_IN23" B L 2000 8000 60 
F28 "MUX_IN22" B L 2000 7900 60 
F29 "MUX_IN21" B L 2000 7800 60 
F30 "MUX_IN20" B L 2000 7700 60 
F31 "MUX_IN19" B L 2000 7600 60 
F32 "MUX_IN18" B L 2000 7500 60 
F33 "MUX_IN17" B L 2000 7400 60 
F34 "MUX_A0" I L 2000 9200 60 
F35 "MUX_A1" I L 2000 9300 60 
F36 "MUX_A2" I L 2000 9400 60 
F37 "MUX_A3" I L 2000 9500 60 
F38 "MUX_A4" I L 2000 9600 60 
F39 "MUX_EN" I L 2000 9750 60 
F40 "MUX_IN13" B L 2000 7000 60 
$EndSheet
Wire Wire Line
	2000 9200 1600 9200
Text Label 1600 9200 0    60   ~ 0
SAFE_A0
Text Label 1600 9300 0    60   ~ 0
SAFE_A1
Text Label 1600 9400 0    60   ~ 0
SAFE_A2
Text Label 1600 9500 0    60   ~ 0
SAFE_A3
Text Label 1600 9600 0    60   ~ 0
SAFE_A4
Text Label 1600 9750 0    60   ~ 0
SAFE_EN
Wire Wire Line
	1600 9300 2000 9300
Wire Wire Line
	2000 9400 1600 9400
Wire Wire Line
	1600 9500 2000 9500
Wire Wire Line
	1600 9600 2000 9600
Wire Wire Line
	1600 9750 2000 9750
$Sheet
S 14100 5700 600  4150
U 5A3524B7
F0 "sheet5A351FBB" 60
F1 "mux.sch" 60
F2 "MUX_IN01" B L 14100 5800 60 
F3 "MUX_IN02" B L 14100 5900 60 
F4 "MUX_IN03" B L 14100 6000 60 
F5 "MUX_IN04" B L 14100 6100 60 
F6 "MUX_IN05" B L 14100 6200 60 
F7 "MUX_IN06" B L 14100 6300 60 
F8 "MUX_IN07" B L 14100 6400 60 
F9 "MUX_IN08" B L 14100 6500 60 
F10 "MUX_IN09" B L 14100 6600 60 
F11 "MUX_IN10" B L 14100 6700 60 
F12 "MUX_IN11" B L 14100 6800 60 
F13 "MUX_IN12" B L 14100 6900 60 
F14 "MUX_IN14" B L 14100 7100 60 
F15 "MUX_IN15" B L 14100 7200 60 
F16 "MUX_IN16" B L 14100 7300 60 
F17 "MUX_OUT" B L 14100 9050 60 
F18 "MUX_IN32" B L 14100 8900 60 
F19 "MUX_IN31" B L 14100 8800 60 
F20 "MUX_IN30" B L 14100 8700 60 
F21 "MUX_IN29" B L 14100 8600 60 
F22 "MUX_IN28" B L 14100 8500 60 
F23 "MUX_IN27" B L 14100 8400 60 
F24 "MUX_IN26" B L 14100 8300 60 
F25 "MUX_IN25" B L 14100 8200 60 
F26 "MUX_IN24" B L 14100 8100 60 
F27 "MUX_IN23" B L 14100 8000 60 
F28 "MUX_IN22" B L 14100 7900 60 
F29 "MUX_IN21" B L 14100 7800 60 
F30 "MUX_IN20" B L 14100 7700 60 
F31 "MUX_IN19" B L 14100 7600 60 
F32 "MUX_IN18" B L 14100 7500 60 
F33 "MUX_IN17" B L 14100 7400 60 
F34 "MUX_A0" I L 14100 9200 60 
F35 "MUX_A1" I L 14100 9300 60 
F36 "MUX_A2" I L 14100 9400 60 
F37 "MUX_A3" I L 14100 9500 60 
F38 "MUX_A4" I L 14100 9600 60 
F39 "MUX_EN" I L 14100 9750 60 
F40 "MUX_IN13" B L 14100 7000 60 
$EndSheet
NoConn ~ 18450 8600
$EndSCHEMATC
