EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
NoConn ~ 4800 3100
NoConn ~ 5600 3100
NoConn ~ 5600 3000
NoConn ~ 5600 2900
NoConn ~ 5600 2800
NoConn ~ 5600 2700
NoConn ~ 5600 2600
NoConn ~ 5600 2500
NoConn ~ 5600 2400
NoConn ~ 5600 2300
NoConn ~ 5600 2200
NoConn ~ 5600 2100
NoConn ~ 5600 2000
NoConn ~ 5600 1900
NoConn ~ 5600 1800
NoConn ~ 4800 1800
NoConn ~ 4800 1900
NoConn ~ 4800 2000
NoConn ~ 4800 2100
NoConn ~ 4800 2200
NoConn ~ 4800 2300
NoConn ~ 4800 2400
NoConn ~ 4800 2500
NoConn ~ 4800 2600
NoConn ~ 4800 2700
NoConn ~ 4800 2800
NoConn ~ 4800 2900
NoConn ~ 4800 3000
Wire Wire Line
	3100 1800 3450 1800
Text Label 3150 1800 0    60   ~ 0
P002
Wire Wire Line
	3100 1900 3450 1900
Wire Wire Line
	3100 2000 3450 2000
Wire Wire Line
	3100 2100 3450 2100
Text Label 3150 1900 0    60   ~ 0
P004
Text Label 3150 2000 0    60   ~ 0
P006
Text Label 3150 2100 0    60   ~ 0
P008
Wire Wire Line
	3100 2200 3450 2200
Text Label 3150 2200 0    60   ~ 0
P010
Wire Wire Line
	3100 2300 3450 2300
Wire Wire Line
	3100 2400 3450 2400
Wire Wire Line
	3100 2500 3450 2500
Text Label 3150 2300 0    60   ~ 0
P012
Text Label 3150 2400 0    60   ~ 0
P014
Text Label 3150 2500 0    60   ~ 0
P016
Wire Wire Line
	3100 2600 3450 2600
Text Label 3150 2600 0    60   ~ 0
P018
Wire Wire Line
	3100 2700 3450 2700
Wire Wire Line
	3100 2800 3450 2800
Wire Wire Line
	3100 2900 3450 2900
Text Label 3150 2700 0    60   ~ 0
P020
Text Label 3150 2800 0    60   ~ 0
P022
Text Label 3150 2900 0    60   ~ 0
P024
Wire Wire Line
	3100 3000 3450 3000
Text Label 3150 3000 0    60   ~ 0
P026
Wire Wire Line
	3100 3100 3450 3100
Wire Wire Line
	3100 3200 3450 3200
Wire Wire Line
	3100 3300 3450 3300
Text Label 3150 3100 0    60   ~ 0
P028
Text Label 3150 3200 0    60   ~ 0
P030
Text Label 3150 3300 0    60   ~ 0
P032
Wire Wire Line
	3100 3400 3450 3400
Text Label 3150 3400 0    60   ~ 0
P034
Wire Wire Line
	3100 3500 3450 3500
Wire Wire Line
	3100 3600 3450 3600
Wire Wire Line
	3100 3700 3450 3700
Text Label 3150 3500 0    60   ~ 0
P036
Text Label 3150 3600 0    60   ~ 0
P038
Text Label 3150 3700 0    60   ~ 0
P040
Wire Wire Line
	3100 3800 3450 3800
Text Label 3150 3800 0    60   ~ 0
P042
Wire Wire Line
	3100 3900 3450 3900
Wire Wire Line
	3100 4000 3450 4000
Wire Wire Line
	3100 4100 3450 4100
Text Label 3150 3900 0    60   ~ 0
P044
Text Label 3150 4000 0    60   ~ 0
P046
Text Label 3150 4100 0    60   ~ 0
P048
Wire Wire Line
	3100 4200 3450 4200
Text Label 3150 4200 0    60   ~ 0
P050
Wire Wire Line
	3100 4300 3450 4300
Wire Wire Line
	3100 4400 3450 4400
Wire Wire Line
	3100 4500 3450 4500
Text Label 3150 4300 0    60   ~ 0
P052
Text Label 3150 4400 0    60   ~ 0
P054
Text Label 3150 4500 0    60   ~ 0
P056
Wire Wire Line
	3100 4600 3450 4600
Text Label 3150 4600 0    60   ~ 0
P058
Wire Wire Line
	3100 4700 3450 4700
Wire Wire Line
	3100 4800 3450 4800
Wire Wire Line
	3100 4900 3450 4900
Text Label 3150 4700 0    60   ~ 0
P060
Text Label 3150 4800 0    60   ~ 0
P062
Text Label 3150 4900 0    60   ~ 0
P064
Wire Wire Line
	1950 1800 2300 1800
Text Label 2000 1800 0    60   ~ 0
P001
Wire Wire Line
	1950 1900 2300 1900
Wire Wire Line
	1950 2000 2300 2000
Wire Wire Line
	1950 2100 2300 2100
Text Label 2000 1900 0    60   ~ 0
P003
Text Label 2000 2000 0    60   ~ 0
P005
Text Label 2000 2100 0    60   ~ 0
P007
Wire Wire Line
	1950 2200 2300 2200
Text Label 2000 2200 0    60   ~ 0
P009
Wire Wire Line
	1950 2300 2300 2300
Wire Wire Line
	1950 2400 2300 2400
Wire Wire Line
	1950 2500 2300 2500
Text Label 2000 2300 0    60   ~ 0
P011
Text Label 2000 2400 0    60   ~ 0
P013
Text Label 2000 2500 0    60   ~ 0
P015
Wire Wire Line
	1950 2600 2300 2600
Text Label 2000 2600 0    60   ~ 0
P017
Wire Wire Line
	1950 2700 2300 2700
Wire Wire Line
	1950 2800 2300 2800
Wire Wire Line
	1950 2900 2300 2900
Text Label 2000 2700 0    60   ~ 0
P019
Text Label 2000 2800 0    60   ~ 0
P021
Text Label 2000 2900 0    60   ~ 0
P023
Wire Wire Line
	1950 3000 2300 3000
Text Label 2000 3000 0    60   ~ 0
P025
Wire Wire Line
	1950 3100 2300 3100
Wire Wire Line
	1950 3200 2300 3200
Wire Wire Line
	1950 3300 2300 3300
Text Label 2000 3100 0    60   ~ 0
P027
Text Label 2000 3200 0    60   ~ 0
P029
Text Label 2000 3300 0    60   ~ 0
P031
Wire Wire Line
	1950 3400 2300 3400
Text Label 2000 3400 0    60   ~ 0
P033
Wire Wire Line
	1950 3500 2300 3500
Wire Wire Line
	1950 3600 2300 3600
Wire Wire Line
	1950 3700 2300 3700
Text Label 2000 3500 0    60   ~ 0
P035
Text Label 2000 3600 0    60   ~ 0
P037
Text Label 2000 3700 0    60   ~ 0
P039
Wire Wire Line
	1950 3800 2300 3800
Text Label 2000 3800 0    60   ~ 0
P041
Wire Wire Line
	1950 3900 2300 3900
Wire Wire Line
	1950 4000 2300 4000
Wire Wire Line
	1950 4100 2300 4100
Text Label 2000 3900 0    60   ~ 0
P043
Text Label 2000 4000 0    60   ~ 0
P045
Text Label 2000 4100 0    60   ~ 0
P047
Wire Wire Line
	1950 4200 2300 4200
Text Label 2000 4200 0    60   ~ 0
P049
Wire Wire Line
	1950 4300 2300 4300
Wire Wire Line
	1950 4400 2300 4400
Wire Wire Line
	1950 4500 2300 4500
Text Label 2000 4300 0    60   ~ 0
P051
Text Label 2000 4400 0    60   ~ 0
P053
Text Label 2000 4500 0    60   ~ 0
P055
Wire Wire Line
	1950 4600 2300 4600
Text Label 2000 4600 0    60   ~ 0
P057
Wire Wire Line
	1950 4700 2300 4700
Wire Wire Line
	1950 4800 2300 4800
Wire Wire Line
	1950 4900 2300 4900
Text Label 2000 4700 0    60   ~ 0
P059
Text Label 2000 4800 0    60   ~ 0
P061
Text Label 2000 4900 0    60   ~ 0
P063
Wire Wire Line
	5600 3800 5950 3800
Text Label 5650 3800 0    60   ~ 0
P087
Wire Wire Line
	5600 3900 5950 3900
Wire Wire Line
	5600 4000 5950 4000
Wire Wire Line
	5600 4100 5950 4100
Text Label 5650 3900 0    60   ~ 0
P085
Text Label 5650 4000 0    60   ~ 0
P083
Text Label 5650 4100 0    60   ~ 0
P081
Wire Wire Line
	5600 4200 5950 4200
Text Label 5650 4200 0    60   ~ 0
P079
Wire Wire Line
	5600 4300 5950 4300
Wire Wire Line
	5600 4400 5950 4400
Wire Wire Line
	5600 4500 5950 4500
Text Label 5650 4300 0    60   ~ 0
P077
Text Label 5650 4400 0    60   ~ 0
P075
Text Label 5650 4500 0    60   ~ 0
P073
Wire Wire Line
	5600 4600 5950 4600
Text Label 5650 4600 0    60   ~ 0
P071
Wire Wire Line
	5600 4700 5950 4700
Wire Wire Line
	5600 4800 5950 4800
Wire Wire Line
	5600 4900 5950 4900
Text Label 5650 4700 0    60   ~ 0
P069
Text Label 5650 4800 0    60   ~ 0
P067
Text Label 5650 4900 0    60   ~ 0
P065
Wire Wire Line
	4450 3800 4800 3800
Text Label 4500 3800 0    60   ~ 0
P088
Wire Wire Line
	4450 3900 4800 3900
Wire Wire Line
	4450 4000 4800 4000
Wire Wire Line
	4450 4100 4800 4100
Text Label 4500 3900 0    60   ~ 0
P086
Text Label 4500 4000 0    60   ~ 0
P084
Text Label 4500 4100 0    60   ~ 0
P082
Wire Wire Line
	4450 4200 4800 4200
Text Label 4500 4200 0    60   ~ 0
P080
Wire Wire Line
	4450 4300 4800 4300
Wire Wire Line
	4450 4400 4800 4400
Wire Wire Line
	4450 4500 4800 4500
Text Label 4500 4300 0    60   ~ 0
P078
Text Label 4500 4400 0    60   ~ 0
P076
Text Label 4500 4500 0    60   ~ 0
P074
Wire Wire Line
	4450 4600 4800 4600
Text Label 4500 4600 0    60   ~ 0
P072
Wire Wire Line
	4450 4700 4800 4700
Wire Wire Line
	4450 4800 4800 4800
Wire Wire Line
	4450 4900 4800 4900
Text Label 4500 4700 0    60   ~ 0
P070
Text Label 4500 4800 0    60   ~ 0
P068
Text Label 4500 4900 0    60   ~ 0
P066
Wire Wire Line
	7300 2550 7650 2550
Text Label 7350 2550 0    60   ~ 0
P001
Wire Wire Line
	7300 2750 7650 2750
Wire Wire Line
	7300 2950 7650 2950
Wire Wire Line
	7300 3150 7650 3150
Text Label 7350 2750 0    60   ~ 0
P003
Text Label 7350 2950 0    60   ~ 0
P005
Text Label 7350 3150 0    60   ~ 0
P007
Wire Wire Line
	7300 3350 7650 3350
Text Label 7350 3350 0    60   ~ 0
P009
Wire Wire Line
	7300 3550 7650 3550
Wire Wire Line
	7300 3750 7650 3750
Wire Wire Line
	7300 3950 7650 3950
Text Label 7350 3550 0    60   ~ 0
P011
Text Label 7350 3750 0    60   ~ 0
P013
Text Label 7350 3950 0    60   ~ 0
P015
Wire Wire Line
	7300 4150 7650 4150
Text Label 7350 4150 0    60   ~ 0
P017
Wire Wire Line
	7300 4350 7650 4350
Wire Wire Line
	7300 4550 7650 4550
Wire Wire Line
	8150 2550 8500 2550
Text Label 7350 4350 0    60   ~ 0
P019
Text Label 7350 4550 0    60   ~ 0
P021
Text Label 8200 2550 0    60   ~ 0
P023
Wire Wire Line
	8150 2750 8500 2750
Text Label 8200 2750 0    60   ~ 0
P025
Wire Wire Line
	8150 2950 8500 2950
Wire Wire Line
	8150 3150 8500 3150
Wire Wire Line
	8150 3350 8500 3350
Text Label 8200 2950 0    60   ~ 0
P027
Text Label 8200 3150 0    60   ~ 0
P029
Text Label 8200 3350 0    60   ~ 0
P031
Wire Wire Line
	8150 3550 8500 3550
Text Label 8200 3550 0    60   ~ 0
P033
Wire Wire Line
	8150 3750 8500 3750
Wire Wire Line
	8150 3950 8500 3950
Wire Wire Line
	8150 4150 8500 4150
Text Label 8200 3750 0    60   ~ 0
P035
Text Label 8200 3950 0    60   ~ 0
P037
Text Label 8200 4150 0    60   ~ 0
P039
Wire Wire Line
	8150 4350 8500 4350
Text Label 8200 4350 0    60   ~ 0
P041
Wire Wire Line
	8150 4550 8500 4550
Wire Wire Line
	8950 2550 9300 2550
Wire Wire Line
	8950 2750 9300 2750
Text Label 8200 4550 0    60   ~ 0
P043
Text Label 9000 2550 0    60   ~ 0
P045
Text Label 9000 2750 0    60   ~ 0
P047
Wire Wire Line
	8950 2950 9300 2950
Text Label 9000 2950 0    60   ~ 0
P049
Wire Wire Line
	8950 3150 9300 3150
Wire Wire Line
	8950 3350 9300 3350
Wire Wire Line
	8950 3550 9300 3550
Text Label 9000 3150 0    60   ~ 0
P051
Text Label 9000 3350 0    60   ~ 0
P053
Text Label 9000 3550 0    60   ~ 0
P055
Wire Wire Line
	8950 3750 9300 3750
Text Label 9000 3750 0    60   ~ 0
P057
Wire Wire Line
	8950 3950 9300 3950
Wire Wire Line
	8950 4150 9300 4150
Wire Wire Line
	8950 4350 9300 4350
Text Label 9000 3950 0    60   ~ 0
P059
Text Label 9000 4150 0    60   ~ 0
P061
Text Label 9000 4350 0    60   ~ 0
P063
Wire Wire Line
	7300 2650 7650 2650
Text Label 7350 2650 0    60   ~ 0
P002
Wire Wire Line
	7300 2850 7650 2850
Wire Wire Line
	7300 3050 7650 3050
Wire Wire Line
	7300 3250 7650 3250
Text Label 7350 2850 0    60   ~ 0
P004
Text Label 7350 3050 0    60   ~ 0
P006
Text Label 7350 3250 0    60   ~ 0
P008
Wire Wire Line
	7300 3450 7650 3450
Text Label 7350 3450 0    60   ~ 0
P010
Wire Wire Line
	7300 3650 7650 3650
Wire Wire Line
	7300 3850 7650 3850
Wire Wire Line
	7300 4050 7650 4050
Text Label 7350 3650 0    60   ~ 0
P012
Text Label 7350 3850 0    60   ~ 0
P014
Text Label 7350 4050 0    60   ~ 0
P016
Wire Wire Line
	7300 4250 7650 4250
Text Label 7350 4250 0    60   ~ 0
P018
Wire Wire Line
	7300 4450 7650 4450
Wire Wire Line
	7300 4650 7650 4650
Wire Wire Line
	8150 2650 8500 2650
Text Label 7350 4450 0    60   ~ 0
P020
Text Label 7350 4650 0    60   ~ 0
P022
Text Label 8200 2650 0    60   ~ 0
P024
Wire Wire Line
	8150 2850 8500 2850
Text Label 8200 2850 0    60   ~ 0
P026
Wire Wire Line
	8150 3050 8500 3050
Wire Wire Line
	8150 3250 8500 3250
Wire Wire Line
	8150 3450 8500 3450
Text Label 8200 3050 0    60   ~ 0
P028
Text Label 8200 3250 0    60   ~ 0
P030
Text Label 8200 3450 0    60   ~ 0
P032
Wire Wire Line
	8150 3650 8500 3650
Text Label 8200 3650 0    60   ~ 0
P034
Wire Wire Line
	8150 3850 8500 3850
Wire Wire Line
	8150 4050 8500 4050
Wire Wire Line
	8150 4250 8500 4250
Text Label 8200 3850 0    60   ~ 0
P036
Text Label 8200 4050 0    60   ~ 0
P038
Text Label 8200 4250 0    60   ~ 0
P040
Wire Wire Line
	8150 4450 8500 4450
Text Label 8200 4450 0    60   ~ 0
P042
Wire Wire Line
	8150 4650 8500 4650
Wire Wire Line
	8950 2650 9300 2650
Wire Wire Line
	8950 2850 9300 2850
Text Label 8200 4650 0    60   ~ 0
P044
Text Label 9000 2650 0    60   ~ 0
P046
Text Label 9000 2850 0    60   ~ 0
P048
Wire Wire Line
	8950 3050 9300 3050
Text Label 9000 3050 0    60   ~ 0
P050
Wire Wire Line
	8950 3250 9300 3250
Wire Wire Line
	8950 3450 9300 3450
Wire Wire Line
	8950 3650 9300 3650
Text Label 9000 3250 0    60   ~ 0
P052
Text Label 9000 3450 0    60   ~ 0
P054
Text Label 9000 3650 0    60   ~ 0
P056
Wire Wire Line
	8950 3850 9300 3850
Text Label 9000 3850 0    60   ~ 0
P058
Wire Wire Line
	8950 4050 9300 4050
Wire Wire Line
	8950 4250 9300 4250
Wire Wire Line
	8950 4450 9300 4450
Text Label 9000 4050 0    60   ~ 0
P060
Text Label 9000 4250 0    60   ~ 0
P062
Text Label 9000 4450 0    60   ~ 0
P064
Wire Wire Line
	9800 4650 10150 4650
Text Label 9850 4650 0    60   ~ 0
P088
Wire Wire Line
	9800 4450 10150 4450
Wire Wire Line
	9800 4250 10150 4250
Wire Wire Line
	9800 4050 10150 4050
Text Label 9850 4450 0    60   ~ 0
P086
Text Label 9850 4250 0    60   ~ 0
P084
Text Label 9850 4050 0    60   ~ 0
P082
Wire Wire Line
	9800 3850 10150 3850
Text Label 9850 3850 0    60   ~ 0
P080
Wire Wire Line
	9800 3650 10150 3650
Wire Wire Line
	9800 3450 10150 3450
Wire Wire Line
	9800 3250 10150 3250
Text Label 9850 3650 0    60   ~ 0
P078
Text Label 9850 3450 0    60   ~ 0
P076
Text Label 9850 3250 0    60   ~ 0
P074
Wire Wire Line
	9800 3050 10150 3050
Text Label 9850 3050 0    60   ~ 0
P072
Wire Wire Line
	9800 2850 10150 2850
Wire Wire Line
	9800 2650 10150 2650
Wire Wire Line
	8950 4650 9300 4650
Text Label 9850 2850 0    60   ~ 0
P070
Text Label 9850 2650 0    60   ~ 0
P068
Text Label 9000 4650 0    60   ~ 0
P066
Wire Wire Line
	9800 4550 10150 4550
Text Label 9850 4550 0    60   ~ 0
P087
Wire Wire Line
	9800 4350 10150 4350
Wire Wire Line
	9800 4150 10150 4150
Wire Wire Line
	9800 3950 10150 3950
Text Label 9850 4350 0    60   ~ 0
P085
Text Label 9850 4150 0    60   ~ 0
P083
Text Label 9850 3950 0    60   ~ 0
P081
Wire Wire Line
	9800 3750 10150 3750
Text Label 9850 3750 0    60   ~ 0
P079
Wire Wire Line
	9800 3550 10150 3550
Wire Wire Line
	9800 3350 10150 3350
Wire Wire Line
	9800 3150 10150 3150
Text Label 9850 3550 0    60   ~ 0
P077
Text Label 9850 3350 0    60   ~ 0
P075
Text Label 9850 3150 0    60   ~ 0
P073
Wire Wire Line
	9800 2950 10150 2950
Text Label 9850 2950 0    60   ~ 0
P071
Wire Wire Line
	9800 2750 10150 2750
Wire Wire Line
	9800 2550 10150 2550
Wire Wire Line
	8950 4550 9300 4550
Text Label 9850 2750 0    60   ~ 0
P069
Text Label 9850 2550 0    60   ~ 0
P067
Text Label 9000 4550 0    60   ~ 0
P065
$Comp
L Connector_Generic:Conn_02x22_Top_Bottom J1
U 1 1 5F8A23AA
P 7850 3550
F 0 "J1" H 7900 4767 50  0000 C CNN
F 1 "Conn_02x22_Top_Bottom" H 7900 4676 50  0000 C CNN
F 2 "Connector_Dsub:DSUB-44-HD_Male_Horizontal_P2.29x1.98mm_EdgePinOffset3.03mm_Housed_MountingHolesOffset4.94mm" H 7850 3550 50  0001 C CNN
F 3 "~" H 7850 3550 50  0001 C CNN
	1    7850 3550
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x22_Top_Bottom J2
U 1 1 5F935D45
P 9500 3550
F 0 "J2" H 9550 4767 50  0000 C CNN
F 1 "Conn_02x22_Top_Bottom" H 9550 4676 50  0000 C CNN
F 2 "Connector_Dsub:DSUB-44-HD_Male_Horizontal_P2.29x1.98mm_EdgePinOffset3.03mm_Housed_MountingHolesOffset4.94mm" H 9500 3550 50  0001 C CNN
F 3 "~" H 9500 3550 50  0001 C CNN
	1    9500 3550
	1    0    0    -1  
$EndComp
$Comp
L connector-header-128:connector-header-128 U1
U 1 1 57ABEA1B
P 2500 1800
F 0 "U1" H 3950 1950 60  0000 C CNN
F 1 "connector-header-128" H 3950 2050 60  0000 C CNN
F 2 "cable-tester:connector-header-128" H 2500 1800 60  0001 C CNN
F 3 "" H 2500 1800 60  0000 C CNN
	1    2500 1800
	1    0    0    -1  
$EndComp
NoConn ~ 4800 3700
NoConn ~ 4800 3600
NoConn ~ 4800 3500
NoConn ~ 4800 3400
NoConn ~ 4800 3300
NoConn ~ 4800 3200
NoConn ~ 5600 3200
NoConn ~ 5600 3300
NoConn ~ 5600 3400
NoConn ~ 5600 3500
NoConn ~ 5600 3600
NoConn ~ 5600 3700
$EndSCHEMATC
