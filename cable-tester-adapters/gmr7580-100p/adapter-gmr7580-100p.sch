EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:mdm
LIBS:ad5270
LIBS:adg732
LIBS:ads1120
LIBS:arduino
LIBS:connector-header-128
LIBS:lm234-to92
LIBS:lmc7660
LIBS:lp2985
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L connector-header-128 U1
U 1 1 57ABEA1B
P 2500 1800
F 0 "U1" H 3950 1950 60  0000 C CNN
F 1 "connector-header-128" H 3950 2050 60  0000 C CNN
F 2 "cable-tester:connector-header-128" H 2500 1800 60  0001 C CNN
F 3 "" H 2500 1800 60  0000 C CNN
	1    2500 1800
	1    0    0    -1  
$EndComp
$Comp
L MDM-100 P1
U 1 1 57ABEA96
P 7400 3650
F 0 "P1" H 7400 6350 50  0000 C CNN
F 1 "MDM-100" V 7400 3650 50  0000 C CNN
F 2 "mdm:gmr7580-100p" H 8500 3450 50  0001 C CNN
F 3 "" H 8500 3450 50  0000 C CNN
	1    7400 3650
	1    0    0    -1  
$EndComp
NoConn ~ 7650 1100
NoConn ~ 7150 1100
NoConn ~ 4800 3100
NoConn ~ 5600 3100
NoConn ~ 5600 3000
NoConn ~ 5600 2900
NoConn ~ 5600 2800
NoConn ~ 5600 2700
NoConn ~ 5600 2600
NoConn ~ 5600 2500
NoConn ~ 5600 2400
NoConn ~ 5600 2300
NoConn ~ 5600 2200
NoConn ~ 5600 2100
NoConn ~ 5600 2000
NoConn ~ 5600 1900
NoConn ~ 5600 1800
NoConn ~ 4800 1800
NoConn ~ 4800 1900
NoConn ~ 4800 2000
NoConn ~ 4800 2100
NoConn ~ 4800 2200
NoConn ~ 4800 2300
NoConn ~ 4800 2400
NoConn ~ 4800 2500
NoConn ~ 4800 2600
NoConn ~ 4800 2700
NoConn ~ 4800 2800
NoConn ~ 4800 2900
NoConn ~ 4800 3000
Wire Wire Line
	3100 1800 3450 1800
Text Label 3150 1800 0    60   ~ 0
P002
Wire Wire Line
	3100 1900 3450 1900
Wire Wire Line
	3100 2000 3450 2000
Wire Wire Line
	3100 2100 3450 2100
Text Label 3150 1900 0    60   ~ 0
P004
Text Label 3150 2000 0    60   ~ 0
P006
Text Label 3150 2100 0    60   ~ 0
P008
Wire Wire Line
	3100 2200 3450 2200
Text Label 3150 2200 0    60   ~ 0
P010
Wire Wire Line
	3100 2300 3450 2300
Wire Wire Line
	3100 2400 3450 2400
Wire Wire Line
	3100 2500 3450 2500
Text Label 3150 2300 0    60   ~ 0
P012
Text Label 3150 2400 0    60   ~ 0
P014
Text Label 3150 2500 0    60   ~ 0
P016
Wire Wire Line
	3100 2600 3450 2600
Text Label 3150 2600 0    60   ~ 0
P018
Wire Wire Line
	3100 2700 3450 2700
Wire Wire Line
	3100 2800 3450 2800
Wire Wire Line
	3100 2900 3450 2900
Text Label 3150 2700 0    60   ~ 0
P020
Text Label 3150 2800 0    60   ~ 0
P022
Text Label 3150 2900 0    60   ~ 0
P024
Wire Wire Line
	3100 3000 3450 3000
Text Label 3150 3000 0    60   ~ 0
P026
Wire Wire Line
	3100 3100 3450 3100
Wire Wire Line
	3100 3200 3450 3200
Wire Wire Line
	3100 3300 3450 3300
Text Label 3150 3100 0    60   ~ 0
P028
Text Label 3150 3200 0    60   ~ 0
P030
Text Label 3150 3300 0    60   ~ 0
P032
Wire Wire Line
	3100 3400 3450 3400
Text Label 3150 3400 0    60   ~ 0
P034
Wire Wire Line
	3100 3500 3450 3500
Wire Wire Line
	3100 3600 3450 3600
Wire Wire Line
	3100 3700 3450 3700
Text Label 3150 3500 0    60   ~ 0
P036
Text Label 3150 3600 0    60   ~ 0
P038
Text Label 3150 3700 0    60   ~ 0
P040
Wire Wire Line
	3100 3800 3450 3800
Text Label 3150 3800 0    60   ~ 0
P042
Wire Wire Line
	3100 3900 3450 3900
Wire Wire Line
	3100 4000 3450 4000
Wire Wire Line
	3100 4100 3450 4100
Text Label 3150 3900 0    60   ~ 0
P044
Text Label 3150 4000 0    60   ~ 0
P046
Text Label 3150 4100 0    60   ~ 0
P048
Wire Wire Line
	3100 4200 3450 4200
Text Label 3150 4200 0    60   ~ 0
P050
Wire Wire Line
	3100 4300 3450 4300
Wire Wire Line
	3100 4400 3450 4400
Wire Wire Line
	3100 4500 3450 4500
Text Label 3150 4300 0    60   ~ 0
P052
Text Label 3150 4400 0    60   ~ 0
P054
Text Label 3150 4500 0    60   ~ 0
P056
Wire Wire Line
	3100 4600 3450 4600
Text Label 3150 4600 0    60   ~ 0
P058
Wire Wire Line
	3100 4700 3450 4700
Wire Wire Line
	3100 4800 3450 4800
Wire Wire Line
	3100 4900 3450 4900
Text Label 3150 4700 0    60   ~ 0
P060
Text Label 3150 4800 0    60   ~ 0
P062
Text Label 3150 4900 0    60   ~ 0
P064
Wire Wire Line
	1950 1800 2300 1800
Text Label 2000 1800 0    60   ~ 0
P001
Wire Wire Line
	1950 1900 2300 1900
Wire Wire Line
	1950 2000 2300 2000
Wire Wire Line
	1950 2100 2300 2100
Text Label 2000 1900 0    60   ~ 0
P003
Text Label 2000 2000 0    60   ~ 0
P005
Text Label 2000 2100 0    60   ~ 0
P007
Wire Wire Line
	1950 2200 2300 2200
Text Label 2000 2200 0    60   ~ 0
P009
Wire Wire Line
	1950 2300 2300 2300
Wire Wire Line
	1950 2400 2300 2400
Wire Wire Line
	1950 2500 2300 2500
Text Label 2000 2300 0    60   ~ 0
P011
Text Label 2000 2400 0    60   ~ 0
P013
Text Label 2000 2500 0    60   ~ 0
P015
Wire Wire Line
	1950 2600 2300 2600
Text Label 2000 2600 0    60   ~ 0
P017
Wire Wire Line
	1950 2700 2300 2700
Wire Wire Line
	1950 2800 2300 2800
Wire Wire Line
	1950 2900 2300 2900
Text Label 2000 2700 0    60   ~ 0
P019
Text Label 2000 2800 0    60   ~ 0
P021
Text Label 2000 2900 0    60   ~ 0
P023
Wire Wire Line
	1950 3000 2300 3000
Text Label 2000 3000 0    60   ~ 0
P025
Wire Wire Line
	1950 3100 2300 3100
Wire Wire Line
	1950 3200 2300 3200
Wire Wire Line
	1950 3300 2300 3300
Text Label 2000 3100 0    60   ~ 0
P027
Text Label 2000 3200 0    60   ~ 0
P029
Text Label 2000 3300 0    60   ~ 0
P031
Wire Wire Line
	1950 3400 2300 3400
Text Label 2000 3400 0    60   ~ 0
P033
Wire Wire Line
	1950 3500 2300 3500
Wire Wire Line
	1950 3600 2300 3600
Wire Wire Line
	1950 3700 2300 3700
Text Label 2000 3500 0    60   ~ 0
P035
Text Label 2000 3600 0    60   ~ 0
P037
Text Label 2000 3700 0    60   ~ 0
P039
Wire Wire Line
	1950 3800 2300 3800
Text Label 2000 3800 0    60   ~ 0
P041
Wire Wire Line
	1950 3900 2300 3900
Wire Wire Line
	1950 4000 2300 4000
Wire Wire Line
	1950 4100 2300 4100
Text Label 2000 3900 0    60   ~ 0
P043
Text Label 2000 4000 0    60   ~ 0
P045
Text Label 2000 4100 0    60   ~ 0
P047
Wire Wire Line
	1950 4200 2300 4200
Text Label 2000 4200 0    60   ~ 0
P049
Wire Wire Line
	1950 4300 2300 4300
Wire Wire Line
	1950 4400 2300 4400
Wire Wire Line
	1950 4500 2300 4500
Text Label 2000 4300 0    60   ~ 0
P051
Text Label 2000 4400 0    60   ~ 0
P053
Text Label 2000 4500 0    60   ~ 0
P055
Wire Wire Line
	1950 4600 2300 4600
Text Label 2000 4600 0    60   ~ 0
P057
Wire Wire Line
	1950 4700 2300 4700
Wire Wire Line
	1950 4800 2300 4800
Wire Wire Line
	1950 4900 2300 4900
Text Label 2000 4700 0    60   ~ 0
P059
Text Label 2000 4800 0    60   ~ 0
P061
Text Label 2000 4900 0    60   ~ 0
P063
Wire Wire Line
	5600 3300 5950 3300
Text Label 5650 3300 0    60   ~ 0
P097
Wire Wire Line
	5600 3400 5950 3400
Text Label 5650 3400 0    60   ~ 0
P095
Wire Wire Line
	5600 3500 5950 3500
Wire Wire Line
	5600 3600 5950 3600
Wire Wire Line
	5600 3700 5950 3700
Text Label 5650 3500 0    60   ~ 0
P093
Text Label 5650 3600 0    60   ~ 0
P091
Text Label 5650 3700 0    60   ~ 0
P089
Wire Wire Line
	5600 3800 5950 3800
Text Label 5650 3800 0    60   ~ 0
P087
Wire Wire Line
	5600 3900 5950 3900
Wire Wire Line
	5600 4000 5950 4000
Wire Wire Line
	5600 4100 5950 4100
Text Label 5650 3900 0    60   ~ 0
P085
Text Label 5650 4000 0    60   ~ 0
P083
Text Label 5650 4100 0    60   ~ 0
P081
Wire Wire Line
	5600 4200 5950 4200
Text Label 5650 4200 0    60   ~ 0
P079
Wire Wire Line
	5600 4300 5950 4300
Wire Wire Line
	5600 4400 5950 4400
Wire Wire Line
	5600 4500 5950 4500
Text Label 5650 4300 0    60   ~ 0
P077
Text Label 5650 4400 0    60   ~ 0
P075
Text Label 5650 4500 0    60   ~ 0
P073
Wire Wire Line
	5600 4600 5950 4600
Text Label 5650 4600 0    60   ~ 0
P071
Wire Wire Line
	5600 4700 5950 4700
Wire Wire Line
	5600 4800 5950 4800
Wire Wire Line
	5600 4900 5950 4900
Text Label 5650 4700 0    60   ~ 0
P069
Text Label 5650 4800 0    60   ~ 0
P067
Text Label 5650 4900 0    60   ~ 0
P065
Wire Wire Line
	4450 3300 4800 3300
Text Label 4500 3300 0    60   ~ 0
P098
Wire Wire Line
	4450 3400 4800 3400
Text Label 4500 3400 0    60   ~ 0
P096
Wire Wire Line
	4450 3500 4800 3500
Wire Wire Line
	4450 3600 4800 3600
Wire Wire Line
	4450 3700 4800 3700
Text Label 4500 3500 0    60   ~ 0
P094
Text Label 4500 3600 0    60   ~ 0
P092
Text Label 4500 3700 0    60   ~ 0
P090
Wire Wire Line
	4450 3800 4800 3800
Text Label 4500 3800 0    60   ~ 0
P088
Wire Wire Line
	4450 3900 4800 3900
Wire Wire Line
	4450 4000 4800 4000
Wire Wire Line
	4450 4100 4800 4100
Text Label 4500 3900 0    60   ~ 0
P086
Text Label 4500 4000 0    60   ~ 0
P084
Text Label 4500 4100 0    60   ~ 0
P082
Wire Wire Line
	4450 4200 4800 4200
Text Label 4500 4200 0    60   ~ 0
P080
Wire Wire Line
	4450 4300 4800 4300
Wire Wire Line
	4450 4400 4800 4400
Wire Wire Line
	4450 4500 4800 4500
Text Label 4500 4300 0    60   ~ 0
P078
Text Label 4500 4400 0    60   ~ 0
P076
Text Label 4500 4500 0    60   ~ 0
P074
Wire Wire Line
	4450 4600 4800 4600
Text Label 4500 4600 0    60   ~ 0
P072
Wire Wire Line
	4450 4700 4800 4700
Wire Wire Line
	4450 4800 4800 4800
Wire Wire Line
	4450 4900 4800 4900
Text Label 4500 4700 0    60   ~ 0
P070
Text Label 4500 4800 0    60   ~ 0
P068
Text Label 4500 4900 0    60   ~ 0
P066
Wire Wire Line
	5600 3200 5950 3200
Wire Wire Line
	4800 3200 4450 3200
Text Label 4500 3200 0    60   ~ 0
P100
Text Label 5650 3200 0    60   ~ 0
P099
Wire Wire Line
	6800 1200 7150 1200
Text Label 6850 1200 0    60   ~ 0
P001
Wire Wire Line
	6800 1300 7150 1300
Wire Wire Line
	6800 1400 7150 1400
Wire Wire Line
	6800 1500 7150 1500
Text Label 6850 1300 0    60   ~ 0
P003
Text Label 6850 1400 0    60   ~ 0
P005
Text Label 6850 1500 0    60   ~ 0
P007
Wire Wire Line
	6800 1600 7150 1600
Text Label 6850 1600 0    60   ~ 0
P009
Wire Wire Line
	6800 1700 7150 1700
Wire Wire Line
	6800 1800 7150 1800
Wire Wire Line
	6800 1900 7150 1900
Text Label 6850 1700 0    60   ~ 0
P011
Text Label 6850 1800 0    60   ~ 0
P013
Text Label 6850 1900 0    60   ~ 0
P015
Wire Wire Line
	6800 2000 7150 2000
Text Label 6850 2000 0    60   ~ 0
P017
Wire Wire Line
	6800 2100 7150 2100
Wire Wire Line
	6800 2200 7150 2200
Wire Wire Line
	6800 2300 7150 2300
Text Label 6850 2100 0    60   ~ 0
P019
Text Label 6850 2200 0    60   ~ 0
P021
Text Label 6850 2300 0    60   ~ 0
P023
Wire Wire Line
	6800 2400 7150 2400
Text Label 6850 2400 0    60   ~ 0
P025
Wire Wire Line
	6800 2500 7150 2500
Wire Wire Line
	6800 2600 7150 2600
Wire Wire Line
	6800 2700 7150 2700
Text Label 6850 2500 0    60   ~ 0
P027
Text Label 6850 2600 0    60   ~ 0
P029
Text Label 6850 2700 0    60   ~ 0
P031
Wire Wire Line
	6800 2800 7150 2800
Text Label 6850 2800 0    60   ~ 0
P033
Wire Wire Line
	6800 2900 7150 2900
Wire Wire Line
	6800 3000 7150 3000
Wire Wire Line
	6800 3100 7150 3100
Text Label 6850 2900 0    60   ~ 0
P035
Text Label 6850 3000 0    60   ~ 0
P037
Text Label 6850 3100 0    60   ~ 0
P039
Wire Wire Line
	6800 3200 7150 3200
Text Label 6850 3200 0    60   ~ 0
P041
Wire Wire Line
	6800 3300 7150 3300
Wire Wire Line
	6800 3400 7150 3400
Wire Wire Line
	6800 3500 7150 3500
Text Label 6850 3300 0    60   ~ 0
P043
Text Label 6850 3400 0    60   ~ 0
P045
Text Label 6850 3500 0    60   ~ 0
P047
Wire Wire Line
	6800 3600 7150 3600
Text Label 6850 3600 0    60   ~ 0
P049
Wire Wire Line
	6800 3700 7150 3700
Wire Wire Line
	6800 3800 7150 3800
Wire Wire Line
	6800 3900 7150 3900
Text Label 6850 3700 0    60   ~ 0
P051
Text Label 6850 3800 0    60   ~ 0
P053
Text Label 6850 3900 0    60   ~ 0
P055
Wire Wire Line
	6800 4000 7150 4000
Text Label 6850 4000 0    60   ~ 0
P057
Wire Wire Line
	6800 4100 7150 4100
Wire Wire Line
	6800 4200 7150 4200
Wire Wire Line
	6800 4300 7150 4300
Text Label 6850 4100 0    60   ~ 0
P059
Text Label 6850 4200 0    60   ~ 0
P061
Text Label 6850 4300 0    60   ~ 0
P063
Wire Wire Line
	7650 1200 8000 1200
Text Label 7700 1200 0    60   ~ 0
P002
Wire Wire Line
	7650 1300 8000 1300
Wire Wire Line
	7650 1400 8000 1400
Wire Wire Line
	7650 1500 8000 1500
Text Label 7700 1300 0    60   ~ 0
P004
Text Label 7700 1400 0    60   ~ 0
P006
Text Label 7700 1500 0    60   ~ 0
P008
Wire Wire Line
	7650 1600 8000 1600
Text Label 7700 1600 0    60   ~ 0
P010
Wire Wire Line
	7650 1700 8000 1700
Wire Wire Line
	7650 1800 8000 1800
Wire Wire Line
	7650 1900 8000 1900
Text Label 7700 1700 0    60   ~ 0
P012
Text Label 7700 1800 0    60   ~ 0
P014
Text Label 7700 1900 0    60   ~ 0
P016
Wire Wire Line
	7650 2000 8000 2000
Text Label 7700 2000 0    60   ~ 0
P018
Wire Wire Line
	7650 2100 8000 2100
Wire Wire Line
	7650 2200 8000 2200
Wire Wire Line
	7650 2300 8000 2300
Text Label 7700 2100 0    60   ~ 0
P020
Text Label 7700 2200 0    60   ~ 0
P022
Text Label 7700 2300 0    60   ~ 0
P024
Wire Wire Line
	7650 2400 8000 2400
Text Label 7700 2400 0    60   ~ 0
P026
Wire Wire Line
	7650 2500 8000 2500
Wire Wire Line
	7650 2600 8000 2600
Wire Wire Line
	7650 2700 8000 2700
Text Label 7700 2500 0    60   ~ 0
P028
Text Label 7700 2600 0    60   ~ 0
P030
Text Label 7700 2700 0    60   ~ 0
P032
Wire Wire Line
	7650 2800 8000 2800
Text Label 7700 2800 0    60   ~ 0
P034
Wire Wire Line
	7650 2900 8000 2900
Wire Wire Line
	7650 3000 8000 3000
Wire Wire Line
	7650 3100 8000 3100
Text Label 7700 2900 0    60   ~ 0
P036
Text Label 7700 3000 0    60   ~ 0
P038
Text Label 7700 3100 0    60   ~ 0
P040
Wire Wire Line
	7650 3200 8000 3200
Text Label 7700 3200 0    60   ~ 0
P042
Wire Wire Line
	7650 3300 8000 3300
Wire Wire Line
	7650 3400 8000 3400
Wire Wire Line
	7650 3500 8000 3500
Text Label 7700 3300 0    60   ~ 0
P044
Text Label 7700 3400 0    60   ~ 0
P046
Text Label 7700 3500 0    60   ~ 0
P048
Wire Wire Line
	7650 3600 8000 3600
Text Label 7700 3600 0    60   ~ 0
P050
Wire Wire Line
	7650 3700 8000 3700
Wire Wire Line
	7650 3800 8000 3800
Wire Wire Line
	7650 3900 8000 3900
Text Label 7700 3700 0    60   ~ 0
P052
Text Label 7700 3800 0    60   ~ 0
P054
Text Label 7700 3900 0    60   ~ 0
P056
Wire Wire Line
	7650 4000 8000 4000
Text Label 7700 4000 0    60   ~ 0
P058
Wire Wire Line
	7650 4100 8000 4100
Wire Wire Line
	7650 4200 8000 4200
Wire Wire Line
	7650 4300 8000 4300
Text Label 7700 4100 0    60   ~ 0
P060
Text Label 7700 4200 0    60   ~ 0
P062
Text Label 7700 4300 0    60   ~ 0
P064
Wire Wire Line
	7650 6000 8000 6000
Text Label 7700 6000 0    60   ~ 0
P098
Wire Wire Line
	7650 5900 8000 5900
Text Label 7700 5900 0    60   ~ 0
P096
Wire Wire Line
	7650 5800 8000 5800
Wire Wire Line
	7650 5700 8000 5700
Wire Wire Line
	7650 5600 8000 5600
Text Label 7700 5800 0    60   ~ 0
P094
Text Label 7700 5700 0    60   ~ 0
P092
Text Label 7700 5600 0    60   ~ 0
P090
Wire Wire Line
	7650 5500 8000 5500
Text Label 7700 5500 0    60   ~ 0
P088
Wire Wire Line
	7650 5400 8000 5400
Wire Wire Line
	7650 5300 8000 5300
Wire Wire Line
	7650 5200 8000 5200
Text Label 7700 5400 0    60   ~ 0
P086
Text Label 7700 5300 0    60   ~ 0
P084
Text Label 7700 5200 0    60   ~ 0
P082
Wire Wire Line
	7650 5100 8000 5100
Text Label 7700 5100 0    60   ~ 0
P080
Wire Wire Line
	7650 5000 8000 5000
Wire Wire Line
	7650 4900 8000 4900
Wire Wire Line
	7650 4800 8000 4800
Text Label 7700 5000 0    60   ~ 0
P078
Text Label 7700 4900 0    60   ~ 0
P076
Text Label 7700 4800 0    60   ~ 0
P074
Wire Wire Line
	7650 4700 8000 4700
Text Label 7700 4700 0    60   ~ 0
P072
Wire Wire Line
	7650 4600 8000 4600
Wire Wire Line
	7650 4500 8000 4500
Wire Wire Line
	7650 4400 8000 4400
Text Label 7700 4600 0    60   ~ 0
P070
Text Label 7700 4500 0    60   ~ 0
P068
Text Label 7700 4400 0    60   ~ 0
P066
Wire Wire Line
	8000 6100 7650 6100
Text Label 7700 6100 0    60   ~ 0
P100
Wire Wire Line
	6800 6000 7150 6000
Text Label 6850 6000 0    60   ~ 0
P097
Wire Wire Line
	6800 5900 7150 5900
Text Label 6850 5900 0    60   ~ 0
P095
Wire Wire Line
	6800 5800 7150 5800
Wire Wire Line
	6800 5700 7150 5700
Wire Wire Line
	6800 5600 7150 5600
Text Label 6850 5800 0    60   ~ 0
P093
Text Label 6850 5700 0    60   ~ 0
P091
Text Label 6850 5600 0    60   ~ 0
P089
Wire Wire Line
	6800 5500 7150 5500
Text Label 6850 5500 0    60   ~ 0
P087
Wire Wire Line
	6800 5400 7150 5400
Wire Wire Line
	6800 5300 7150 5300
Wire Wire Line
	6800 5200 7150 5200
Text Label 6850 5400 0    60   ~ 0
P085
Text Label 6850 5300 0    60   ~ 0
P083
Text Label 6850 5200 0    60   ~ 0
P081
Wire Wire Line
	6800 5100 7150 5100
Text Label 6850 5100 0    60   ~ 0
P079
Wire Wire Line
	6800 5000 7150 5000
Wire Wire Line
	6800 4900 7150 4900
Wire Wire Line
	6800 4800 7150 4800
Text Label 6850 5000 0    60   ~ 0
P077
Text Label 6850 4900 0    60   ~ 0
P075
Text Label 6850 4800 0    60   ~ 0
P073
Wire Wire Line
	6800 4700 7150 4700
Text Label 6850 4700 0    60   ~ 0
P071
Wire Wire Line
	6800 4600 7150 4600
Wire Wire Line
	6800 4500 7150 4500
Wire Wire Line
	6800 4400 7150 4400
Text Label 6850 4600 0    60   ~ 0
P069
Text Label 6850 4500 0    60   ~ 0
P067
Text Label 6850 4400 0    60   ~ 0
P065
Wire Wire Line
	6800 6100 7150 6100
Text Label 6850 6100 0    60   ~ 0
P099
$EndSCHEMATC
