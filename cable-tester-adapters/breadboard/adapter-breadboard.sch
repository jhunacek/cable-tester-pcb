EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:mdm
LIBS:ad5270
LIBS:adg732
LIBS:ads1120
LIBS:arduino
LIBS:connector-header-128
LIBS:lm234-to92
LIBS:lmc7660
LIBS:lp2985
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	1800 1000 2150 1000
Text Label 1850 1000 0    60   ~ 0
P002
Wire Wire Line
	1800 1100 2150 1100
Wire Wire Line
	1800 1200 2150 1200
Wire Wire Line
	1800 1300 2150 1300
Text Label 1850 1100 0    60   ~ 0
P004
Text Label 1850 1200 0    60   ~ 0
P006
Text Label 1850 1300 0    60   ~ 0
P008
Wire Wire Line
	1800 1400 2150 1400
Text Label 1850 1400 0    60   ~ 0
P010
Wire Wire Line
	1800 1500 2150 1500
Wire Wire Line
	1800 1600 2150 1600
Wire Wire Line
	1800 1700 2150 1700
Text Label 1850 1500 0    60   ~ 0
P012
Text Label 1850 1600 0    60   ~ 0
P014
Text Label 1850 1700 0    60   ~ 0
P016
Wire Wire Line
	1800 1800 2150 1800
Text Label 1850 1800 0    60   ~ 0
P018
Wire Wire Line
	1800 1900 2150 1900
Wire Wire Line
	1800 2000 2150 2000
Wire Wire Line
	1800 2100 2150 2100
Text Label 1850 1900 0    60   ~ 0
P020
Text Label 1850 2000 0    60   ~ 0
P022
Text Label 1850 2100 0    60   ~ 0
P024
Wire Wire Line
	1800 2200 2150 2200
Text Label 1850 2200 0    60   ~ 0
P026
Wire Wire Line
	1800 2300 2150 2300
Wire Wire Line
	1800 2400 2150 2400
Wire Wire Line
	1800 2500 2150 2500
Text Label 1850 2300 0    60   ~ 0
P028
Text Label 1850 2400 0    60   ~ 0
P030
Text Label 1850 2500 0    60   ~ 0
P032
Wire Wire Line
	1800 2600 2150 2600
Text Label 1850 2600 0    60   ~ 0
P034
Wire Wire Line
	1800 2700 2150 2700
Wire Wire Line
	1800 2800 2150 2800
Wire Wire Line
	1800 2900 2150 2900
Text Label 1850 2700 0    60   ~ 0
P036
Text Label 1850 2800 0    60   ~ 0
P038
Text Label 1850 2900 0    60   ~ 0
P040
Wire Wire Line
	1800 3000 2150 3000
Text Label 1850 3000 0    60   ~ 0
P042
Wire Wire Line
	1800 3100 2150 3100
Wire Wire Line
	1800 3200 2150 3200
Wire Wire Line
	1800 3300 2150 3300
Text Label 1850 3100 0    60   ~ 0
P044
Text Label 1850 3200 0    60   ~ 0
P046
Text Label 1850 3300 0    60   ~ 0
P048
Wire Wire Line
	1800 3400 2150 3400
Text Label 1850 3400 0    60   ~ 0
P050
Wire Wire Line
	1800 3500 2150 3500
Wire Wire Line
	1800 3600 2150 3600
Wire Wire Line
	1800 3700 2150 3700
Text Label 1850 3500 0    60   ~ 0
P052
Text Label 1850 3600 0    60   ~ 0
P054
Text Label 1850 3700 0    60   ~ 0
P056
Wire Wire Line
	1800 3800 2150 3800
Text Label 1850 3800 0    60   ~ 0
P058
Wire Wire Line
	1800 3900 2150 3900
Wire Wire Line
	1800 4000 2150 4000
Wire Wire Line
	1800 4100 2150 4100
Text Label 1850 3900 0    60   ~ 0
P060
Text Label 1850 4000 0    60   ~ 0
P062
Text Label 1850 4100 0    60   ~ 0
P064
Wire Wire Line
	650  1000 1000 1000
Text Label 700  1000 0    60   ~ 0
P001
Wire Wire Line
	650  1100 1000 1100
Wire Wire Line
	650  1200 1000 1200
Wire Wire Line
	650  1300 1000 1300
Text Label 700  1100 0    60   ~ 0
P003
Text Label 700  1200 0    60   ~ 0
P005
Text Label 700  1300 0    60   ~ 0
P007
Wire Wire Line
	650  1400 1000 1400
Text Label 700  1400 0    60   ~ 0
P009
Wire Wire Line
	650  1500 1000 1500
Wire Wire Line
	650  1600 1000 1600
Wire Wire Line
	650  1700 1000 1700
Text Label 700  1500 0    60   ~ 0
P011
Text Label 700  1600 0    60   ~ 0
P013
Text Label 700  1700 0    60   ~ 0
P015
Wire Wire Line
	650  1800 1000 1800
Text Label 700  1800 0    60   ~ 0
P017
Wire Wire Line
	650  1900 1000 1900
Wire Wire Line
	650  2000 1000 2000
Wire Wire Line
	650  2100 1000 2100
Text Label 700  1900 0    60   ~ 0
P019
Text Label 700  2000 0    60   ~ 0
P021
Text Label 700  2100 0    60   ~ 0
P023
Wire Wire Line
	650  2200 1000 2200
Text Label 700  2200 0    60   ~ 0
P025
Wire Wire Line
	650  2300 1000 2300
Wire Wire Line
	650  2400 1000 2400
Wire Wire Line
	650  2500 1000 2500
Text Label 700  2300 0    60   ~ 0
P027
Text Label 700  2400 0    60   ~ 0
P029
Text Label 700  2500 0    60   ~ 0
P031
Wire Wire Line
	650  2600 1000 2600
Text Label 700  2600 0    60   ~ 0
P033
Wire Wire Line
	650  2700 1000 2700
Wire Wire Line
	650  2800 1000 2800
Wire Wire Line
	650  2900 1000 2900
Text Label 700  2700 0    60   ~ 0
P035
Text Label 700  2800 0    60   ~ 0
P037
Text Label 700  2900 0    60   ~ 0
P039
Wire Wire Line
	650  3000 1000 3000
Text Label 700  3000 0    60   ~ 0
P041
Wire Wire Line
	650  3100 1000 3100
Wire Wire Line
	650  3200 1000 3200
Wire Wire Line
	650  3300 1000 3300
Text Label 700  3100 0    60   ~ 0
P043
Text Label 700  3200 0    60   ~ 0
P045
Text Label 700  3300 0    60   ~ 0
P047
Wire Wire Line
	650  3400 1000 3400
Text Label 700  3400 0    60   ~ 0
P049
Wire Wire Line
	650  3500 1000 3500
Wire Wire Line
	650  3600 1000 3600
Wire Wire Line
	650  3700 1000 3700
Text Label 700  3500 0    60   ~ 0
P051
Text Label 700  3600 0    60   ~ 0
P053
Text Label 700  3700 0    60   ~ 0
P055
Wire Wire Line
	650  3800 1000 3800
Text Label 700  3800 0    60   ~ 0
P057
Wire Wire Line
	650  3900 1000 3900
Wire Wire Line
	650  4000 1000 4000
Wire Wire Line
	650  4100 1000 4100
Text Label 700  3900 0    60   ~ 0
P059
Text Label 700  4000 0    60   ~ 0
P061
Text Label 700  4100 0    60   ~ 0
P063
Wire Wire Line
	4300 2500 4650 2500
Text Label 4350 2500 0    60   ~ 0
P097
Wire Wire Line
	4300 2600 4650 2600
Text Label 4350 2600 0    60   ~ 0
P095
Wire Wire Line
	4300 2700 4650 2700
Wire Wire Line
	4300 2800 4650 2800
Wire Wire Line
	4300 2900 4650 2900
Text Label 4350 2700 0    60   ~ 0
P093
Text Label 4350 2800 0    60   ~ 0
P091
Text Label 4350 2900 0    60   ~ 0
P089
Wire Wire Line
	4300 3000 4650 3000
Text Label 4350 3000 0    60   ~ 0
P087
Wire Wire Line
	4300 3100 4650 3100
Wire Wire Line
	4300 3200 4650 3200
Wire Wire Line
	4300 3300 4650 3300
Text Label 4350 3100 0    60   ~ 0
P085
Text Label 4350 3200 0    60   ~ 0
P083
Text Label 4350 3300 0    60   ~ 0
P081
Wire Wire Line
	4300 3400 4650 3400
Text Label 4350 3400 0    60   ~ 0
P079
Wire Wire Line
	4300 3500 4650 3500
Wire Wire Line
	4300 3600 4650 3600
Wire Wire Line
	4300 3700 4650 3700
Text Label 4350 3500 0    60   ~ 0
P077
Text Label 4350 3600 0    60   ~ 0
P075
Text Label 4350 3700 0    60   ~ 0
P073
Wire Wire Line
	4300 3800 4650 3800
Text Label 4350 3800 0    60   ~ 0
P071
Wire Wire Line
	4300 3900 4650 3900
Wire Wire Line
	4300 4000 4650 4000
Wire Wire Line
	4300 4100 4650 4100
Text Label 4350 3900 0    60   ~ 0
P069
Text Label 4350 4000 0    60   ~ 0
P067
Text Label 4350 4100 0    60   ~ 0
P065
Wire Wire Line
	3150 2500 3500 2500
Text Label 3200 2500 0    60   ~ 0
P098
Wire Wire Line
	3150 2600 3500 2600
Text Label 3200 2600 0    60   ~ 0
P096
Wire Wire Line
	3150 2700 3500 2700
Wire Wire Line
	3150 2800 3500 2800
Wire Wire Line
	3150 2900 3500 2900
Text Label 3200 2700 0    60   ~ 0
P094
Text Label 3200 2800 0    60   ~ 0
P092
Text Label 3200 2900 0    60   ~ 0
P090
Wire Wire Line
	3150 3000 3500 3000
Text Label 3200 3000 0    60   ~ 0
P088
Wire Wire Line
	3150 3100 3500 3100
Wire Wire Line
	3150 3200 3500 3200
Wire Wire Line
	3150 3300 3500 3300
Text Label 3200 3100 0    60   ~ 0
P086
Text Label 3200 3200 0    60   ~ 0
P084
Text Label 3200 3300 0    60   ~ 0
P082
Wire Wire Line
	3150 3400 3500 3400
Text Label 3200 3400 0    60   ~ 0
P080
Wire Wire Line
	3150 3500 3500 3500
Wire Wire Line
	3150 3600 3500 3600
Wire Wire Line
	3150 3700 3500 3700
Text Label 3200 3500 0    60   ~ 0
P078
Text Label 3200 3600 0    60   ~ 0
P076
Text Label 3200 3700 0    60   ~ 0
P074
Wire Wire Line
	3150 3800 3500 3800
Text Label 3200 3800 0    60   ~ 0
P072
Wire Wire Line
	3150 3900 3500 3900
Wire Wire Line
	3150 4000 3500 4000
Wire Wire Line
	3150 4100 3500 4100
Text Label 3200 3900 0    60   ~ 0
P070
Text Label 3200 4000 0    60   ~ 0
P068
Text Label 3200 4100 0    60   ~ 0
P066
Wire Wire Line
	4300 2400 4650 2400
Wire Wire Line
	3500 2400 3150 2400
Text Label 3200 2400 0    60   ~ 0
P100
Text Label 4350 2400 0    60   ~ 0
P099
Wire Wire Line
	6200 950  6550 950 
Text Label 6250 950  0    60   ~ 0
P001
Wire Wire Line
	6200 1050 6550 1050
Wire Wire Line
	6200 1150 6550 1150
Wire Wire Line
	6200 1250 6550 1250
Text Label 6250 1050 0    60   ~ 0
P003
Text Label 6250 1150 0    60   ~ 0
P005
Text Label 6250 1250 0    60   ~ 0
P007
Wire Wire Line
	6200 1350 6550 1350
Text Label 6250 1350 0    60   ~ 0
P009
Wire Wire Line
	6200 1450 6550 1450
Wire Wire Line
	6200 1550 6550 1550
Wire Wire Line
	6200 1650 6550 1650
Text Label 6250 1450 0    60   ~ 0
P011
Text Label 6250 1550 0    60   ~ 0
P013
Text Label 6250 1650 0    60   ~ 0
P015
Wire Wire Line
	6200 1750 6550 1750
Text Label 6250 1750 0    60   ~ 0
P017
Wire Wire Line
	6200 1850 6550 1850
Wire Wire Line
	6200 1950 6550 1950
Wire Wire Line
	6200 2050 6550 2050
Text Label 6250 1850 0    60   ~ 0
P019
Text Label 6250 1950 0    60   ~ 0
P021
Text Label 6250 2050 0    60   ~ 0
P023
Wire Wire Line
	6200 2150 6550 2150
Text Label 6250 2150 0    60   ~ 0
P025
Wire Wire Line
	6200 2250 6550 2250
Wire Wire Line
	6200 2350 6550 2350
Wire Wire Line
	6200 2450 6550 2450
Text Label 6250 2250 0    60   ~ 0
P027
Text Label 6250 2350 0    60   ~ 0
P029
Text Label 6250 2450 0    60   ~ 0
P031
Wire Wire Line
	6200 2550 6550 2550
Text Label 6250 2550 0    60   ~ 0
P033
Wire Wire Line
	6200 2650 6550 2650
Wire Wire Line
	6200 2750 6550 2750
Wire Wire Line
	6200 2850 6550 2850
Text Label 6250 2650 0    60   ~ 0
P035
Text Label 6250 2750 0    60   ~ 0
P037
Text Label 6250 2850 0    60   ~ 0
P039
Wire Wire Line
	6200 2950 6550 2950
Text Label 6250 2950 0    60   ~ 0
P041
Wire Wire Line
	6200 3050 6550 3050
Wire Wire Line
	6200 3150 6550 3150
Wire Wire Line
	6200 3250 6550 3250
Text Label 6250 3050 0    60   ~ 0
P043
Text Label 6250 3150 0    60   ~ 0
P045
Text Label 6250 3250 0    60   ~ 0
P047
Wire Wire Line
	6200 3350 6550 3350
Text Label 6250 3350 0    60   ~ 0
P049
Wire Wire Line
	6200 3450 6550 3450
Wire Wire Line
	6200 3550 6550 3550
Wire Wire Line
	6200 3650 6550 3650
Text Label 6250 3450 0    60   ~ 0
P051
Text Label 6250 3550 0    60   ~ 0
P053
Text Label 6250 3650 0    60   ~ 0
P055
Wire Wire Line
	6200 3750 6550 3750
Text Label 6250 3750 0    60   ~ 0
P057
Wire Wire Line
	6200 3850 6550 3850
Wire Wire Line
	6200 3950 6550 3950
Wire Wire Line
	6200 4050 6550 4050
Text Label 6250 3850 0    60   ~ 0
P059
Text Label 6250 3950 0    60   ~ 0
P061
Text Label 6250 4050 0    60   ~ 0
P063
Wire Wire Line
	7050 950  7400 950 
Text Label 7100 950  0    60   ~ 0
P002
Wire Wire Line
	7050 1050 7400 1050
Wire Wire Line
	7050 1150 7400 1150
Wire Wire Line
	7050 1250 7400 1250
Text Label 7100 1050 0    60   ~ 0
P004
Text Label 7100 1150 0    60   ~ 0
P006
Text Label 7100 1250 0    60   ~ 0
P008
Wire Wire Line
	7050 1350 7400 1350
Text Label 7100 1350 0    60   ~ 0
P010
Wire Wire Line
	7050 1450 7400 1450
Wire Wire Line
	7050 1550 7400 1550
Wire Wire Line
	7050 1650 7400 1650
Text Label 7100 1450 0    60   ~ 0
P012
Text Label 7100 1550 0    60   ~ 0
P014
Text Label 7100 1650 0    60   ~ 0
P016
Wire Wire Line
	7050 1750 7400 1750
Text Label 7100 1750 0    60   ~ 0
P018
Wire Wire Line
	7050 1850 7400 1850
Wire Wire Line
	7050 1950 7400 1950
Wire Wire Line
	7050 2050 7400 2050
Text Label 7100 1850 0    60   ~ 0
P020
Text Label 7100 1950 0    60   ~ 0
P022
Text Label 7100 2050 0    60   ~ 0
P024
Wire Wire Line
	7050 2150 7400 2150
Text Label 7100 2150 0    60   ~ 0
P026
Wire Wire Line
	7050 2250 7400 2250
Wire Wire Line
	7050 2350 7400 2350
Wire Wire Line
	7050 2450 7400 2450
Text Label 7100 2250 0    60   ~ 0
P028
Text Label 7100 2350 0    60   ~ 0
P030
Text Label 7100 2450 0    60   ~ 0
P032
Wire Wire Line
	7050 2550 7400 2550
Text Label 7100 2550 0    60   ~ 0
P034
Wire Wire Line
	7050 2650 7400 2650
Wire Wire Line
	7050 2750 7400 2750
Wire Wire Line
	7050 2850 7400 2850
Text Label 7100 2650 0    60   ~ 0
P036
Text Label 7100 2750 0    60   ~ 0
P038
Text Label 7100 2850 0    60   ~ 0
P040
Wire Wire Line
	7050 2950 7400 2950
Text Label 7100 2950 0    60   ~ 0
P042
Wire Wire Line
	7050 3050 7400 3050
Wire Wire Line
	7050 3150 7400 3150
Wire Wire Line
	7050 3250 7400 3250
Text Label 7100 3050 0    60   ~ 0
P044
Text Label 7100 3150 0    60   ~ 0
P046
Text Label 7100 3250 0    60   ~ 0
P048
Wire Wire Line
	7050 3350 7400 3350
Text Label 7100 3350 0    60   ~ 0
P050
Wire Wire Line
	7050 3450 7400 3450
Wire Wire Line
	7050 3550 7400 3550
Wire Wire Line
	7050 3650 7400 3650
Text Label 7100 3450 0    60   ~ 0
P052
Text Label 7100 3550 0    60   ~ 0
P054
Text Label 7100 3650 0    60   ~ 0
P056
Wire Wire Line
	7050 3750 7400 3750
Text Label 7100 3750 0    60   ~ 0
P058
Wire Wire Line
	7050 3850 7400 3850
Wire Wire Line
	7050 3950 7400 3950
Wire Wire Line
	7050 4050 7400 4050
Text Label 7100 3850 0    60   ~ 0
P060
Text Label 7100 3950 0    60   ~ 0
P062
Text Label 7100 4050 0    60   ~ 0
P064
Wire Wire Line
	8800 2550 9150 2550
Text Label 8850 2550 0    60   ~ 0
P098
Wire Wire Line
	8800 2450 9150 2450
Text Label 8850 2450 0    60   ~ 0
P096
Wire Wire Line
	8800 2350 9150 2350
Wire Wire Line
	8800 2250 9150 2250
Wire Wire Line
	8800 2150 9150 2150
Text Label 8850 2350 0    60   ~ 0
P094
Text Label 8850 2250 0    60   ~ 0
P092
Text Label 8850 2150 0    60   ~ 0
P090
Wire Wire Line
	8800 2050 9150 2050
Text Label 8850 2050 0    60   ~ 0
P088
Wire Wire Line
	8800 1950 9150 1950
Wire Wire Line
	8800 1850 9150 1850
Wire Wire Line
	8800 1750 9150 1750
Text Label 8850 1950 0    60   ~ 0
P086
Text Label 8850 1850 0    60   ~ 0
P084
Text Label 8850 1750 0    60   ~ 0
P082
Wire Wire Line
	8800 1650 9150 1650
Text Label 8850 1650 0    60   ~ 0
P080
Wire Wire Line
	8800 1550 9150 1550
Wire Wire Line
	8800 1450 9150 1450
Wire Wire Line
	8800 1350 9150 1350
Text Label 8850 1550 0    60   ~ 0
P078
Text Label 8850 1450 0    60   ~ 0
P076
Text Label 8850 1350 0    60   ~ 0
P074
Wire Wire Line
	8800 1250 9150 1250
Text Label 8850 1250 0    60   ~ 0
P072
Wire Wire Line
	8800 1150 9150 1150
Wire Wire Line
	8800 1050 9150 1050
Wire Wire Line
	8800 950  9150 950 
Text Label 8850 1150 0    60   ~ 0
P070
Text Label 8850 1050 0    60   ~ 0
P068
Text Label 8850 950  0    60   ~ 0
P066
Wire Wire Line
	9150 2650 8800 2650
Text Label 8850 2650 0    60   ~ 0
P100
Wire Wire Line
	7950 2550 8300 2550
Text Label 8000 2550 0    60   ~ 0
P097
Wire Wire Line
	7950 2450 8300 2450
Text Label 8000 2450 0    60   ~ 0
P095
Wire Wire Line
	7950 2350 8300 2350
Wire Wire Line
	7950 2250 8300 2250
Wire Wire Line
	7950 2150 8300 2150
Text Label 8000 2350 0    60   ~ 0
P093
Text Label 8000 2250 0    60   ~ 0
P091
Text Label 8000 2150 0    60   ~ 0
P089
Wire Wire Line
	7950 2050 8300 2050
Text Label 8000 2050 0    60   ~ 0
P087
Wire Wire Line
	7950 1950 8300 1950
Wire Wire Line
	7950 1850 8300 1850
Wire Wire Line
	7950 1750 8300 1750
Text Label 8000 1950 0    60   ~ 0
P085
Text Label 8000 1850 0    60   ~ 0
P083
Text Label 8000 1750 0    60   ~ 0
P081
Wire Wire Line
	7950 1650 8300 1650
Text Label 8000 1650 0    60   ~ 0
P079
Wire Wire Line
	7950 1550 8300 1550
Wire Wire Line
	7950 1450 8300 1450
Wire Wire Line
	7950 1350 8300 1350
Text Label 8000 1550 0    60   ~ 0
P077
Text Label 8000 1450 0    60   ~ 0
P075
Text Label 8000 1350 0    60   ~ 0
P073
Wire Wire Line
	7950 1250 8300 1250
Text Label 8000 1250 0    60   ~ 0
P071
Wire Wire Line
	7950 1150 8300 1150
Wire Wire Line
	7950 1050 8300 1050
Wire Wire Line
	7950 950  8300 950 
Text Label 8000 1150 0    60   ~ 0
P069
Text Label 8000 1050 0    60   ~ 0
P067
Text Label 8000 950  0    60   ~ 0
P065
Wire Wire Line
	7950 2650 8300 2650
Text Label 8000 2650 0    60   ~ 0
P099
Wire Wire Line
	7950 2750 8300 2750
Text Label 8000 2750 0    60   ~ 0
P101
Wire Wire Line
	7950 2850 8300 2850
Text Label 8000 2850 0    60   ~ 0
P103
Wire Wire Line
	7950 2950 8300 2950
Text Label 8000 2950 0    60   ~ 0
P105
Wire Wire Line
	7950 3050 8300 3050
Text Label 8000 3050 0    60   ~ 0
P107
Wire Wire Line
	7950 3150 8300 3150
Text Label 8000 3150 0    60   ~ 0
P109
Wire Wire Line
	7950 3250 8300 3250
Text Label 8000 3250 0    60   ~ 0
P111
Wire Wire Line
	8800 3050 9150 3050
Text Label 8850 3050 0    60   ~ 0
P108
Wire Wire Line
	8800 2950 9150 2950
Text Label 8850 2950 0    60   ~ 0
P106
Wire Wire Line
	8800 2850 9150 2850
Wire Wire Line
	8800 2750 9150 2750
Text Label 8850 2850 0    60   ~ 0
P104
Text Label 8850 2750 0    60   ~ 0
P102
Wire Wire Line
	9150 3150 8800 3150
Text Label 8850 3150 0    60   ~ 0
P110
Wire Wire Line
	4300 2300 4650 2300
Text Label 4350 2300 0    60   ~ 0
P101
Wire Wire Line
	4300 2200 4650 2200
Text Label 4350 2200 0    60   ~ 0
P103
Wire Wire Line
	4300 2100 4650 2100
Text Label 4350 2100 0    60   ~ 0
P105
Wire Wire Line
	4300 2000 4650 2000
Text Label 4350 2000 0    60   ~ 0
P107
Wire Wire Line
	4300 1900 4650 1900
Text Label 4350 1900 0    60   ~ 0
P109
Wire Wire Line
	4300 1800 4650 1800
Text Label 4350 1800 0    60   ~ 0
P111
Wire Wire Line
	3150 2000 3500 2000
Text Label 3200 2000 0    60   ~ 0
P108
Wire Wire Line
	3150 2100 3500 2100
Text Label 3200 2100 0    60   ~ 0
P106
Wire Wire Line
	3150 2200 3500 2200
Wire Wire Line
	3150 2300 3500 2300
Text Label 3200 2200 0    60   ~ 0
P104
Text Label 3200 2300 0    60   ~ 0
P102
Wire Wire Line
	3500 1900 3150 1900
Text Label 3200 1900 0    60   ~ 0
P110
$Comp
L CONN_02X32 P1
U 1 1 57AE2F8D
P 6800 2500
F 0 "P1" H 6800 4150 50  0000 C CNN
F 1 "CONN_02X32" V 6800 2500 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x32" H 6800 2100 50  0001 C CNN
F 3 "" H 6800 2100 50  0000 C CNN
	1    6800 2500
	1    0    0    -1  
$EndComp
$Comp
L CONN_02X32 P2
U 1 1 57AE312F
P 8550 2500
F 0 "P2" H 8550 4150 50  0000 C CNN
F 1 "CONN_02X32" V 8550 2500 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x32" H 8550 2100 50  0001 C CNN
F 3 "" H 8550 2100 50  0000 C CNN
	1    8550 2500
	1    0    0    -1  
$EndComp
$Comp
L connector-header-128 U1
U 1 1 57ABEA1B
P 1200 1000
F 0 "U1" H 2650 1150 60  0000 C CNN
F 1 "connector-header-128" H 2650 1250 60  0000 C CNN
F 2 "cable-tester:connector-header-128" H 1200 1000 60  0001 C CNN
F 3 "" H 1200 1000 60  0000 C CNN
	1    1200 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 1700 4650 1700
Text Label 4350 1700 0    60   ~ 0
P113
Wire Wire Line
	4300 1600 4650 1600
Text Label 4350 1600 0    60   ~ 0
P115
Wire Wire Line
	4300 1500 4650 1500
Text Label 4350 1500 0    60   ~ 0
P117
Wire Wire Line
	4300 1400 4650 1400
Text Label 4350 1400 0    60   ~ 0
P119
Wire Wire Line
	4300 1300 4650 1300
Text Label 4350 1300 0    60   ~ 0
P121
Wire Wire Line
	4300 1200 4650 1200
Text Label 4350 1200 0    60   ~ 0
P123
Wire Wire Line
	4300 1100 4650 1100
Text Label 4350 1100 0    60   ~ 0
P125
Wire Wire Line
	4300 1000 4650 1000
Text Label 4350 1000 0    60   ~ 0
P127
Wire Wire Line
	3150 1800 3500 1800
Text Label 3200 1800 0    60   ~ 0
P112
Wire Wire Line
	3150 1700 3500 1700
Text Label 3200 1700 0    60   ~ 0
P114
Wire Wire Line
	3150 1600 3500 1600
Text Label 3200 1600 0    60   ~ 0
P116
Wire Wire Line
	3150 1500 3500 1500
Text Label 3200 1500 0    60   ~ 0
P118
Wire Wire Line
	3150 1400 3500 1400
Text Label 3200 1400 0    60   ~ 0
P120
Wire Wire Line
	3150 1300 3500 1300
Text Label 3200 1300 0    60   ~ 0
P122
Wire Wire Line
	3150 1200 3500 1200
Text Label 3200 1200 0    60   ~ 0
P124
Wire Wire Line
	3150 1100 3500 1100
Text Label 3200 1100 0    60   ~ 0
P126
Wire Wire Line
	3150 1000 3500 1000
Text Label 3200 1000 0    60   ~ 0
P128
Wire Wire Line
	8800 3250 9150 3250
Text Label 8850 3250 0    60   ~ 0
P112
Wire Wire Line
	8800 3350 9150 3350
Text Label 8850 3350 0    60   ~ 0
P114
Wire Wire Line
	8800 3450 9150 3450
Text Label 8850 3450 0    60   ~ 0
P116
Wire Wire Line
	8800 3550 9150 3550
Text Label 8850 3550 0    60   ~ 0
P118
Wire Wire Line
	8800 3650 9150 3650
Text Label 8850 3650 0    60   ~ 0
P120
Wire Wire Line
	8800 3750 9150 3750
Text Label 8850 3750 0    60   ~ 0
P122
Wire Wire Line
	8800 3850 9150 3850
Text Label 8850 3850 0    60   ~ 0
P124
Wire Wire Line
	8800 3950 9150 3950
Text Label 8850 3950 0    60   ~ 0
P126
Wire Wire Line
	8800 4050 9150 4050
Text Label 8850 4050 0    60   ~ 0
P128
Wire Wire Line
	7950 3350 8300 3350
Text Label 8000 3350 0    60   ~ 0
P113
Wire Wire Line
	7950 3450 8300 3450
Text Label 8000 3450 0    60   ~ 0
P115
Wire Wire Line
	7950 3550 8300 3550
Text Label 8000 3550 0    60   ~ 0
P117
Wire Wire Line
	7950 3650 8300 3650
Text Label 8000 3650 0    60   ~ 0
P119
Wire Wire Line
	7950 3750 8300 3750
Text Label 8000 3750 0    60   ~ 0
P121
Wire Wire Line
	7950 3850 8300 3850
Text Label 8000 3850 0    60   ~ 0
P123
Wire Wire Line
	7950 3950 8300 3950
Text Label 8000 3950 0    60   ~ 0
P125
Wire Wire Line
	7950 4050 8300 4050
Text Label 8000 4050 0    60   ~ 0
P127
$EndSCHEMATC
